import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesSummaryComponent } from './accessories-summary.component';

describe('AccessoriesSummaryComponent', () => {
  let component: AccessoriesSummaryComponent;
  let fixture: ComponentFixture<AccessoriesSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessoriesSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessoriesSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
