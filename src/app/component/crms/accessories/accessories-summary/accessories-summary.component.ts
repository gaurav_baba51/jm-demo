import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { AccessoriesDataService } from '../accessories-data.service';
import { AccessoriesNewComponent } from '../accessories-new/accessories-new.component';

@Component({
  selector: 'app-accessories-summary',
  templateUrl: './accessories-summary.component.html',
  styleUrls: ['./accessories-summary.component.scss']
})
export class AccessoriesSummaryComponent implements OnInit {

  entityTitle = "Occupation";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private accessoriesDataService: AccessoriesDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.accessoriesDataService.getEntityById(this.id);
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(AccessoriesNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }


}
