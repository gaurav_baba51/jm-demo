import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AccessoriesRoutingModule } from './accessories-routing.module';
import { AccessoriesComponent } from './accessories.component';
import { AccessoriesListComponent } from './accessories-list/accessories-list.component';
import { AccessoriesSummaryComponent } from './accessories-summary/accessories-summary.component';
import { AccessoriesNewComponent } from './accessories-new/accessories-new.component';
import { AccessoriesDataService } from './accessories-data.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AccessoriesRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [
    AccessoriesComponent,
    AccessoriesListComponent,
    AccessoriesSummaryComponent,
    AccessoriesNewComponent],
  providers: [AccessoriesDataService]
})
export class AccessoriesModule { }
