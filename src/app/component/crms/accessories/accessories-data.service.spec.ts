import { TestBed, inject } from '@angular/core/testing';

import { AccessoriesDataService } from './accessories-data.service';

describe('AccessoriesDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccessoriesDataService]
    });
  });

  it('should be created', inject([AccessoriesDataService], (service: AccessoriesDataService) => {
    expect(service).toBeTruthy();
  }));
});
