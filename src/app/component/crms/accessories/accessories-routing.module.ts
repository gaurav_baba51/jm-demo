import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { AccessoriesNewComponent } from './accessories-new/accessories-new.component';
import { AccessoriesComponent } from './accessories.component';
import { AccessoriesSummaryComponent } from './accessories-summary/accessories-summary.component';


const routes: Routes = [
  {
    path: '',
    component: AccessoriesComponent,
    data: {
      title: 'Accessories'
    }
  },
  {
    path: 'new-accessories/:id',
    component: AccessoriesNewComponent ,
    data: {
      title: 'Create New Accessories'
    }
  },
  {
    path: 'accessories-summary/:id',
    component: AccessoriesSummaryComponent ,
    data: {
      title: 'Accessories Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessoriesRoutingModule { }
