import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunicationForComponent } from './communication-for.component';

describe('CommunicationForComponent', () => {
  let component: CommunicationForComponent;
  let fixture: ComponentFixture<CommunicationForComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunicationForComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunicationForComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
