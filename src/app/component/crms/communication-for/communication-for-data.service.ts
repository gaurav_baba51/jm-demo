import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommunicationForDataService {

  entities;

  constructor() { }


  getAllCommunicationFor() {
    return this.entities;
  }

  setAllCommunicationFor(entities) {
    this.entities = entities;
  }

  getCommunicationForById(id) {
    for (var i in this.entities) {
      if (this.entities[i].entity_key_id == id) {
        return this.entities[i];
      }
    }
  }
}
