import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCommunicationForComponent } from './new-communication-for.component';

describe('NewCommunicationForComponent', () => {
  let component: NewCommunicationForComponent;
  let fixture: ComponentFixture<NewCommunicationForComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCommunicationForComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCommunicationForComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
