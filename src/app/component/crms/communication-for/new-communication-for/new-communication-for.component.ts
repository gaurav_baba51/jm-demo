import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from '../../../../service/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-communication-for',
  templateUrl: './new-communication-for.component.html',
  styleUrls: ['./new-communication-for.component.scss']
})
export class NewCommunicationForComponent implements OnInit {
  entity: any;
  modalRef: BsModalRef;
  message: string;
  
  constructor(private route: ActivatedRoute,
    private router: Router,
    public bsModalRef: BsModalRef,
    private apiService: ApiService,
    public modalService: BsModalService,
    private toastr: ToastrService) { 
      console.log("modal:" + modalService);
      console.log("s" + bsModalRef);
      this.entity = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};

    }

  ngOnInit() {
  }

  submit(entity) {
    if (entity != undefined && entity.entity_key_id != undefined) {
      this.update(entity);
    } else {
      this.create(entity);
    }
  }

  update(entity) {
    entity.entity_name = 'commfor';
    this.apiService.updateEntity(entity).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('CommunicationFor Updated Successfully');
      } else {
        this.toastr.error('Failed to Update CommunicationFor');
      }
    }).catch(error => {
      this.toastr.error('Failed to update CommunicationFor');
    });
  }

  create(entity) {
    entity.entity_name = 'commfor';
    this.apiService.addEntity(entity).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('CommunicationFor Added Successfully');
      } else {
        this.toastr.error('Failed to add CommunicationFor');
      }
    }).catch(error => {
      this.toastr.error('Failed to add CommunicationFor');
    });
  }

}
