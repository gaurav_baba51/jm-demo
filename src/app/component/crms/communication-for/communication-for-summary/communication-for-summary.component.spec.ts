import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunicationForSummaryComponent } from './communication-for-summary.component';

describe('CommunicationForSummaryComponent', () => {
  let component: CommunicationForSummaryComponent;
  let fixture: ComponentFixture<CommunicationForSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunicationForSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunicationForSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
