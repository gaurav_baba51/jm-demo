import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { CommunicationForDataService } from '../communication-for-data.service';
import { NewCommunicationForComponent } from '../new-communication-for/new-communication-for.component';

@Component({
  selector: 'app-communication-for-summary',
  templateUrl: './communication-for-summary.component.html',
  styleUrls: ['./communication-for-summary.component.scss']
})
export class CommunicationForSummaryComponent implements OnInit {
  entityTitle = "Communication For";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private communicationForDataService: CommunicationForDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.communicationForDataService.getCommunicationForById(this.id);
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create Communication For',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewCommunicationForComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }


}
