import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { CommunicationForComponent } from './communication-for.component';
import { NewCommunicationForComponent } from './new-communication-for/new-communication-for.component';
import { CommunicationForSummaryComponent } from './communication-for-summary/communication-for-summary.component';


const routes: Routes = [
  {
    path: '',
    component: CommunicationForComponent,
    data: {
      title: 'Communication For'
    }
  },
  {
    path: 'new-communicationFor/:id',
    component: NewCommunicationForComponent ,
    data: {
      title: 'Create New Communication For'
    }
  },
  {
    path: 'communicationFor-summary/:id',
    component: CommunicationForSummaryComponent ,
    data: {
      title: 'Communication For Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunicationForRoutingModule { }
