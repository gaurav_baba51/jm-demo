import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunicationForListComponent } from './communication-for-list.component';

describe('CommunicationForListComponent', () => {
  let component: CommunicationForListComponent;
  let fixture: ComponentFixture<CommunicationForListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunicationForListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunicationForListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
