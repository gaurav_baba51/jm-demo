import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CommunicationForRoutingModule } from './communicationFor-routing.module';
import { CommunicationForSummaryComponent } from './communication-for-summary/communication-for-summary.component';
import { CommunicationForListComponent } from './communication-for-list/communication-for-list.component';
import { NewCommunicationForComponent } from './new-communication-for/new-communication-for.component';
import { CommunicationForComponent } from './communication-for.component';
import { CommunicationForDataService } from './communication-for-data.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CommunicationForRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [
  CommunicationForComponent,
  CommunicationForSummaryComponent,
  CommunicationForListComponent,
  NewCommunicationForComponent],
  providers: [CommunicationForDataService]
})
export class CommunicationForModule { }
