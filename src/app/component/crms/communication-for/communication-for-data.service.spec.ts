import { TestBed, inject } from '@angular/core/testing';

import { CommunicationForDataService } from './communication-for-data.service';

describe('CommunicationForDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommunicationForDataService]
    });
  });

  it('should be created', inject([CommunicationForDataService], (service: CommunicationForDataService) => {
    expect(service).toBeTruthy();
  }));
});
