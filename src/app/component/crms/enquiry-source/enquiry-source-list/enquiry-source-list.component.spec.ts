import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquirySourceListComponent } from './enquiry-source-list.component';

describe('EnquirySourceListComponent', () => {
  let component: EnquirySourceListComponent;
  let fixture: ComponentFixture<EnquirySourceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquirySourceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquirySourceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
