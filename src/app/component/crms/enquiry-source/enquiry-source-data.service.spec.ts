import { TestBed, inject } from '@angular/core/testing';

import { EnquirySourceDataService } from './enquiry-source-data.service';

describe('EnquirySourceDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnquirySourceDataService]
    });
  });

  it('should be created', inject([EnquirySourceDataService], (service: EnquirySourceDataService) => {
    expect(service).toBeTruthy();
  }));
});
