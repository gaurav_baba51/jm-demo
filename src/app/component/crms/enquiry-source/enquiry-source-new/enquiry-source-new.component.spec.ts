import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquirySourceNewComponent } from './enquiry-source-new.component';

describe('EnquirySourceNewComponent', () => {
  let component: EnquirySourceNewComponent;
  let fixture: ComponentFixture<EnquirySourceNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquirySourceNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquirySourceNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
