import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { EnquirySourceNewComponent } from './enquiry-source-new/enquiry-source-new.component';
import { EnquirySourceComponent } from './enquiry-source.component';
import { EnquirySourceSummaryComponent } from './enquiry-source-summary/enquiry-source-summary.component';


const routes: Routes = [
  {
    path: '',
    component: EnquirySourceComponent,
    data: {
      title: 'Enquiry Source'
    }
  },
  {
    path: 'new-enquiry-source/:id',
    component: EnquirySourceNewComponent ,
    data: {
      title: 'Create New Enquiry Source'
    }
  },
  {
    path: 'enquiry-source-summary/:id',
    component: EnquirySourceSummaryComponent ,
    data: {
      title: 'Enquiry Source Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnquirySourceRoutingModule { }
