import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { EnquirySourceNewComponent } from './enquiry-source-new/enquiry-source-new.component';
import { EnquirySourceSummaryComponent } from './enquiry-source-summary/enquiry-source-summary.component';
import { EnquirySourceListComponent } from './enquiry-source-list/enquiry-source-list.component';
import { EnquirySourceComponent } from './enquiry-source.component';
import { EnquirySourceDataService } from './enquiry-source-data.service';
import { EnquirySourceRoutingModule } from './enquirySource-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EnquirySourceRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [
    EnquirySourceComponent,
    EnquirySourceListComponent,
    EnquirySourceSummaryComponent,
    EnquirySourceNewComponent],
  providers: [EnquirySourceDataService]
})
export class EnquirySourceModule { }
