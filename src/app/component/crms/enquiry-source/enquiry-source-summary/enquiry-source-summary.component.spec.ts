import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquirySourceSummaryComponent } from './enquiry-source-summary.component';

describe('EnquirySourceSummaryComponent', () => {
  let component: EnquirySourceSummaryComponent;
  let fixture: ComponentFixture<EnquirySourceSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquirySourceSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquirySourceSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
