import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { EnquirySourceDataService } from '../enquiry-source-data.service';
import { EnquirySourceNewComponent } from '../enquiry-source-new/enquiry-source-new.component';

@Component({
  selector: 'app-enquiry-source-summary',
  templateUrl: './enquiry-source-summary.component.html',
  styleUrls: ['./enquiry-source-summary.component.scss']
})
export class EnquirySourceSummaryComponent implements OnInit {

  entityTitle = "Enquiry Source";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private enquirySourceDataService: EnquirySourceDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.enquirySourceDataService.getEntityById(this.id);
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(EnquirySourceNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

}
