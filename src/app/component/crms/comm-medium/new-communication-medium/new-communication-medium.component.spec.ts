import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCommunicationMediumComponent } from './new-communication-medium.component';

describe('NewCommunicationMediumComponent', () => {
  let component: NewCommunicationMediumComponent;
  let fixture: ComponentFixture<NewCommunicationMediumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCommunicationMediumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCommunicationMediumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
