import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunicationMediumSummaryComponent } from './communication-medium-summary.component';

describe('CommunicationMediumSummaryComponent', () => {
  let component: CommunicationMediumSummaryComponent;
  let fixture: ComponentFixture<CommunicationMediumSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunicationMediumSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunicationMediumSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
