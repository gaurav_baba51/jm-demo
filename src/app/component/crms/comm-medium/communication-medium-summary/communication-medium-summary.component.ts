import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { CommunicationMediumDataService } from '../communication-medium-data.service';
import { NewCommunicationMediumComponent } from '../new-communication-medium/new-communication-medium.component';

@Component({
  selector: 'app-communication-medium-summary',
  templateUrl: './communication-medium-summary.component.html',
  styleUrls: ['./communication-medium-summary.component.scss']
})
export class CommunicationMediumSummaryComponent implements OnInit {
  entityTitle = 'Communication Medium';
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private communicationMediumDataService: CommunicationMediumDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.communicationMediumDataService.getCommunicationMediumById(this.id);
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create '+ this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewCommunicationMediumComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

}
