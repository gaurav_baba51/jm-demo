import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunicationMediumListComponent } from './communication-medium-list.component';

describe('CommunicationMediumListComponent', () => {
  let component: CommunicationMediumListComponent;
  let fixture: ComponentFixture<CommunicationMediumListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunicationMediumListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunicationMediumListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
