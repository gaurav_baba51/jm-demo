import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { NewCommunicationMediumComponent } from '../new-communication-medium/new-communication-medium.component';
import { CommunicationMediumDataService } from '../communication-medium-data.service';

@Component({
  selector: 'app-communication-medium-list',
  templateUrl: './communication-medium-list.component.html',
  styleUrls: ['./communication-medium-list.component.scss']
})
export class CommunicationMediumListComponent implements OnInit {

  entities: any;
  entityName: any;
  bsModalRef: BsModalRef;
  paginatedEntity;
  entityTitle = 'Communication Medium';


  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);

  }

  doPagination(start, end){
    this.paginatedEntity = this.entities.slice(start, end);
  }

  getAllBranches(start, end){
    this.apiService.getAllEntities("commmedium").then(result => {
      this.entities = result;
      this.communicationMediumDataService.setAllCommunicationMedium(result);
      this.totalItems = this.entities.length;
      this.doPagination(start, end); 
    });
  }

  navigateToRoute(entity) {
    var id = entity != undefined ? entity.id : '1';
    this.router.navigate(['dashboard/communicationMedium/new-communicationMedium', id]);
  }

  navigateToEntitySummary(entity) {
    var id = entity != undefined ? entity.entity_key_id : '1';
    this.router.navigate(['dashboard/communicationMedium/communicationMedium-summary', id]);
  }


  constructor(private router: Router,
    private apiService: ApiService,
    private commonDataService : CommonDataService,
    private communicationMediumDataService: CommunicationMediumDataService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.paginatedEntity = [];
    this.getAllBranches(this.currentStartIndex, this.currentEndIndex);
  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewCommunicationMediumComponent , initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      console.log("modal close: " + reason);
      this.getAllBranches(this.currentStartIndex, this.currentEndIndex);

    })
  }
}
