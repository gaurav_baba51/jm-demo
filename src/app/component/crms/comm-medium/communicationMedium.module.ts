import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CommunicationMediumRoutingModule } from './communicationMedium-routing.module';
import { CommunicationMediumComponent } from './communication-medium.component';
import { CommunicationMediumSummaryComponent } from './communication-medium-summary/communication-medium-summary.component';
import { NewCommunicationMediumComponent } from './new-communication-medium/new-communication-medium.component';
import { CommunicationMediumDataService } from './communication-medium-data.service';
import { CommunicationMediumListComponent } from './comm-medium-list/communication-medium-list.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CommunicationMediumRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [ 
    CommunicationMediumComponent,
    CommunicationMediumSummaryComponent,
    CommunicationMediumListComponent,
    NewCommunicationMediumComponent
],
  providers: [CommunicationMediumDataService]
})
export class CommunicationMediumModule { }
