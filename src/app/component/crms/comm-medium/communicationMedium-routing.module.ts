import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { CommunicationMediumComponent } from './communication-medium.component';
import { NewCommunicationMediumComponent } from './new-communication-medium/new-communication-medium.component';
import { CommunicationMediumSummaryComponent } from './communication-medium-summary/communication-medium-summary.component';


const routes: Routes = [
  {
    path: '',
    component: CommunicationMediumComponent,
    data: {
      title: 'Communication Medium'
    }
  },
  {
    path: 'new-communicationMedium/:id',
    component: NewCommunicationMediumComponent ,
    data: {
      title: 'Create Communication Medium'
    }
  },
  {
    path: 'communicationMedium-summary/:id',
    component: CommunicationMediumSummaryComponent ,
    data: {
      title: 'Communication Medium Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunicationMediumRoutingModule { }
