import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunicationMediumComponent } from './communication-medium.component';

describe('CommunicationMediumComponent', () => {
  let component: CommunicationMediumComponent;
  let fixture: ComponentFixture<CommunicationMediumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunicationMediumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunicationMediumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
