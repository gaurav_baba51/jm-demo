import { TestBed, inject } from '@angular/core/testing';

import { CommunicationMediumDataService } from './communication-medium-data.service';

describe('CommunicationMediumDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommunicationMediumDataService]
    });
  });

  it('should be created', inject([CommunicationMediumDataService], (service: CommunicationMediumDataService) => {
    expect(service).toBeTruthy();
  }));
});
