import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommunicationMediumDataService {

  entities;

  constructor() { }


  getAllCommunicationMedium() {
    return this.entities;
  }

  setAllCommunicationMedium(entities) {
    this.entities = entities;
  }

  getCommunicationMediumById(id) {
    for (var i in this.entities) {
      if (this.entities[i].entity_key_id == id) {
        return this.entities[i];
      }
    }
  }
}
