import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeDataService {

  constructor() { }

  contacts: any = [];
  branches: any = [];
  designations: any = [];
  employees: any = [];
  cities: any = [];

  getCities() {
    return this.cities;
  }

  setCities(cities) {
    this.cities = cities;
  }

  getCityByCityId(id) {
    for (var employee of this.cities) {
      if (employee.entity_key_id == id) {
        return employee;
      }
    }
  }

  getEmployees() {
    return this.employees;
  }

  setEmployees(employees) {
    this.employees = employees;
  }

  getEmployeeByEmployeeId(id) {
    for (var employee of this.employees) {
      if (employee.entity_key_id == id) {
        return employee;
      }
    }
  }

  getContacts() {
    return this.contacts;
  }

  setContacts(contacts) {
    this.contacts = contacts;
  }

  getContactByContactId(id) {
    for (var employee of this.contacts) {
      if (employee.entity_key_id == id) {
        return employee;
      }
    }
  }

  getBranches() {
    return this.branches;
  }

  setBranches(branches) {
    this.branches = branches;
  }

  getBranchByBranchId(id) {
    for (var employee of this.branches) {
      if (employee.entity_key_id == id) {
        return employee;
      }
    }
  }

  getDesignations() {
    return this.designations;
  }

  setDesignations(designations) {
    this.designations = designations;
  }

  getDesigntionByDesignationId(id) {
    for (var employee of this.designations) {
      if (employee.entity_key_id == id) {
        return employee;
      }
    }
  }
}
