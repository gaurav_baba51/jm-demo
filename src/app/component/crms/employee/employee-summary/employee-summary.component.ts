import { Component, OnInit } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NewEmployeeComponent } from '../new-employee/new-employee.component';
import { EmployeeDataService } from '../employee-data.service';
import { CommonDataService } from '../../../../service/common-data.service';

@Component({
  selector: 'app-employee-summary',
  templateUrl: './employee-summary.component.html',
  styleUrls: ['./employee-summary.component.scss']
})
export class EmployeeSummaryComponent implements OnInit {

  id: any;
  employee: any;

  bsModalRef: BsModalRef;

  employeeName: any;
  managerName: any;
  designation: any;
  branch: any;
  dateOfJoining: any;

  constructor(private route: ActivatedRoute,
    private apiService: ApiService,
    private modalService: BsModalService,
    private employeeDataService: EmployeeDataService,
    private commonDataService: CommonDataService,
    private router: Router) {
    this.id = this.route.snapshot.params['id'];

    this.fetchData(this.id);
  }

  fetchData(id) {
    this.employee = this.employeeDataService.getEmployeeByEmployeeId(id);
    if (this.employee != undefined) {
      this.getEmployeeName(this.employee.personid);
      this.dateOfJoining = this.getDate(this.employee.dateofjoining);
      this.setDesignation(this.employee.designation);
    } else {
      this.commonDataService.getEmployees().then(employees => {
        this.employeeDataService.setEmployees(employees);
        this.employee = this.employeeDataService.getEmployeeByEmployeeId(id);
        this.getEmployeeName(this.employee.personid);
        this.dateOfJoining = this.getDate(this.employee.dateofjoining);
        this.setDesignation(this.employee.designation);
      });
    }
  }

  getEmployeeName(id) {
    var contact = this.employeeDataService.getContactByContactId(id);
    if (contact != undefined) {
      this.employeeName = contact.fname + ' '+ contact.lname;
      this.getManagerName(this.employee.reportingto);
    } else {
      this.commonDataService.getContacts().then(contacts => {
        this.employeeDataService.setContacts(contacts);
        contact = this.employeeDataService.getContactByContactId(id);
        this.employeeName = contact.fname + ' '+ contact.lname;
        this.getManagerName(this.employee.reportingto);
      });
    }
  }

  getManagerName(id) {
    var contact = this.employeeDataService.getContactByContactId(id);
    if (contact != undefined) {
      this.managerName = contact.fname + ' '+ contact.lname;
    } else {
      this.commonDataService.getContacts().then(contacts => {
        this.employeeDataService.setContacts(contacts);
        contact = this.employeeDataService.getContactByContactId(id);
        this.managerName = contact.fname + ' '+ contact.lname;
      });
    }
  }

  setDesignation(id) {
    var designation = this.employeeDataService.getDesigntionByDesignationId(id);
    if (designation != undefined) {
      this.designation = designation.desgName;
      this.getBranch();
    } else {
      this.commonDataService.getDesignations().then(designations => {
        this.employeeDataService.setDesignations(designations);
        designation = this.employeeDataService.getDesigntionByDesignationId(id);
        this.designation = designation.desgName;
        this.getBranch();
      });
    }
  }

  getBranch(){
    var branch = this.employeeDataService.getBranchByBranchId(this.employee.branch);
    if (branch != undefined) {
      this.getCity(branch);
    } else {
      this.commonDataService.getBranches().then(branches => {
        this.employeeDataService.setBranches(branches);
        branch = this.employeeDataService.getBranchByBranchId(this.employee.branch);
        this.getCity(branch);
      });
    }
  }

  getCity(branch) {
    var city = this.employeeDataService.getCityByCityId(branch.location);
    if (city != undefined) {
      this.branch = branch.title + ', ' + city.cityName;
    } else {
      this.commonDataService.getCities().then(cities => {
        this.employeeDataService.setCities(cities);
        city = this.employeeDataService.getCityByCityId(branch.location);
        this.branch = branch.title + ', ' + city.cityName;
      });
    }
  }

  ngOnInit() {
  }

  getDate(date: any) {
    var dateVar = new Date(date);
    return dateVar.getDate() + '-' + dateVar.getMonth() + '-' + dateVar.getFullYear();
  }

  openModalWithComponent() {
    const initialState = {
      data: this.employee,
      title: 'Edit Employee ',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewEmployeeComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

}
