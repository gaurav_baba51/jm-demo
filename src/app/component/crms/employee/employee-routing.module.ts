import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { NewEmployeeComponent } from './new-employee/new-employee.component';
import { EmployeeSummaryComponent } from './employee-summary/employee-summary.component';
import { EmployeeComponent } from './employee.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeeComponent,
    data: {
      title: 'Enquiries'
    }
  },
  {
    path: 'new-employee/:id',
    component: NewEmployeeComponent ,
    data: {
      title: 'Create New Enquiry'
    }
  },
  {
    path: 'employee-summary/:id',
    component: EmployeeSummaryComponent ,
    data: {
      title: 'Enquiry Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
