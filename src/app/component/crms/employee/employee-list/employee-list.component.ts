import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { EmployeeDataService } from '../employee-data.service';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NewEmployeeComponent } from '../new-employee/new-employee.component';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {


  bsModalRef: BsModalRef;
  employees: any;

  public rows: Array<any> = [];
  public columns: Array<any> = [
    { title: 'Id', name: 'id', sort: 'asc' },
    { title: 'Name', name: 'name', sort: false },
    { title: 'Date Of Joining', name: 'date', sort: false },
    { title: 'Designation', name: 'designation', sort: false },
    { title: 'Manager', name: 'manager', sort: false },
    { title: 'Branch', name: 'branch', sort: false }
  ];

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  data: Array<any> = [];

  constructor(private router: Router,
    private apiService: ApiService,
    private commonDataService: CommonDataService,
    private employeeDataService: EmployeeDataService,
    private modalService: BsModalService) {
    this.fetchData();
  }

  ngOnInit() {
  }

  private fetchData() {
    this.commonDataService.getEmployees().then(employees => {
      this.employeeDataService.setEmployees(employees);
      this.commonDataService.getContacts().then(contacts => {
        this.employeeDataService.setContacts(contacts);
        this.commonDataService.getBranches().then(branches => {
          this.employeeDataService.setBranches(branches);
          this.commonDataService.getDesignations().then(designations => {
            this.employeeDataService.setDesignations(designations);
            this.commonDataService.getCities().then(cities => {
              this.employeeDataService.setCities(cities);
              this.employees = employees;
              this.loadData();
              this.length = this.data.length;
              this.onChangeTable(this.config);
            });
          });
        });
      });
    });
  }

  loadData() {
    var temp = [];
    this.employees.forEach((employee, index) => {
      var person = this.employeeDataService.getContactByContactId(employee.personid);
      var designation = this.employeeDataService.getDesigntionByDesignationId(employee.designation);
      var managerEmployee = this.employeeDataService.getEmployeeByEmployeeId(employee.reportingto);
      var manager = this.employeeDataService.getContactByContactId(managerEmployee.personid);
      var branch = this.employeeDataService.getBranchByBranchId(employee.branch);
      var city = this.employeeDataService.getCityByCityId(branch.location);
      var emp = {
        'id': employee.entity_key_id,
        'name': person.fname + ' ' + person.lname,
        'date': employee.dateofjoining != undefined ? this.getDate(employee.dateofjoining) : '',
        'designation': designation.desgName,
        'manager': manager.fname + ' ' + manager.lname,
        'branch': branch.title + ', ' + city.cityName
      };
      temp.push(emp);
      if (index == this.employees.length - 1) {
        this.data = temp;
        this.data = [...this.data];
      }
    });
  }

  getDate(date: any) {
    var dateVar = new Date(date);
    return dateVar.getDate() + '-' + dateVar.getMonth() + '-' + dateVar.getFullYear();
  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New Contact',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewEmployeeComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      console.log("modal close: " + reason);
      this.fetchData();
      this.loadData();
    });
  }

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    var id = data.row.id != undefined ? data.row.id : '1';
    this.router.navigate(['dashboard/employee/employee-summary', id]);
  }

}
