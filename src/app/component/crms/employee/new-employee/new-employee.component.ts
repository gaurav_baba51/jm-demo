import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeDataService } from '../employee-data.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';

@Component({
  selector: 'app-new-employee',
  templateUrl: './new-employee.component.html',
  styleUrls: ['./new-employee.component.scss']
})
export class NewEmployeeComponent implements OnInit {

  employee: any;
  modalRef: BsModalRef;
  contacts: any;
  designations: any;
  employees: any;
  branches: any;
  isEdit: Boolean;

  constructor(private route: ActivatedRoute,
    private employeeDataService: EmployeeDataService,
    private router: Router,
    public bsModalRef: BsModalRef,
    private apiService: ApiService,
    private commonDataService: CommonDataService,
    public modalService: BsModalService,
    private toastr: ToastrService) {
      debugger;
      this.employee = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};

      if (this.employee.entity_key_id != undefined) {
        this.isEdit = true;
      }

      this.contacts = this.employeeDataService.getContacts();
      this.designations = this.employeeDataService.getDesignations();
      this.employees = this.employeeDataService.getEmployees();
      this.branches = this.employeeDataService.getBranches();
    }

  ngOnInit() {
  }

  submit(employee) {
    if (employee.entity_key_id != undefined) {
      this.update(employee);
    } else {
      this.create(employee);
    }
  }

  update(user) {
    user.entity_name = 'person';
    this.apiService.updateEntity(user).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Contact Updated Successfully');
      } else {
        this.toastr.error('Failed to Update Contact');
      }
    }).catch(error => {
      this.toastr.error('Failed to update Contact');
    });
  }

  create(employee) {
    employee.entity_name = 'employee';
    this.apiService.addEntity(employee).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Employee Added Successfully');
      } else {
        this.toastr.error('Failed to add Employee');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Employee');
    });
  }

  getEmployeeNameByEmployeeId(id) {
    var employee = this.employeeDataService.getEmployeeByEmployeeId(id);
    var contact = this.employeeDataService.getContactByContactId(employee.personid);
    return contact.fname + ' ' + contact.lname;
  }

  getContactNameByContactId(id) {
    var contact = this.employeeDataService.getContactByContactId(id);
    return contact.fname + ' ' + contact.lname;
  }

  getBranchTitleAndLocationById(id) {
    var branch = this.employeeDataService.getBranchByBranchId(id);
    var city = this.employeeDataService.getCityByCityId(branch.location);
    return branch.title + ', ' + city.cityName;
  }

}
