import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { Ng2TableModule } from 'ngx-datatable/ng2-table';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgPipesModule } from 'ng-pipes';
import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeComponent } from './employee.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeSummaryComponent } from './employee-summary/employee-summary.component';
import { NewEmployeeComponent } from './new-employee/new-employee.component';
import { EmployeeDataService } from './employee-data.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EmployeeRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    Ng2TableModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  declarations: [EmployeeComponent, EmployeeListComponent, EmployeeSummaryComponent, NewEmployeeComponent],
  providers: [EmployeeDataService]
})
export class EmployeeModule { }
