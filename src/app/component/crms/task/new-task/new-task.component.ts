import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { CommonDataService } from '../../../../service/common-data.service';
import { TaskDataService } from '../task-data.service';
import { ApiService } from '../../../../service/api.service';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss']
})
export class NewTaskComponent implements OnInit {

  task: any;
  employees: any;

  modalRef: BsModalRef;

  constructor(public bsModalRef: BsModalRef,
    private commonDataService: CommonDataService,
    public modalService: BsModalService,
    private toastr: ToastrService,
    private taskDataService: TaskDataService,
    private apiService: ApiService) {
      this.task = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};

      this.employees = this.taskDataService.getEmployees();
    }

  ngOnInit() {
  }

  submit(task) {
    if (task.entity_key_id != undefined) {
      this.update(task);
    } else {
      this.create(task);
    }
  }

  update(task) {
    task.entity_name = 'task';
    this.apiService.updateEntity(task).then(result => {
      if (result === true) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Task Updated Successfully');
      } else {
        this.toastr.error('Failed to Update Contact');
      }
    }).catch(error => {
      this.toastr.error('Failed to update Contact');
    });
  }

  create(task) {
    task.entity_name = 'task';
    this.apiService.addEntity(task).then(result => {
      if (result === true) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Contact Added Successfully');
      } else {
        this.toastr.error('Failed to add Contact');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Contact');
    });
  }

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  getEmployeeNameById(employeeId) {
    var employee = this.taskDataService.getContactByContactId(employeeId);
    return employee.fname + ' ' + employee.lname;
  }

}
