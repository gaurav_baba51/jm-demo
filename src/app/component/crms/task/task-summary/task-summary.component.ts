import { Component, OnInit } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { NewTaskComponent } from '../new-task/new-task.component';
import { CommonDataService } from '../../../../service/common-data.service';
import { TaskDataService } from '../task-data.service';

@Component({
  selector: 'app-task-summary',
  templateUrl: './task-summary.component.html',
  styleUrls: ['./task-summary.component.scss']
})
export class TaskSummaryComponent implements OnInit {

  id: any;
  task: any;
  bsModalRef: BsModalRef;
  employee;

  constructor(private route: ActivatedRoute, private taskDataService: TaskDataService,
    private modalService: BsModalService, private commonDataService: CommonDataService) {
    this.id = this.route.snapshot.params['id'];
    this.task = this.taskDataService.getTaskByTaskId(this.id);

    if (this.task == undefined) {
      this.commonDataService.getTasks().then(result => {
        this.commonDataService.getEmployees().then(result1 => {
          this.taskDataService.setEmployees(result1);
          this.commonDataService.getContacts().then(result2 => {
            this.taskDataService.setContacts(result2);
            this.taskDataService.setTasks(result);
            this.task = this.taskDataService.getTaskByTaskId(this.id);
            this.getEmployeesNameByEmployeeId(this.task.assignto);
          });
        });
      });
    }
  }

  ngOnInit() { }

  openModalWithComponent() {
    const initialState = {
      data: this.task,
      title: 'Update Task',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewTaskComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  getEmployeesNameByEmployeeId(employeeId) {
    var employee = this.taskDataService.getEmployeeByEmployeeId(employeeId);
    var contact = this.taskDataService.getContactByContactId(employee.personid);
    this.employee =  contact.fname + ' ' + contact.lname;
  }
}
