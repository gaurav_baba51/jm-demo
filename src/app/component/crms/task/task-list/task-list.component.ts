import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';

import { NewTaskComponent } from '../new-task/new-task.component';
import { CommonDataService } from '../../../../service/common-data.service';
import { TaskDataService } from '../task-data.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  bsModalRef: BsModalRef;
  paginatedEnquiries;
  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;

  tasks: any;

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);
  }

  doPagination(start, end) {
    this.paginatedEnquiries = this.tasks.slice(start, end);
  }

  constructor(private router: Router, private modalService: BsModalService,
    private commonDataService: CommonDataService, 
    private taskDataService: TaskDataService) { }

  ngOnInit() {
    this.currentEndIndex = this.pageSize;
    this.currentStartIndex = 0;
    this.getTasks(this.currentStartIndex, this.currentEndIndex);

    this.commonDataService.getEmployees().then(result => {
      this.taskDataService.setEmployees(result);
    });
    this.commonDataService.getContacts().then(result => {
      this.taskDataService.setContacts(result);
    });
  }

  getTasks(start, end) {
    this.commonDataService.getTasks().then(res => {
      this.tasks = res;
      this.taskDataService.setTasks(this.tasks);
      this.totalItems = this.tasks.length;
      console.log("total records:" + this.totalItems);
      this.doPagination(start, end); 
    });
  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New Enquiry',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewTaskComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  navigateToTaskSummary(task) {
    var id = task != undefined ? task.entity_key_id : ':id';
    this.router.navigate(['dashboard/tasks/task-summary', id]);
  }

  getEmployeeNameById(employeeId) {
    var employee = this.taskDataService.getEmployeeByEmployeeId(employeeId);
    var contact = this.taskDataService.getContactByContactId(employee.personid);
    return contact.fname + ' ' + contact.lname;
  }

}
