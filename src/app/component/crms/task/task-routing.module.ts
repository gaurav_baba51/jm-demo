import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';


import { TaskListComponent } from './task-list/task-list.component';
import { NewTaskComponent } from './new-task/new-task.component';
import { TaskSummaryComponent } from './task-summary/task-summary.component';
import { TaskComponent } from './task.component';
import { TaskDataService } from './task-data.service';

const routes: Routes = [
  {
    path: '',
    component: TaskComponent,
    data: {
      title: 'Tasks'
    }
  },
  {
    path: 'new-task/:id',
    component: NewTaskComponent ,
    data: {
      title: 'Create New Task'
    }
  },
  {
    path: 'task-summary/:id',
    component: TaskSummaryComponent ,
    data: {
      title: 'Task Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskRoutingModule { }
