import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TaskDataService {

  employees: any = [];
  tasks: any = [];
  contacts: any = [];

  constructor() { }


  getEmployees() {
    return this.employees;
  }

  setEmployees(employees) {
    this.employees = employees;
  }

  getTasks() {
    return this.tasks;
  }

  setTasks(tasks) {
    this.tasks = tasks;
  }

  getContacts() {
    return this.contacts;
  }

  setContacts(contacts) {
    this.contacts = contacts;
  }

  getContactByContactId(contactId) {
    for (var contact of this.contacts) {
      if (contact.entity_key_id == contactId) {
        return contact;
      }
    }
  }

  getTaskByTaskId(taskId) {
    for (var task of this.tasks) {
      if (task.entity_key_id == taskId) {
        return task;
      }
    }
  }

  getEmployeeByEmployeeId(employeId) {
    for (var employee of this.employees) {
      if (employee.entity_key_id == employeId) {
        return employee;
      }
    }
  }
}
