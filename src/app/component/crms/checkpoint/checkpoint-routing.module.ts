import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { CheckpointComponent } from './checkpoint.component';
import { CheckpointNewComponent } from './checkpoint-new/checkpoint-new.component';
import { CheckpointSummaryComponent } from './checkpoint-summary/checkpoint-summary.component';


const routes: Routes = [
  {
    path: '',
    component: CheckpointComponent,
    data: {
      title: 'Checkpoint'
    }
  },
  {
    path: 'new-checkpoint/:id',
    component: CheckpointNewComponent ,
    data: {
      title: 'Create New Checkpoint'
    }
  },
  {
    path: 'checkpoint-summary/:id',
    component: CheckpointSummaryComponent ,
    data: {
      title: 'Checkpoint Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckpointRoutingModule { }
