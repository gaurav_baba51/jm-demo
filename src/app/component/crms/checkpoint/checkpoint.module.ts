import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CheckpointRoutingModule } from './checkpoint-routing.module';
import { CheckpointNewComponent } from './checkpoint-new/checkpoint-new.component';
import { CheckpointDataService } from './checkpoint-data.service';
import { CheckpointSummaryComponent } from './checkpoint-summary/checkpoint-summary.component';
import { CheckpointListComponent } from './checkpoint-list/checkpoint-list.component';
import { CheckpointComponent } from './checkpoint.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CheckpointRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [
    CheckpointComponent,
    CheckpointListComponent,
    CheckpointSummaryComponent,
    CheckpointNewComponent],
  providers: [CheckpointDataService]
})
export class CheckpointModule { }
