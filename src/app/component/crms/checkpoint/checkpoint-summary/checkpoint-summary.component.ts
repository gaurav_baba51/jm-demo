import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { CheckpointDataService } from '../checkpoint-data.service';
import { CheckpointNewComponent } from '../checkpoint-new/checkpoint-new.component';

@Component({
  selector: 'app-checkpoint-summary',
  templateUrl: './checkpoint-summary.component.html',
  styleUrls: ['./checkpoint-summary.component.scss']
})
export class CheckpointSummaryComponent implements OnInit {
  entityTitle = "Process Status";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private checkpointDataService: CheckpointDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.checkpointDataService.getEntityById(this.id);
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(CheckpointNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }


}
