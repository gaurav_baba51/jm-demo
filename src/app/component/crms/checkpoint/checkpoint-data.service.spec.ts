import { TestBed, inject } from '@angular/core/testing';

import { CheckpointDataService } from './checkpoint-data.service';

describe('CheckpointDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckpointDataService]
    });
  });

  it('should be created', inject([CheckpointDataService], (service: CheckpointDataService) => {
    expect(service).toBeTruthy();
  }));
});
