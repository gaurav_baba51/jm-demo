import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckpointNewComponent } from './checkpoint-new.component';

describe('CheckpointNewComponent', () => {
  let component: CheckpointNewComponent;
  let fixture: ComponentFixture<CheckpointNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckpointNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckpointNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
