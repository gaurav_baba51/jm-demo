import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommunicationDataService {
  communications: any = [];
  communicationFors: any = [];
  contacts: any = [];
  communicationMediums: any = []; 
  enquiries: any = [];
  communicationStatuses: any = [];
  processStatuses: any = [];
  
  constructor() { }

  getProcessStatuses() {
    return this.processStatuses;
  }

  setProcessStatuses(processStatuses) {
    this.processStatuses = processStatuses;
  }

  getProcessStatusById(processStatusId) {
    for (var processStatus of this.processStatuses) {
      if (processStatus.entity_key_id == processStatusId) {
        return processStatus;
      }
    }
  }

  getCommunicationMediums() {
    return this.communicationMediums;
  }

  setCommunicationMediums(communicationMediums) {
    this.communicationMediums = communicationMediums;
  }

  getCommunicationMediumByCommunicationMediumId(communicationMediumId) {
    for (var communicationMedium of this.communicationMediums) {
      if (communicationMedium.entity_key_id == communicationMediumId) {
        return communicationMedium;
      }
    }
  }

  getEnquiries() {
    return this.enquiries;
  }

  setEnquiries(enquiries) {
    this.enquiries = enquiries;
  }

  getEnquiryByEnquiryId(enquiryId) {
    for (var enquiry of this.enquiries) {
      if (enquiry.entity_key_id == enquiryId) {
        return enquiry;
      }
    }
  }

  getCommunicationStatuses() {
    return this.communicationStatuses;
  }

  setCommunicationStatuses(communicationStatuses) {
    this.communicationStatuses = communicationStatuses;
  }

  getCommunicationStatusesById(communicationStatusId) {
    for (var communicationStatus of this.communicationStatuses) {
      if (communicationStatus.entity_key_id == communicationStatusId) {
        return communicationStatus;
      }
    }
  }

  getCommunicationFors() {
    return this.communicationFors;
  }

  setCommunicationFors(communicationFors) {
    this.communicationFors = communicationFors;
    console.log(this.communicationFors);
  }

  getCommunicationForByCommunicationForId(communicationForId) {
    for (var communicationFor of this.communicationFors) {
      if (communicationFor.entity_key_id == communicationForId) {
        return communicationFor;
      }
    }
  }

   getContacts() {
    return this.contacts;
  }

  setContacts(contacts) {
    this.contacts = contacts;
  }

  getContactByMobileNumber(mobileNumber) {
    for (var contact of this.contacts) {
      if (contact.mobno == mobileNumber) {
        return contact;
      }
    }
  }

  getContactByContactId(contactId) {
    for (var contact of this.contacts) {
      if (contact.entity_key_id == contactId) {
        return contact;
      }
    }
  }

  getCommunications() {
    return this.communications;
  }

  setCommunications(communications) {
    this.communications = communications;
  }

  getCommunicationByCommunicationId(communicationId) {
    for (var communication of this.communications) {
      if (communication.entity_key_id == communicationId) {
        return communication;
      }
    }
  }
}
