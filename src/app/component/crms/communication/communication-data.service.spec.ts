import { TestBed, inject } from '@angular/core/testing';

import { CommunicationDataService } from './communication-data.service';

describe('CommunicationDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommunicationDataService]
    });
  });

  it('should be created', inject([CommunicationDataService], (service: CommunicationDataService) => {
    expect(service).toBeTruthy();
  }));
});
