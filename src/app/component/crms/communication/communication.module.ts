import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CollapseModule } from 'ngx-bootstrap';

import { NewCommunicationComponent } from './new-communication/new-communication.component';
import { CommunicationListComponent } from './communication-list/communication-list.component';
import { CommunicationSummaryComponent } from './communication-summary/communication-summary.component';
import { CommunicationComponent } from './communication.component';
import { CommunicationRoutingModule } from './communication-routing.module';
import { CommunicationDataService } from './communication-data.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CommunicationRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [
    CommunicationComponent,
    CommunicationListComponent,
    NewCommunicationComponent,
    CommunicationSummaryComponent],
  providers: [CommunicationDataService] 
})
export class CommunicationModule { }
