import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';

import { NewCommunicationComponent } from '../new-communication/new-communication.component';
import { CommonDataService } from '../../../../service/common-data.service';
import { CommunicationDataService } from '../communication-data.service';

@Component({
  selector: 'app-communication-list',
  templateUrl: './communication-list.component.html',
  styleUrls: ['./communication-list.component.scss']
})
export class CommunicationListComponent implements OnInit {

  communications: any;
  bsModalRef: BsModalRef;
  paginatedEnquiries;

  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);
  }

  doPagination(start, end) {
    this.paginatedEnquiries = this.communications.slice(start, end);
  }

  constructor(private router: Router, private modalService: BsModalService,
    private commonDataService: CommonDataService, private communicationDataService: CommunicationDataService) { }

  ngOnInit() {
    this.currentEndIndex = this.pageSize;
    this.currentStartIndex = 0;
    this.getCommunications(this.currentStartIndex, this.currentEndIndex);
    this.commonDataService.getCommunicationFors().then(result => {
      this.communicationDataService.setCommunicationFors(result);
    });
    this.commonDataService.getContacts().then(result => {
      this.communicationDataService.setContacts(result);
    });
    this.commonDataService.getCommunicationMediums().then(result => {
      this.communicationDataService.setCommunicationMediums(result);
    });
    this.commonDataService.getProcessStatus().then(result => {
      this.communicationDataService.setCommunicationStatuses(result);
    });
    this.commonDataService.getEnquiries().then(result => {
      this.communicationDataService.setEnquiries(result);
    });
    this.commonDataService.getProcessStatus().then(result => {
      this.communicationDataService.setProcessStatuses(result);
    });
  }

  getCommunications(start, end) {
    this.commonDataService.getCommunications().then(res => {
      this.communications = res;
      this.communicationDataService.setCommunications(this.communications);
      this.totalItems = this.communications.length;
      console.log("total records:" + this.totalItems);
      this.doPagination(start, end); 
    });
  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New Enquiry',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewCommunicationComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  getCommunicationForById(communicationForId) {
    return this.communicationDataService.getCommunicationForByCommunicationForId(communicationForId).commfor;
  }

  getContactById(contactId) {
    var contact = this.communicationDataService.getContactByContactId(contactId);
    return contact.fname + ' ' + contact.lname;
  }

  navigateToCommunicationSummary(communication) {
    var id = communication != undefined ? communication.entity_key_id : ':id';
    this.router.navigate(['dashboard/communications/communication-summary', id]);
  }

}
