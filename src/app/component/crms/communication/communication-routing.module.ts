import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { NewCommunicationComponent } from './new-communication/new-communication.component';
import { CommunicationSummaryComponent } from './communication-summary/communication-summary.component';
import { CommunicationComponent } from './communication.component';

const routes: Routes = [
  {
    path: '',
    component: CommunicationComponent,
    data: {
      title: 'Communications'
    }
  },
  {
    path: 'new-communication/:id',
    component: NewCommunicationComponent ,
    data: {
      title: 'Create New Communication'
    }
  },
  {
    path: 'communication-summary/:id',
    component: CommunicationSummaryComponent ,
    data: {
      title: 'Comunication Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunicationRoutingModule { }
