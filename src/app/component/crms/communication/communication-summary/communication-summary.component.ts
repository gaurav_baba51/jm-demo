import { Component, OnInit } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { CommonDataService } from '../../../../service/common-data.service';
import { NewCommunicationComponent } from '../new-communication/new-communication.component';
import { CommunicationDataService } from '../communication-data.service';
import { ApiService } from '../../../../service/api.service';

@Component({
  selector: 'app-communication-summary',
  templateUrl: './communication-summary.component.html',
  styleUrls: ['./communication-summary.component.scss']
})
export class CommunicationSummaryComponent implements OnInit {

  id: any;
  communication: any;
  bsModalRef: BsModalRef;
  communicationFor;
  contact;
  communicationMedium;

  constructor(private apiService: ApiService,private route: ActivatedRoute, private modalService: BsModalService,
    private communicationDataService: CommunicationDataService, private commonDataService: CommonDataService) {

    }

  ngOnInit() {
       
    this.id = this.route.snapshot.params['id'];
    this.apiService.getEntityFromId('communication',this.id).then(result => {
      this.communication = result;
      console.log(this.communication);
    });
  }

  openModalWithComponent() {
    const initialState = {
      data: this.communication,
      title: 'Update Communication',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewCommunicationComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  getCommunicationForById(communicationForId) {
    if (this.communicationDataService.getCommunicationFors().length <= 0) {
      this.commonDataService.getCommunicationFors().then(result => {
        this.communicationDataService.setCommunicationFors(result);
        this.communicationFor = this.communicationDataService.getCommunicationForByCommunicationForId(communicationForId).commfor;
        this.getContactById(this.communication.personid);
      });
    }else{
      this.communicationFor = this.communicationDataService.getCommunicationForByCommunicationForId(communicationForId).commfor;
      this.getContactById(this.communication.personid);
    }
  }

  getContactById(contactId) {
    if (this.communicationDataService.getContacts().length <= 0) {
      this.commonDataService.getContacts().then(result => {
        this.communicationDataService.setContacts(result);
        var contact = this.communicationDataService.getContactByContactId(contactId);
        this.contact = contact.fname + ' ' + contact.lname;
        this.getCommunicationMediumById(this.communication.medium);
      });
    }else{
      var contact = this.communicationDataService.getContactByContactId(contactId);
      this.contact = contact.fname + ' ' + contact.lname;
      this.getCommunicationMediumById(this.communication.medium);
    }
  }

  getCommunicationMediumById(communicationMediumId) {
    if (this.communicationDataService.getCommunicationMediums().length <= 0) {
      this.commonDataService.getCommunicationMediums().then(result => {
        this.communicationDataService.setCommunicationMediums(result);
        var medium = this.communicationDataService.getCommunicationMediumByCommunicationMediumId(communicationMediumId);
        this.communicationMedium = medium != undefined ? medium.commMedium : undefined;
      });
    }else{
      var medium = this.communicationDataService.getCommunicationMediumByCommunicationMediumId(communicationMediumId);
      this.communicationMedium = medium != undefined ? medium.commMedium : undefined;
    }
  }

  getCommunicationStatus(communicationStatusById) {
    if (this.communicationDataService.getCommunicationStatuses().length <= 0) {
      this.commonDataService.getProcessStatus().then(result => {
        this.communicationDataService.setCommunicationStatuses(result);
      });
    }
    return this.communicationDataService.getCommunicationStatusesById(communicationStatusById) != undefined ? 
    this.communicationDataService.getCommunicationStatusesById(communicationStatusById).statusval :undefined;
  }
}
