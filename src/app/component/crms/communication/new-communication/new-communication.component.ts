import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { CommunicationDataService } from '../communication-data.service';

@Component({
  selector: 'app-new-communication',
  templateUrl: './new-communication.component.html',
  styleUrls: ['./new-communication.component.scss']
})
export class NewCommunicationComponent implements OnInit {

  modalRef: BsModalRef;
  isSummaryCollapsed: boolean = false;
  isContactCollapsed: boolean = true;

  communication: any;
  enquiries: any;
  communicationFors: any;
  communicationMediums: any;
  contacts: any;
  user: any;
  isEditMode: any;
  contactFound: any;
  isEnquiry: boolean;
  enquiry: any;
  processStatuses: any;

  constructor(public bsModalRef: BsModalRef, private apiService: ApiService,
    private commonDataService: CommonDataService, public modalService: BsModalService,
    private toastr: ToastrService, private communicationDataService: CommunicationDataService) {
    this.communication = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};
    console.log(this.communication);
    this.isEditMode = this.modalService.config['data'] != undefined ? true : false;

    if (this.isEditMode) {

      console.log( "-----------------------------------" );
      console.log(  this.communication.personid );
      

        this.user = this.communication.personid
    
   
      if (this.communication.inquiryid != undefined) {
        this.enquiry = this.communicationDataService.getEnquiryByEnquiryId(this.communication.inquiryid);
        if (this.enquiry != undefined) {
          this.isEnquiry = true;
        }
      }
    } else {
      this.user = {};
      this.enquiry = {};
    }
    
  }

  ngOnInit() {
    this.enquiries = this.communicationDataService.getEnquiries();
    this.communicationFors = this.communicationDataService.getCommunicationFors();
    console.log(this.communicationFors);
    this.communicationMediums = this.communicationDataService.getCommunicationMediums();
    this.contacts = this.communicationDataService.getContacts();
    this.processStatuses = this.communicationDataService.getProcessStatuses();


    this.commonDataService.getCommunicationMediums().then(result => {
      this.communicationDataService.setCommunicationMediums(result);
      this.communicationMediums = this.communicationDataService.getCommunicationMediums();
    });
    
    this.commonDataService.getCommunicationFors().then(result => {
      this.communicationDataService.setCommunicationFors(result);
      this.communicationFors = this.communicationDataService.getCommunicationFors();
    });

  }

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  searchContact(searchQuery) {
    var contact = this.communicationDataService.getContactByMobileNumber(searchQuery);
    if (contact == undefined) {
      this.contactFound = false;
    } else {
      this.user = contact;
     
      this.contactFound = true;
    }
  }

  createUser(user) {
    user.entity_name = 'person';
    this.apiService.addEntity(user).then(result => {
      if (result != undefined) {
        this.toastr.success('Contact Added Successfully');
        this.contactFound = true;
      } else {
        this.toastr.error('Failed to add Contact');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Contact');
    });
  }

  submit(communication) {
    if (communication.entity_key_id != undefined) {
      this.update(communication);
    } else {
      this.create(communication);
    }
  }

  update(communication) {
    communication.entity_name = 'communication';
    this.apiService.updateEntity(communication).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Communication Updated Successfully');
      } else {
        this.toastr.error('Failed to Update Communication');
      }
    }).catch(error => {
      this.toastr.error('Failed to update Communication');
    });
  }

  create(communication) {
    communication.entity_name = 'communication';
    communication.personid = this.user.entity_key_id;
    if (this.enquiry.entity_key_id != undefined) {
      communication.inquiryid = this.enquiry.entity_key_id;
    }
    this.apiService.addEntity(communication).then(result => {
      if (result === true) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Communication Added Successfully');
      } else {
        this.toastr.error('Failed to add Communication');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Communication');
    });
  }

  toggleEnquiry(event) {
    if (event.target.checked) {
      this.isEnquiry = true;
    } else {
      this.isEnquiry = false;
    }
  }

  getProcessStatusById(processStatusId) {
    var processStatus = this.communicationDataService.getProcessStatusById(processStatusId);
    return processStatus.mprocessstatus;
  }

  onSelect(value) {
    var val = value.split(':');
    var communicationFor = this.communicationDataService.getCommunicationForByCommunicationForId(val[1].trim());
    if (communicationFor.commfor.toUpperCase() == "ENQUIRY") {
      this.isEnquiry = true;
    } else {
      this.isEnquiry = false;
    }
    console.log(communicationFor)
  }

}
