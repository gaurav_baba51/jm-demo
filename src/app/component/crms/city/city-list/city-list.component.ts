import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { CityDataService } from '../city-data.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { CityNewComponent } from '../city-new/city-new.component';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.scss']
})
export class CityListComponent implements OnInit {
  entityTitle = 'City';
  entities: any;
  entityName: any;
  bsModalRef: BsModalRef;
  paginatedEntity;


  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);

  }

  doPagination(start, end){
    this.paginatedEntity = this.entities.slice(start, end);
  }

  getAllEntity(start, end){
    this.apiService.getAllEntities("city").then(result => {
      this.entities = result;
      this.cityDataService.setAllEntity(result);
      this.totalItems = this.entities.length;
      this.doPagination(start, end); 
    });
  }

  navigateToRoute(entity) {
    var id = entity != undefined ? entity.id : '1';
    this.router.navigate(['dashboard/cities/new-city', id]);
  }

  navigateToEntitySummary(entity) {
    var id = entity != undefined ? entity.entity_key_id : '1';
    this.router.navigate(['dashboard/cities/city-summary', id]);
  }


  constructor(private router: Router,
    private apiService: ApiService,
    private commonDataService : CommonDataService,
    private cityDataService: CityDataService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.paginatedEntity = [];
    this.getAllEntity(this.currentStartIndex, this.currentEndIndex);
  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(CityNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      console.log("modal close: " + reason);
      this.getAllEntity(this.currentStartIndex, this.currentEndIndex);

    })
  }

}
