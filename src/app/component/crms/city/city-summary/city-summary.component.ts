import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { CityDataService } from '../city-data.service';
import { CityNewComponent } from '../city-new/city-new.component';

@Component({
  selector: 'app-city-summary',
  templateUrl: './city-summary.component.html',
  styleUrls: ['./city-summary.component.scss']
})
export class CitySummaryComponent implements OnInit {
  entityTitle = "City";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private cityDataService: CityDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.cityDataService.getEntityById(this.id);
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(CityNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

}
