import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { CitySummaryComponent } from '../city/city-summary/city-summary.component';
import { CityComponent } from '../city/city.component';
import { CityNewComponent } from '../city/city-new/city-new.component';


const routes: Routes = [
  {
    path: '',
    component: CityComponent,
    data: {
      title: 'Cities'
    }
  },
  {
    path: 'new-city/:id',
    component: CityNewComponent ,
    data: {
      title: 'Create New City'
    }
  },
  {
    path: 'city-summary/:id',
    component: CitySummaryComponent ,
    data: {
      title: 'City Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CityRoutingModule { }
