import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CityRoutingModule } from './city-routing.module';
import { CityComponent } from './city.component';
import { CityListComponent } from './city-list/city-list.component';
import { CityNewComponent } from './city-new/city-new.component';
import { CitySummaryComponent } from './city-summary/city-summary.component';
import { CityDataService } from './city-data.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CityRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [
    CityComponent,
    CityListComponent,
    CityNewComponent,
    CitySummaryComponent],
  providers: [CityDataService]
})
export class CityModule { }
