import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CollapseModule, CarouselModule } from 'ngx-bootstrap';
import { RatingModule } from 'ngx-bootstrap';
import { Ng2TableModule } from 'ngx-datatable/ng2-table';
import { ProgressbarModule } from 'ngx-bootstrap';


import { Ng5SliderModule } from 'ng5-slider';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapSliderModule } from 'ngx-bootstrap-slider';
import { VehicleCategoryRoutingModule } from './vehicle-category-routing.module';
import { VehicleCategoryDataService } from './vehicle-category-data.service';
import { VehicleCategorySummaryComponent } from './vehicle-category-summary/vehicle-category-summary.component';
import { VehicleCategoryListComponent } from './vehicle-category-list/vehicle-category-list.component';
import { NewVehicleCategoryComponent } from './new-vehicle-category/new-vehicle-category.component';
import { VehicleCategoryComponent } from './vehicle-category.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    VehicleCategoryRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    Ng2TableModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    RatingModule.forRoot(),
    CarouselModule.forRoot(),
    ProgressbarModule.forRoot(),
    BsDatepickerModule.forRoot(),
    Ng5SliderModule,
    NgxBootstrapSliderModule
  ],
  declarations: [
    VehicleCategoryComponent,
    NewVehicleCategoryComponent,
    VehicleCategoryListComponent,
    VehicleCategorySummaryComponent],
  providers: [VehicleCategoryDataService]
})
export class VehicleCategoryModule { }
