import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVehicleCategoryComponent } from './new-vehicle-category.component';

describe('NewVehicleCategoryComponent', () => {
  let component: NewVehicleCategoryComponent;
  let fixture: ComponentFixture<NewVehicleCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVehicleCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVehicleCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
