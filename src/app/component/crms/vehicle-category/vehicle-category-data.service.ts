import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VehicleCategoryDataService {

 
  entities;

  constructor() { }


  getAllEntity() {
    return this.entities;
  }

  setAllEntity(entities) {
    this.entities = entities;
  }

  getEntityById(id) {
    for (var i in this.entities) {
      if (this.entities[i].entity_key_id == id) {
        return this.entities[i];
      }
   }
  }
}
