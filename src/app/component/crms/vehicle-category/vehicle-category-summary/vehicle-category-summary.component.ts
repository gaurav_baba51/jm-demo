import { Component, OnInit } from '@angular/core'; 
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { VehicleCategoryDataService } from '../vehicle-category-data.service';
import { NewVehicleCategoryComponent } from '../new-vehicle-category/new-vehicle-category.component';

@Component({
  selector: 'app-vehicle-category-summary',
  templateUrl: './vehicle-category-summary.component.html',
  styleUrls: ['./vehicle-category-summary.component.scss']
})
export class VehicleCategorySummaryComponent implements OnInit {

  entityTitle = "Vehicle Category";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private dataService: VehicleCategoryDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.dataService.getEntityById(this.id);
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewVehicleCategoryComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

}
