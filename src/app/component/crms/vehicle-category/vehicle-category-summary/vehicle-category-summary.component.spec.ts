import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleCategorySummaryComponent } from './vehicle-category-summary.component';

describe('VehicleCategorySummaryComponent', () => {
  let component: VehicleCategorySummaryComponent;
  let fixture: ComponentFixture<VehicleCategorySummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleCategorySummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleCategorySummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
