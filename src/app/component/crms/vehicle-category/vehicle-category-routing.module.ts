import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { VehicleCategoryComponent } from './vehicle-category.component';
import { NewVehicleCategoryComponent } from './new-vehicle-category/new-vehicle-category.component';
import { VehicleCategorySummaryComponent } from './vehicle-category-summary/vehicle-category-summary.component';



const routes: Routes = [
  {
    path: '',
    component: VehicleCategoryComponent,
    data: {
      title: 'Vehicles Category'
    }
  },
  {
    path: 'new-vehicle-category/:id',
    component: NewVehicleCategoryComponent ,
    data: {
      title: 'Create New Vehicle Category'
    }
  },
  {
    path: 'vehicle-category-summary/:id',
    component: VehicleCategorySummaryComponent,
    data: {
      title: 'Vehicle Category Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleCategoryRoutingModule { }
