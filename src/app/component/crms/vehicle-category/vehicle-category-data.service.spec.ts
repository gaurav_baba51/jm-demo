import { TestBed, inject } from '@angular/core/testing';

import { VehicleCategoryDataService } from './vehicle-category-data.service';

describe('VehicleCategoryDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VehicleCategoryDataService]
    });
  });

  it('should be created', inject([VehicleCategoryDataService], (service: VehicleCategoryDataService) => {
    expect(service).toBeTruthy();
  }));
});
