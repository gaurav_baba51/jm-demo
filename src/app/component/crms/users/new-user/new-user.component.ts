import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDataService } from '../user-data.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {
  user: any;
  modalRef: BsModalRef;
  message: string;
  cities: any;
  branches: any;
  contacts: any;
  occupations: any;
  isEdit: Boolean = false;
  personImage: any;
  createImage: Boolean = false;
  dt:string;
  
  phoneNumbr = /^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/;

  constructor(private route: ActivatedRoute,
    private userDataService: UserDataService,
    private router: Router,
    public bsModalRef: BsModalRef,
    private apiService: ApiService,
    private commonDataService: CommonDataService,
    public modalService: BsModalService,
    private toastr: ToastrService) {


    this.user = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};

    if (this.user.dob != undefined) {
      var temp = new Date(this.user.dob);
      this.user.dob = temp;
    }

    if (this.user.entity_key_id != undefined) {
      this.isEdit = true;
      this.apiService.getImageEntityById(1,this.user.entity_key_id ).then(result=>{
        this.personImage = result['blobValue'];
        if (this.personImage == undefined) {
          this.createImage = true;
        }
      })
    }

    this.cities = this.userDataService.getCities();
    this.branches = this.userDataService.getBranches();
    this.contacts = this.userDataService.getAllUsers();
    this.occupations = this.userDataService.getOccupation();
    this.dt = new Date().toISOString();
   
  }

  ngOnInit() {
  }
  
  setAge() {
    var diff =(new Date().getTime() - new Date(this.user.dob).getTime()) / 1000;
    diff /= (60 * 60 * 24);
    var exact =  Math.abs(Math.round(diff/365.25));
    this.user.age = exact;
  }

  submit(user) {
    if (user.entity_key_id != undefined) {
      this.update(user);
    } else {
      this.create(user);
    }
  }

  update(user) {
    user.entity_name = 'person';
    this.apiService.updateEntity(user).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        if (this.createImage) {
          this.addBlobImage(result);
        } else {
          this.updateBlobImage(result);
        }
        this.toastr.success('Contact Updated Successfully');
      } else {
        this.toastr.error('Failed to Update Contact');
      }
    }).catch(error => {
      this.toastr.error('Failed to update Contact');
    });
  }

  create(user) {
    user.entity_name = 'person';
    this.apiService.addEntity(user).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.addBlobImage(result.entityKey);
        this.toastr.success('Contact Added Successfully');
      } else {
        this.toastr.error('Failed to add Contact');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Contact');
    });
  }

  getCity(id) {
    return this.userDataService.getCityNameByCityId(id);
  }

  onFileChange(event){
    console.log(event);
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = (e)=> {
        var rawData = reader.result;
        this.personImage=rawData;
      }
    }
  }

  addBlobImage(id){
    var image = {};
    image['entity_name'] = "person";
    image['entity_key_id'] = id;
    image['imagestring'] = this.personImage;
    if (this.personImage != undefined) {
      this.apiService.addBlobImage(image).then(result=>{
        console.log("result:" + result);
      });
    }
  }

  updateBlobImage(id){
    var image = {};
    image['entity_name'] = "person";
    image['entity_key_id'] = id;
    image['imagestring'] = this.personImage;
    if (this.personImage != undefined) {
      this.apiService.updateBlobImage(image).then(result=>{
        console.log("result:" + result);
      });
    }
  }
}
