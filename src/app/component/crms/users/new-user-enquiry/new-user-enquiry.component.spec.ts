import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewUserEnquiryComponent } from './new-user-enquiry.component';

describe('NewUserEnquiryComponent', () => {
  let component: NewUserEnquiryComponent;
  let fixture: ComponentFixture<NewUserEnquiryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewUserEnquiryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewUserEnquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
