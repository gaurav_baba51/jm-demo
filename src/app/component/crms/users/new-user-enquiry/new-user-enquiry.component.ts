import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { UserDataService } from '../user-data.service';
import { TreeModel } from 'ng2-tree';

@Component({
  selector: 'app-new-user-enquiry',
  templateUrl: './new-user-enquiry.component.html',
  styleUrls: ['./new-user-enquiry.component.scss'],

})
export class NewUserEnquiryComponent implements OnInit {
  modalRef: BsModalRef;
  enquiry: any;
  contactFound: boolean;
  user: any;
  enquiryFors: any;
  enquiryStatuses: any;
  searchQuery: any;
  enquirySources: any;
  targetProducts: any;
  processStatuses: any;
  employees: any;
  contacts: any;
  branches: any;

  constructor(public bsModalRef: BsModalRef,
    private apiService: ApiService,
    private commonDataService: CommonDataService,
    public modalService: BsModalService,
    private toastr: ToastrService,
    private userDataService: UserDataService) {

    this.user = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};
    this.enquiry = {};

    this.enquiryFors = this.userDataService.getAllEnquiryFor();
    this.enquirySources = this.userDataService.getAllSource();
    this.targetProducts = this.userDataService.getAllTargetProducts();
    this.branches = this.userDataService.getBranches();
    this.enquiryStatuses = this.userDataService.getAllEnquiryStatus();
    this.processStatuses = this.userDataService.getProcessStatuses();
    this.employees = this.userDataService.getEmployees();

  }

  ngOnInit() {
  }

  getEmployeesNameByEmployeeId(employeeId) {
    var employee = this.userDataService.getEmployeeByEmployeeId(employeeId);
    var contact = this.userDataService.getUserByUserId(employee.personid);
    return contact.fname + ' ' + contact.lname;
  }

  getBranchName(branchId) {
    var branch = this.userDataService.getBranchNameAndLocationByBranchId(branchId);
    if (branch == undefined) {
      return 'NA';
    } else {
      return branch.title + ', ' + branch.location;
    }
  }

  submit(enquiry) {
    if (enquiry.entity_key_id != undefined) {
      this.update(enquiry);
    } else {
      this.create(enquiry);
    }
  }

  update(enquiry) {
    enquiry.entity_name = 'enquiry';
    this.apiService.updateEntity(enquiry).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Enquiry Updated Successfully');
      } else {
        this.toastr.error('Failed to Update Enquiry');
      }
    }).catch(error => {
      this.toastr.error('Failed to update Enquiry');
    });
  }

  create(enquiry) {
    enquiry.entity_name = 'enquiry';
    enquiry.personid = this.user.entity_key_id;
    var task = {
      entity_name: 'task',
      task_title: 'New Enquiry',
      assignto: enquiry.assignto,
      description: enquiry.description,
      expdate: enquiry.followupdate,
      datevalue: enquiry.datevalue,
      taskstatus: enquiry.statusvalue,
      remarkvalue: enquiry.remarkvalue,
      inquiryid: ''
    };
    this.apiService.addEntity(enquiry).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Enquiry Added Successfully');
        task.inquiryid = result.entityKey;
        this.apiService.addEntity(task).then(result1 => {
          if (result1 != undefined) {
            this.modalService.setDismissReason(result);
            this.bsModalRef.hide();
            this.toastr.success('Task Added Successfully');
          } else {
            this.toastr.error('Failed to add Task');
          }
        });
      } else {
        this.toastr.error('Failed to add Enquiry');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Enquiry');
    });
  } 

}
