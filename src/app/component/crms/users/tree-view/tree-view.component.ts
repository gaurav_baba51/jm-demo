import { Component, OnInit } from '@angular/core';
import { TreeModel, Ng2TreeSettings } from 'ng2-tree';

import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { UserDataService } from '../user-data.service';



@Component({
  selector: 'app-tree-view',
  template: '<tree [tree]="tree" [settings]="settings"></tree>',
 
})
export class TreeViewComponent implements OnInit {

  makes:any;
  models:any;
  variants: any;
  Variant_array:any;
  tree: TreeModel;
  settings:Ng2TreeSettings = {
    rootIsVisible: false
  }; 
  constructor(
    private apiService: ApiService,
    private commonDataService: CommonDataService,
    private userDataService: UserDataService) { 

      this.apiService.getEntity('variant')
      .then(result=>{
        this.variants = result;
        console.log(this.variants);
        
        this.variants.sort(function(a, b){return a.model.make-b.model.make});

        let prevMake=[]; 
        this.Variant_array =  this.variants.map(function(variant, index, arr){
          let make = variant.model.make.name;
          if(make!= undefined)
            {
              if(undefined == prevMake.find( currentmake => currentmake == make ) ) 
              {
                prevMake.push(make);     
                return { 
                  value: variant.model.make.name,
                  children:arr.filter(function(variant, index, arr){ 
                      let childmake = variant.model.make.name;

                      if(childmake==undefined)
                         return false;
                      else   
                         return childmake==make;            
                  })
                }
              } else return null;  
            }
        }).filter(function(value){
          return value!=undefined;
        })
       
      console.log(this.Variant_array);



      this.Variant_array =  this.Variant_array.map(function(variant, index, arr){
       
       if(variant.children!=null)
         {     
          let prevModelid=[];
          return {
             value:variant.value,
             settings:{
               isCollapsedOnInit:true
             }, 
             children:variant.children.map(function(variant, index, arr){ 

              let modelid = variant.modelid;
              if( undefined == prevModelid.find( currentmodelid => currentmodelid == modelid )  ) 
                {
                  prevModelid.push(modelid);
                  return { 
                    value: variant.modelid,
                    children:arr.filter(function(currentvariant){
                      return variant.modelid==currentvariant.modelid;
                    }).map(function(variant, index, arr){ 
                           return {value:variant.variant };            
                       }) 
                  }
                } else {
                  return null;
                } 
  
              }).filter(function(value){
                return value!=null;
              }) 
          }
         }  
      })
     
      this.apiService.getAllEntities('make')
      .then(result=>{
        this.makes = result;
        this.apiService.getAllEntities('model')
        .then(result=>{
          this.models = result;
            for(var i=0; i<this.Variant_array.length; i++)
            {
             
              this.Variant_array[i].value = this.Variant_array[i].value;
            
              for(var j=0; j<this.Variant_array[i].children.length; j++)
                {
                  this.Variant_array[i].children[j].value = this.Variant_array[i].children[j].value;
                }
            } 
            
          

            this.tree = {
              value: 'Select Vehicles from below List',
              children:  this.Variant_array
            };

        })
      })
      
      })
    }

  ngOnInit() {
  }




/* 
  
  public tree: TreeModel = {
    value: 'Programming languages by programming paradigm',
    children: [
      {
        value: 'Object-oriented programming',
        children: [{ value: 'Java' }, { value: 'C++' }, { value: 'C#' }]
      },
      {
        value: 'Prototype-based programming',
        children: [{ value: 'JavaScript' }, { value: 'CoffeeScript' }, { value: 'Lua' }]
      }
    ]
  };
 */

    


}
