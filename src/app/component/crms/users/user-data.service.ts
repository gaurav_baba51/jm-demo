import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  cities: any = [];
  users: any = [];
  branches: any = [];
  varients: any = [];
  source: any = [];
  enquiryFor: any = [];
  targetProduct: any = [];
  enquiryStatus: any = [];
  enquiries: any = [];
  communications: any = [];
  cummunicationFors: any = [];
  communicationMediums: any = [];
  processStatuses: any = [];
  employees: any = [];
  occupation: any = [];
  

  constructor() {
  }

  getOccupation() {
    return this.occupation;
  }

  setOccupation(occupation) {
    this.occupation = occupation;
  }

  getOccupationById(id) {
    for (var occupation of this.occupation) {
      if (occupation.entity_key_id == id) {
        return occupation;
      }
    }
  }

  getVarients() {
    return this.varients;
  }

  setVarients(varients) {
    this.varients = varients;
  }


  getEmployees() {
    return this.employees;
  }

  setEmployees(employees) {
    this.employees = employees;
  }

  getEmployeeByEmployeeId(employeeId) {
    for (var employee of this.employees) {
      if (employee.entity_key_id == employeeId) {
        return employee;
      }
    }
  }

  getProcessStatuses() {
    return this.processStatuses;
  }

  setProcessStatuses(processStatuses) {
    this.processStatuses = processStatuses;
  }

  getProcessStatusById(processStatusId) {
    for (var processStatus of this.processStatuses) {
      if (processStatus.entity_key_id == processStatusId) {
        return processStatus;
      }
    }
  }

  getCommunicationByCommunicationById(communicationId) {
    for (var communication of this.communications) {
      if (communication.entity_key_id == communicationId) {
        return communication;
      }
    }
  }

  getEnquiryByEnquiryId(enquiryId) {
    for (var enquiry of this.enquiries) {
      if (enquiry.entity_key_id == enquiryId) {
        return enquiry;
      }
    }
  }

  getCommunicationMediums(communicationMediums) {
    return this.communicationMediums;
  }

  setCommunicationMediums(communicationMediums) {
    this.communicationMediums = communicationMediums;
  }

  getCommunicationMediumByCommunicationMediumId(communicationMediumId) {
    for (var commuicationMedium of this.communicationMediums) {
      if (commuicationMedium.entity_key_id == communicationMediumId) {
        return commuicationMedium;
      }
    }
  }

  getCommunicationFors() {
    return this.cummunicationFors;
  }

  setCommunicationFors(cummunicationFors) {
    this.cummunicationFors = cummunicationFors;
  }

  getCommunicationForByCommunicationById(communicationForId) {
    for (var communicationFor of this.cummunicationFors) {
      if (communicationFor.entity_key_id == communicationForId) {
        return communicationFor;
      }
    }
  }

  getEnquiries() {
    return this.enquiries;
  }

  setEnquiries(enquiries) {
    this.enquiries = enquiries;
  }

  getLatestEnquiryForContact() {
    return this.enquiries[this.enquiries.length -1];
  }

  getCommunications() {
    return this.communications;
  }

  setCommunications(communications) {
    this.communications = communications;
  }

  getLatestCommunicationForContact() {
    return this.communications[this.communications.length -1];
  }

  getAllSource() {
    return this.source;
  }

  setAllSource(sources) {
    this.source = sources;
  }

  getSourceBySourceId(sourceId) {
    for (var source of this.source) {
      if (source.entity_key_id == sourceId) {
        return source;
      }
    }
  }

  getAllEnquiryFor() {
    return this.enquiryFor;
  }

  setAllEnquiryFor(enquiryFors) {
    this.enquiryFor = enquiryFors;
  }

  getEnquiryForByEnquiryForId(enquiryForId) {
    for (var enquiryFor of this.enquiryFor) {
      if (enquiryFor.entity_key_id == enquiryForId) {
        return enquiryFor;
      }
    }
  }

  getAllTargetProducts() {
    return this.targetProduct;
  }

  setAllTargetProducts(targetProducts) {
    this.targetProduct = targetProducts;
  }

  getTargetProductByTargetProductId(targetProductId) {
    for (var targetProduct of this.targetProduct) {
      if (targetProduct.entity_key_id == targetProductId) {
        return targetProduct;
      }
    }
  }

  getAllEnquiryStatus() {
    return this.enquiryStatus;
  }

  setAllEnquiryStatuses(enquiryStatuses) {
    this.enquiryStatus = enquiryStatuses;
  }

  getEnquiryStatusByEnquiryStatusId(enquiryStatusId) {
    for (var enquiryStatus of this.enquiryStatus) {
      if (enquiryStatus.entity_key_id == enquiryStatusId) {
        return enquiryStatus;
      }
    }
  }

  getPersonByPersonId(personId) {
    for (var i in this.users) {
      if (this.users[i].entity_key_id == personId) {
        return this.users[i];
      }
    }
  }

  getAllUsers() {
    return this.users;
  }

  setAllUsers(users) {
    this.users = users;
  }

  getUserByUserId(userId) {
    for (var user of this.users) {
      if (user.entity_key_id == userId) {
        return user;
      }
    }
  }

  setCities(cities) {
    this.cities = cities;
  }

  getCities() {
    return this.cities;
  }

  getCityNameByCityId(cityId) {
    for (var city in this.cities) {
      if (this.cities[city].entity_key_id == cityId) {
        return this.cities[city];
      }
    }
  }

  getBranches() {
    return this.branches;
  }

  setBranches(branches) {
    this.branches = branches;
  }

  getBranchNameAndLocationByBranchId(branchId) {
    for (var i in this.branches) {
      if (this.branches[i].entity_key_id == branchId) {
        return this.branches[i];

      }
    }
  }

}
