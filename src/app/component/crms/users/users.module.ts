import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UserListComponent } from './user-list/user-list.component';
import { NewUserComponent } from './new-user/new-user.component';
import { NgPipesModule } from 'ng-pipes';
import { UserSummaryComponent } from './user-summary/user-summary.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { UserDataService } from './user-data.service';
import { Ng2TableModule } from 'ngx-datatable/ng2-table';
import { NewUserEnquiryComponent } from './new-user-enquiry/new-user-enquiry.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TreeViewComponent } from './tree-view/tree-view.component';
import { TreeModule } from 'ng2-tree';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UsersRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    Ng2TableModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TreeModule
  ],
  entryComponents: [
    NewUserComponent,
    NewUserEnquiryComponent
  ],
  declarations: [
    UsersComponent,
    UserListComponent,
    NewUserComponent,
    UserSummaryComponent,
    NewUserEnquiryComponent,
    TreeViewComponent],
  providers: [UserDataService]
})
export class UsersModule { }
