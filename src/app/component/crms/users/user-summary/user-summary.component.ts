import { Component, OnInit } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NewUserComponent } from '../new-user/new-user.component';
import { UserDataService } from '../user-data.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { NewUserEnquiryComponent } from '../new-user-enquiry/new-user-enquiry.component';
import { EnquiryDataService } from '../../enquiry/enquiry-data.service';

@Component({
  selector: 'app-user-summary',
  templateUrl: './user-summary.component.html',
  styleUrls: ['./user-summary.component.scss']
})
export class UserSummaryComponent implements OnInit {
  user: any;
  bsModalRef: BsModalRef;
  city: any;
  branch: any;
  referalPersonName: any;
  branchNameAndLocation: any;
  enquiries: any;
  occupation;
  communications: any;
  id: any;
  latestEnquiry: any;
  latestCommunication: any;
  personImage:any;

  targetProduct: any;
  status: any;
  person: any;

  latestTargetProduct: any;
  latestEnquiryFor: any;
  latestEnquiryStatus: any;
  latestProcessStatus: any;

  latestCommunicationFor: any;
  latestCommunicationMedium: any;

  constructor(private route: ActivatedRoute,
    private apiService: ApiService,
    private modalService: BsModalService,
    private userDataService: UserDataService,
    private commonDataService: CommonDataService, private router: Router,
    private enquiryDataService: EnquiryDataService) {
    this.id = this.route.snapshot.params['id'];
    

    this.commonDataService.getTargetProducts().then(result => {
      this.userDataService.setAllTargetProducts(result);
      this.commonDataService.getEnquiryStatuses().then(result1 => {
        this.userDataService.setAllEnquiryStatuses(result1);
        this.getUserByUserId(this.id);
      });
    });
  }

  getUserByUserId(userId) {
    this.user = this.userDataService.getPersonByPersonId(userId);
    if (this.user == undefined) {
      this.commonDataService.getContacts().then(result => {
        this.userDataService.setAllUsers(result);
        this.user = this.userDataService.getPersonByPersonId(this.id);
        this.getAllEnquiries();
      });
    } else {
      this.getAllEnquiries();
    }
  }

  getAllEnquiries() {
    this.commonDataService.getEnquiriesByPersonId(this.id).then(result => {
      this.enquiries = result;
      this.loadData();
      this.length = this.data.length;
      this.onChangeTable(this.config);
      this.userDataService.setEnquiries(this.enquiries);
      if (this.userDataService.getLatestEnquiryForContact() == undefined) {
        this.latestEnquiry = {};
      } else {
        this.latestEnquiry = this.userDataService.getLatestEnquiryForContact();
        this.latestTargetProduct = this.getTargetProductNameByTargetProductId(this.latestEnquiry.enquiryfor);
        this.latestEnquiryStatus = this.getEnquiryStatusByEnquiryStatusId(this.latestEnquiry.statusvalue);

      }
      this.apiService.getImageEntityById(1,this.id).then(result=>{
        this.personImage = result['blobValue'];
      })
    });
    this.getLatestCommunication();
  }

  getLatestCommunication() {
    this.commonDataService.getCommunicationsByPersonId(this.id).then(result => {
      this.communications = result;
      this.userDataService.setCommunications(this.communications);
      if (this.userDataService.getLatestCommunicationForContact() == undefined) {
        this.latestCommunication = {};
      } else {
        this.latestCommunication = this.userDataService.getLatestCommunicationForContact();
        this.latestCommunicationFor = this.getCommunicationForNameById(this.latestCommunication.communicationfor);
        this.latestCommunicationMedium = this.getCommunicationMediumNameById(this.latestCommunication.medium);
      
        //this.latestProcessStatus = this.getProcessStatusById(this.latestCommunication.processstatus);
      }
    });

    this.getReferalPerson();
  }

  getProcessStatusById(processStatus) {
    var processStatus = this.userDataService.getProcessStatusById(processStatus);
    if (processStatus == undefined) {
      this.commonDataService.getProcessStatus().then(result => {
        this.userDataService.setProcessStatuses(result);
        processStatus = this.userDataService.getProcessStatusById(processStatus);
        this.latestProcessStatus = processStatus.mprocessstatus;
      });
    } else {
      this.latestProcessStatus = processStatus.mprocessstatus;
    }
  }

  getReferalPerson() {
    var referalPerson = this.userDataService.getPersonByPersonId(this.user.reffid);
    if (referalPerson == undefined) {
      this.commonDataService.getContacts().then(result => {
        this.userDataService.setAllUsers(result);
        referalPerson = this.userDataService.getPersonByPersonId(this.user.reffid);
        if (referalPerson != undefined) {
          this.referalPersonName = referalPerson.fname + ' ' + referalPerson.lname;
        } else {
          this.referalPersonName = '';
        }
        this.getBranch();
      });
    } else {
      if (referalPerson != undefined) {
        this.referalPersonName = referalPerson.fname + ' ' + referalPerson.lname;
      } else {
        this.referalPersonName = {};
      }
      this.getBranch();
    }
  }

  getBranch() {
    var branch = this.userDataService.getBranchNameAndLocationByBranchId(this.user.branch);
    if (branch == undefined) {
      this.commonDataService.getBranches().then(result => {
        this.userDataService.setBranches(result);
        branch = this.userDataService.getBranchNameAndLocationByBranchId(this.user.branch);
        this.getCityNameByCityId(branch);
      });
    } else {
      this.getCityNameByCityId(branch);
    }
  }

  loadData() {
    var temp = [];
    this.enquiries.forEach((enq, index) => {
      var person = this.userDataService.getUserByUserId(enq.personid);
      var targetProduct = this.getTargetProductNameByTargetProductId(enq.targetproduct);
      var status = this.getEnquiryStatusByEnquiryStatusId(enq.statusvalue);
      var enquiry = {
        'id': enq.entity_key_id,
        'datevalue': enq.datevalue,
        'contactname': person.fname + ' ' + person.lname,
        'contactno': person.mobno,
        'targetproduct': targetProduct,
        'status': status
        /* 'vehiclename': this.getBranchNameByBranchId(user.branch),
        'vehicleno': this.getBranchNameByBranchId(user.branch), */
      };
      temp.push(enquiry);
      if (index == this.enquiries.length - 1) {
        this.data = temp;
        this.data = [...this.data];
      }
    });
  }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.user,
      title: 'Create New Contact',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewUserComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      this.commonDataService.getContacts().then(result => {
        this.userDataService.setAllUsers(result);
        this.user = this.userDataService.getPersonByPersonId(this.id);
        this.getAllEnquiries();
        this.apiService.getImageEntityById(1,this.id).then(result=>{
          this.personImage = result['blobValue'];
        })
      });
    })
  }

  openEnquiryModelWithComponent() {
    const initialState = {
      data: this.user,
      title: 'Create New Contact',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewUserEnquiryComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      console.log("modal close: " + reason);
      this.getAllEnquiries();
      this.loadData();
    })
  }

  openEnquiryModelWithComponentEdit() {
    const initialState = {
      data: this.user,
      title: 'Create New Contact',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewUserEnquiryComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  getSourceNameBySourceId(sourceId) {
    var source = this.userDataService.getSourceBySourceId(sourceId);
    return source.enqsource;
  }

  getEnquiryForNnameByEnquiryForId(enquiryForId) {
    var enquiryFor = this.userDataService.getEnquiryForByEnquiryForId(enquiryForId);
    if (enquiryFor == undefined) {
      this.commonDataService.getEnquiryFors().then(result => {
        this.userDataService.setAllEnquiryFor(result);
        enquiryFor = this.userDataService.getEnquiryForByEnquiryForId(enquiryForId);
        return enquiryFor.enqfor;
      });
    } else {
      return enquiryFor.enqfor;
    }
  }

  getTargetProductNameByTargetProductId(targetProductId) {
    var targetProduct = this.userDataService.getTargetProductByTargetProductId(targetProductId);
    if (targetProduct == undefined) {
      this.commonDataService.getTargetProducts().then(result => {
        this.userDataService.setAllTargetProducts(result);
        targetProduct = this.userDataService.getTargetProductByTargetProductId(targetProductId);
        if (targetProduct == undefined) {
          return "";
        }
        else  
        return targetProduct.tmake + ' ' + targetProduct.tmodel;
      });
    } else {
      return targetProduct.tmake + ' ' + targetProduct.tmodel;
    }
  }

  getBranchNameByBranchId(branchId) {
    var branch = this.userDataService.getBranchNameAndLocationByBranchId(branchId);
    return branch.title + ', ' + branch.location;
  }

  getEnquiryStatusByEnquiryStatusId(enquiryStatusId) {
    var enquiryStatus = this.userDataService.getEnquiryStatusByEnquiryStatusId(enquiryStatusId);
    if (enquiryStatus == undefined) {
      this.commonDataService.getEnquiryStatuses().then(result => {
        this.userDataService.setAllEnquiryStatuses(result);
        enquiryStatus = this.userDataService.getEnquiryStatusByEnquiryStatusId(enquiryStatusId);
        return enquiryStatus.statusval;
      });
    } else {
      return enquiryStatus.statusval;
    }
  }

  getCityNameByCityId(citye) {
    var cityId = citye.location;
    var city = this.userDataService.getCityNameByCityId(cityId);
    if (city == undefined) {
      this.commonDataService.getCities().then(result => {
        this.userDataService.setCities(result);
        city = this.userDataService.getCityNameByCityId(cityId);
        this.city = city.cityName;
        this.branchNameAndLocation = citye.title + ', ' + city.cityName;
        this.getOccupationById(this.user.occupation);
      });
    } else {
      this.city = city.cityName;
      this.branchNameAndLocation = citye.title + ', ' + city.cityName;
      this.getOccupationById(this.user.occupation);
    }
  }

  getCityName(cityId) {
    var city = this.userDataService.getCityNameByCityId(cityId);
    if (city == undefined) {
      this.commonDataService.getCities().then(result => {
        this.userDataService.setCities(result);
        city = this.userDataService.getCityNameByCityId(cityId);
        this.city = city.cityName;
      });
    } else {
      this.city = city.cityName;
    }
  }


  getOccupationById(id) {
    if (id != undefined) {
      var occupation = this.userDataService.getOccupationById(id);
      if (occupation == undefined) {
        this.commonDataService.getOccupation().then(result => {
          this.userDataService.setOccupation(result);
          occupation = this.userDataService.getOccupationById(id);
          this.occupation = occupation != undefined ? occupation.occupationm : '';
          this.getCityName(this.user.city);
        });
      } else {
        this.occupation = occupation != undefined ? occupation.occupationm : '';
        this.getCityName(this.user.city);
      }
    }
  }

  getCommunicationForNameById(communicationForId) {
    var communicationFor = this.userDataService.getCommunicationForByCommunicationById(communicationForId);
    if (communicationFor == undefined) {
      this.commonDataService.getCommunicationFors().then(result => {
        this.userDataService.setCommunicationFors(result);
        communicationFor = this.userDataService.getCommunicationForByCommunicationById(communicationForId);
        this.latestCommunicationFor =  communicationFor.commfor;
      });
    } else {
      return communicationFor.commfor;
    }
  }

  getPersonNameById(personId) {
    var person = this.userDataService.getPersonByPersonId(personId);
    return person.fname + ' ' + person.lname;
  }

  getCommunicationMediumNameById(communicationMediumId) {
    var communicationMedium = this.userDataService.getCommunicationMediumByCommunicationMediumId(communicationMediumId);
    if (communicationMedium == undefined) {
      this.commonDataService.getCommunicationMediums().then(result => {
        this.userDataService.setCommunicationMediums(result);
        communicationMedium = this.userDataService.getCommunicationMediumByCommunicationMediumId(communicationMediumId);
        this.latestCommunicationMedium = communicationMedium.commMedium;
      });
    } else {
      this.latestCommunicationMedium = communicationMedium.commMedium;
    }
    //return communicationMedium.commMedium;
  }

  public rows: Array<any> = [];
  public columns: Array<any> = [
    { title: 'Enquiry Id', name: 'id', sort: 'asc'/* , filtering: {filterString: '', placeholder: 'Filter by name'} */ },
    { title: 'Enquiry Date', name: 'datevalue' },
    { title: 'Contact Name', name: 'contactname' },
    { title: 'Contact Number', name: 'contactno' },
    { title: 'Target Product', name: 'targetproduct' },
    { title: 'Enquiry Status', name: 'status' }
  ];

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  data: Array<any> = [];

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if(item[column.name]!= undefined)
          {
            if (item[column.name].toString().match(this.config.filtering.filterString)) {
              flag = true;
            }
          }
        else{
          flag = true;
        }  

      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public navigateToEnquiry(data: any): any {
    console.log(data.row.id);
    var id = data.row.id != undefined ? data.row.id : '1';
    this.commonDataService.getEnquiries().then(result => {
      this.enquiryDataService.setEnquiries(result);
      this.router.navigate(['dashboard/enquiries/enquiry-summary/', id]);
    })
  }

  getDate(date: any) {
    var dateVar = new Date(date);
    return dateVar.getDate() + '/' + (dateVar.getMonth() + 1) + '/' + dateVar.getFullYear();
  }

}
