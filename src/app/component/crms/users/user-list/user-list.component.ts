import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { UserDataService } from '../user-data.service';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NewUserComponent } from '../new-user/new-user.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  users: any;
  userName: any;
  bsModalRef: BsModalRef;
  paginatedUsers;


  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;

  /*---------------------------------*/

  public rows: Array<any> = [];
  public columns: Array<any> = [
    { title: 'Id', name: 'id', sort: 'asc'},
    { title: 'Name', name: 'name', sort: false},
    { title: 'Mobile', name: 'mobile', sort: false },
    { title: 'Email Id', name: 'email', sort: false },
    { title: 'City', name: 'city', sort: false },
    { title: 'Branch', name: 'branch', sort: false }
  ];

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  data: Array<any> = [];

  constructor(private router: Router,
    private apiService: ApiService,
    private commonDataService: CommonDataService,
    private userDataService: UserDataService,
    private modalService: BsModalService) {
    //this.length = this.data.length;
    this.getAllContacts(this.currentStartIndex, this.currentEndIndex);
  }

  ngOnInit() {
    this.currentEndIndex = this.pageSize;
    this.currentStartIndex = 0;
  }

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
       console.log(item[column.name]);
       if(item[column.name]!=undefined)
         {
          if (item[column.name].toString().match(this.config.filtering.filterString)) {
            flag = true;
          }
         }
       else{
        flag = true;
       }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    console.log(data.row.id);
    var id = data.row.id != undefined ? data.row.id : '1';
    this.router.navigate(['dashboard/users/person-summary', id]);
  }

  getAllContacts(start, end) {
    this.commonDataService.getContacts().then(result => {
      this.users = result;

      console.log(this.users);
      this.userDataService.setAllUsers(result);
      this.totalItems = this.users.length;

      this.commonDataService.getEnquirySources().then(result => {
        this.userDataService.setAllSource(result);
      });
      this.commonDataService.getEnquiryFors().then(result => {
        this.userDataService.setAllEnquiryFor(result);
      });
      this.commonDataService.getTargetProducts().then(result => {
        this.userDataService.setAllTargetProducts(result);
      });
      this.commonDataService.getEnquiryStatuses().then(result => {
        this.userDataService.setAllEnquiryStatuses(result);
      });
      this.commonDataService.getCommunicationFors().then(result => {
        this.userDataService.setCommunicationFors(result);
      });
      this.commonDataService.getCommunicationMediums().then(result => {
        this.userDataService.setCommunicationMediums(result);
      });
      this.commonDataService.getProcessStatus().then(result => {
        this.userDataService.setProcessStatuses(result);
      });
      this.commonDataService.getEmployees().then(result => {
        this.userDataService.setEmployees(result);
      });
      this.commonDataService.getOccupation().then(result => {
        this.userDataService.setOccupation(result);
      });

      this.commonDataService.getCities().then(result => {
        this.userDataService.setCities(result);
        this.commonDataService.getBranches().then(result => {
          this.userDataService.setBranches(result);
          this.loadData();
          this.length = this.data.length;
          this.onChangeTable(this.config);
        });
      });
    });
  }

  loadData() {
    var temp = [];
    this.users.forEach((user,index) => {
      var person = {
        'id': user.entity_key_id,
        'name': user.fname + ' ' + user.lname,
        'mobile': user.mobno,
        'email': user.mailid,
        'city': this.getCityNameByCityId(user.city),
        'branch': this.getBranchNameByBranchId(user.branch)
      };
      temp.push(person);
      if(index == this.users.length-1){
        this.data = temp;
        this.data = [...this.data];
      }
    });

  }

  navigateToRoute(user) {
    var id = user != undefined ? user.id : '1';
    this.router.navigate(['dashboard/users/new-user', id]);
  }

  navigateToPersonSummary(user) {
    var id = user != undefined ? user.entity_key_id : '1';
    this.router.navigate(['dashboard/users/person-summary', id]);
  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New Contact',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewUserComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      console.log("modal close: " + reason);
      this.getAllContacts(this.currentStartIndex, this.currentEndIndex);
      this.loadData();
    })
  }

  getCityNameByCityId(cityId) {
    var city = this.userDataService.getCityNameByCityId(cityId);
    if (city == undefined) {
      return 'NA';
    } else {
      return city.cityName;
    }
  }

  getBranchNameByBranchId(branchId) {
    var branch = this.userDataService.getBranchNameAndLocationByBranchId(branchId);
    if (branch == undefined) {
      return 'NA';
    } else {
      return branch.title + ', ' + this.getCityNameByCityId(branch.location);
    }
  }

}
