import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { NewUserComponent } from './new-user/new-user.component';
import { UserSummaryComponent } from './user-summary/user-summary.component';


const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    data: {
      title: 'Users'
    }
  },
  {
    path: 'new-user/:id',
    component: NewUserComponent ,
    data: {
      title: 'Create new users'
    }
  },
  {
    path: 'person-summary/:id',
    component: UserSummaryComponent ,
    data: {
      title: 'Person Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
