import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataService } from './user-data.service';
import { ApiService } from '../../../service/api.service';
import { CommonDataService } from '../../../service/common-data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(private router: Router, private apiService: ApiService,
    private commonDataService: CommonDataService,
    private userDataService: UserDataService) { }

  ngOnInit() {
  }
}
