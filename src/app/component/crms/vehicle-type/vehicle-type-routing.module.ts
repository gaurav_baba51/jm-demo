import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { VehicleTypeComponent } from './vehicle-type.component';
import { NewVehicleTypeComponent } from './new-vehicle-type/new-vehicle-type.component';
import { VehicleTypeSummaryComponent } from './vehicle-type-summary/vehicle-type-summary.component';


const routes: Routes = [
  {
    path: '',
    component: VehicleTypeComponent,
    data: {
      title: 'Vehicle Type'
    }
  },
  {
    path: 'new-vehicle-type/:id',
    component: NewVehicleTypeComponent ,
    data: {
      title: 'Create new vehicle type'
    }
  },
  {
    path: 'vehicle-type-summary/:id',
    component: VehicleTypeSummaryComponent ,
    data: {
      title: 'Vehicle Type Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleTypeRoutingModule {}
