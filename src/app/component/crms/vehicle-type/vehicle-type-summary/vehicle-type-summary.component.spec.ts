import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleTypeSummaryComponent } from './vehicle-type-summary.component';

describe('VehicleTypeSummaryComponent', () => {
  let component: VehicleTypeSummaryComponent;
  let fixture: ComponentFixture<VehicleTypeSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleTypeSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleTypeSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
