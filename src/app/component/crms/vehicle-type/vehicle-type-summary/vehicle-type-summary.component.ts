import { Component, OnInit } from '@angular/core'; 
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { VehicleTypeDataService } from '../vehicle-type-data.service';
import { NewVehicleTypeComponent } from '../new-vehicle-type/new-vehicle-type.component';

@Component({
  selector: 'app-vehicle-type-summary',
  templateUrl: './vehicle-type-summary.component.html',
  styleUrls: ['./vehicle-type-summary.component.scss']
})
export class VehicleTypeSummaryComponent implements OnInit {

  entityTitle = "Vehicle Type";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private dataService: VehicleTypeDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.dataService.getEntityById(this.id);
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewVehicleTypeComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }
}
