import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVehicleTypeComponent } from './new-vehicle-type.component';

describe('NewVehicleTypeComponent', () => {
  let component: NewVehicleTypeComponent;
  let fixture: ComponentFixture<NewVehicleTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVehicleTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVehicleTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
