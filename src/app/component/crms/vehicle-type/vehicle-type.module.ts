import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { Ng2TableModule } from 'ngx-datatable/ng2-table';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NewVehicleTypeComponent } from './new-vehicle-type/new-vehicle-type.component';
import { VehicleTypeComponent } from './vehicle-type.component';
import { VehicleTypeListComponent } from './vehicle-type-list/vehicle-type-list.component';
import { VehicleTypeSummaryComponent } from './vehicle-type-summary/vehicle-type-summary.component';
import { VehicleTypeDataService } from '../vehicle-type/vehicle-type-data.service';
import { VehicleTypeRoutingModule } from './vehicle-type-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    VehicleTypeRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    Ng2TableModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  entryComponents: [
    NewVehicleTypeComponent
  ],
  declarations: [
    VehicleTypeComponent,
    VehicleTypeListComponent,
    NewVehicleTypeComponent,
    VehicleTypeSummaryComponent],
  providers: [VehicleTypeDataService]
})
export class VehicleTypeModule { }
