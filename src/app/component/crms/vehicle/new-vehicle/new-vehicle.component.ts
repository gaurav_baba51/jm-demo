import { Component, OnInit, EventEmitter } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Options } from 'ng5-slider';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { CommonDataService } from '../../../../service/common-data.service';
import { VehicleDataService } from '../vehicle-data.service';
import { ApiService } from '../../../../service/api.service';

@Component({
  selector: 'app-new-vehicle',
  templateUrl: './new-vehicle.component.html',
  styleUrls: ['./new-vehicle.component.scss']
})
export class NewVehicleComponent implements OnInit {

  vehicle: any;
  isBasicDetailCollapsed = false;
  isExteriorCollapsed = true;
  isInteriorCollapsed = true;
  isSystemsAndFunctionsCollapsed = true;
  isSuspensionsAndBrakesCollapsed = true;
  isWheelsDetailsCollapsed = true;
  isHoodCollapsed = true;
  isUnderBodyCollapsed = true;
  isVehicleDocumentCollapsed = true;
  bsValue = new Date();
  isTransmissionOther: Boolean = false;
  isFuelTypeOther: Boolean = false;
  contacts: any;
  targetProducts: any;
  vehicleTypes: any;
  vehicleCategories: any;
  years: any;
  vehicleImages = [];

  modalRef: BsModalRef;
  manualRefresh: EventEmitter<void> = new EventEmitter<void>();

  runningValue: number = 30000;
  runningOptions: Options = {
    floor: 0,
    ceil: 100000,
    step: 1000
  };

  tyreLifeValue: number = 50;
  tyreLifeOptions: Options = {
    floor: 0,
    ceil: 100,
    step: 5
  };



  constructor(public bsModalRef: BsModalRef,
    private commonDataService: CommonDataService,
    public modalService: BsModalService,
    private toastr: ToastrService,
    private vehicleDataService: VehicleDataService,
    private apiService: ApiService) {
    this.vehicle = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {
      documents: 'no',
      hpa_status: 'no',
      bank_noc: 'no',
      damage_status: 'no',
      accident_status: 'no',
      dentnscratches: 'no',
      insurance: 'no',
      jackandtommy: 'no',
      wheelspanner: 'no',
      abs: 'no',
      airbag: 'no',
      esp: 'no',
      reverseparkingassist: 'no',
      rc_status: 'no',
      fcvalidity: 'no',
      servicelog: 'no'
    };

    this.contacts = this.vehicleDataService.getContacts();
    this.targetProducts = this.vehicleDataService.getTargetProducts();
    this.vehicleCategories = this.vehicleDataService.getVehicleCategorys();
    this.vehicleTypes = this.vehicleDataService.getVehicleTypes();

   /*  if (this.vehicle.running != undefined) {
      this.runningValue = this.vehicle.running;
    } */

    if (this.vehicle.rem_tyre_life != undefined) {
      this.tyreLifeValue = this.vehicle.rem_tyre_life;
    }

    this.years = this.getYears(1990);

    console.log(this.years);
    this.getBlobData();
  }

  getYears(startYear) {
    var currentYear = new Date().getFullYear(), years = [];
    startYear = startYear || 1990;

    while (startYear <= currentYear) {
      years.push(startYear++);
    }

    return years;
  }

  ngOnInit() {
  }

  toggleDocuments(event) {
    if (event.target.checked) {
      this.vehicle.documents = 'yes';
    } else {
      this.vehicle.documents = 'no';
    }
  }

  toggleHPAStatus(event) {
    if (event.target.checked) {
      this.vehicle.hpa_status = 'yes';
    } else {
      this.vehicle.hpa_status = 'no';
    }
  }

  toggleBankNoc(event) {
    if (event.target.checked) {
      this.vehicle.bank_noc = 'yes';
    } else {
      this.vehicle.bank_noc = 'no';
    }
  }

  toggleDamageStatus(event) {
    if (event.target.checked) {
      this.vehicle.damage_status = 'yes';
    } else {
      this.vehicle.damage_status = 'no';
    }
  }

  toggleAccidentStatus(event) {
    if (event.target.checked) {
      this.vehicle.accident_status = 'yes';
    } else {
      this.vehicle.accident_status = 'no';
    }
  }

  toggleDentScratches(event) {
    if (event.target.checked) {
      this.vehicle.dentnscratches = 'yes';
    } else {
      this.vehicle.dentnscratches = 'no';
    }
  }

  toggleInsuranceStatus(event) {
    if (event.target.checked) {
      this.vehicle.insurance = 'yes';
    } else {
      this.vehicle.insurance = 'no';
    }
  }

  toggleJack(event) {
    if (event.target.checked) {
      this.vehicle.jackandtommy = 'yes';
    } else {
      this.vehicle.jackandtommy = 'no';
    }
  }

  toggleWheelSpammer(event) {
    if (event.target.checked) {
      this.vehicle.wheelspanner = 'yes';
    } else {
      this.vehicle.wheelspanner = 'no';
    }
  }

  toggleABS(event) {
    if (event.target.checked) {
      this.vehicle.abs = 'yes';
    } else {
      this.vehicle.abs = 'no';
    }
  }

  toggleAirbags(event) {
    if (event.target.checked) {
      this.vehicle.airbag = 'yes';
    } else {
      this.vehicle.aairbagbs = 'no';
    }
  }

  toggleESP(event) {
    if (event.target.checked) {
      this.vehicle.esp = 'yes';
    } else {
      this.vehicle.esp = 'no';
    }
  }

  toggleReverseParkingAssist(event) {
    if (event.target.checked) {
      this.vehicle.reverseparkingassist = 'yes';
    } else {
      this.vehicle.reverseparkingassist = 'no';
    }
  }

  toggleRCStatus(event) {
    if (event.target.checked) {
      this.vehicle.rcstatusdoc = 'yes';
    } else {
      this.vehicle.rcstatusdoc = 'no';
    }
  }

  toggleFC(event) {
    if (event.target.checked) {
      this.vehicle.fcvalidity = 'yes';
    } else {
      this.vehicle.fcvalidity = 'no';
    }
  }

  toggleServiceLog(event) {
    if (event.target.checked) {
      this.vehicle.servicelog = 'yes';
    } else {
      this.vehicle.servicelog = 'no';
    }
  }

  submit(vehicle) {
    console.log(vehicle);
    if (vehicle.entity_key_id != undefined) {
      this.update(vehicle);
    } else {
      this.create(vehicle);
    }
  }

  update(vehicle) {
    vehicle.entity_name = 'vehicle';
    vehicle.running = this.runningValue;
    vehicle.rem_tyre_life = this.tyreLifeValue;
    this.apiService.updateEntity(vehicle).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        debugger;
        this.updateBlobImage(result, this.vehicleImages);
        this.toastr.success('Vehicle Updated Successfully');
      } else {
        this.toastr.error('Failed to Update Vehicle');
      }
    }).catch(error => {
      this.toastr.error('Failed to update Vehicle');
    });
  }

  create(vehicle) {
    vehicle.entity_name = 'vehicle';
    vehicle.trans_status = 'created';
    vehicle.running = this.runningValue;
    vehicle.rem_tyre_life = this.tyreLifeValue;
    this.apiService.addEntity(vehicle).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        debugger;
        /* this.vehicleImages.forEach(file => {
          this.addBlobImage(result.entityKey, file);
        }) */
        this.addBlobImage(result.entityKey, this.vehicleImages);
        this.toastr.success('Vehicle Added Successfully');
      } else {
        this.toastr.error('Failed to add Vehicle');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Vehicle');
    });
  }

  collapsed(event: any): void {
    console.log(event);
    this.manualRefresh.emit();
  }

  expanded(event: any): void {
    console.log(event);
    this.manualRefresh.emit();
  }

  onTransmissionSelect(transmission): void {
    this.vehicle.transmission = transmission;
    if (this.vehicle.transmission == 3) {
      this.isTransmissionOther = true;
    } else {
      this.isTransmissionOther = false;
    }
  }

  onFuelTypeSelect(fuelType): void {
    this.vehicle.fueltype = fuelType;
    if (this.vehicle.fueltype == 5) {
      this.isFuelTypeOther = true;
    } else {
      this.isFuelTypeOther = false;
    }
  }

  onBankNOC(bankNOC): void {
    this.vehicle.bank_noc = bankNOC;
  }

  onDamageStatus(damageStatus): void {
    this.vehicle.damage_status = damageStatus;
  }

  onAccidentStatus(accidentStatus): void {
    this.vehicle.accident_status = accidentStatus;
  }

  onDentScratches(dentScratches): void {
    this.vehicle.dentnscratches = dentScratches;
  }

  onInsuranceStatus(dentScratches): void {
    this.vehicle.insurance = dentScratches;
  }

  onFrontBonnetSelect(rating): void {
    this.vehicle.frontbonnet = rating;
  }

  onLicensePlate(rating): void {
    this.vehicle.licencesplate = rating;
  }

  onFrontbumper(rating): void {
    this.vehicle.frontbumper = rating;
  }

  onRHSideFender(rating): void {
    this.vehicle.rhfender = rating;
  }

  onRHFrontDoor(rating): void {
    this.vehicle.rhfrontdoor = rating;
  }

  onRHRearDoor(rating): void {
    this.vehicle.rhreardoor = rating;
  }

  onRHQuarterPanel(rating): void {
    this.vehicle.rhquarterpanel = rating;
  }

  onBoot(rating): void {
    this.vehicle.boot = rating;
  }

  onRearBumper(rating): void {
    this.vehicle.rearbumper = rating;
  }

  onLHQuarterPanel(rating): void {
    this.vehicle.lhquarterpanel = rating;
  }

  onLHSideRearDoor(rating): void {
    this.vehicle.lhreardoor = rating;
  }

  onLHSideFrontDoor(rating): void {
    this.vehicle.lhfrontdoor = rating;
  }

  onLHSideFender(rating): void {
    this.vehicle.lhfender = rating;
  }

  onWindshieldFront(rating): void {
    this.vehicle.windshieldfront = rating;
  }

  onRearGlass(rating): void {
    this.vehicle.rearglass = rating;
  }

  onRoof(rating): void {
    this.vehicle.roof = rating;
  }

  onPillars(rating): void {
    this.vehicle.pillars = rating;
  }

  onSideViewMirrors(rating): void {
    this.vehicle.sidemirrors = rating;
  }

  onFuelLid(rating): void {
    this.vehicle.fuellid = rating;
  }

  onPaintOverspray(rating): void {
    this.vehicle.paintoverspray = rating;
  }

  onPaintPeelOff(rating): void {
    this.vehicle.paintpeeloff = rating;
  }

  onPaintCondition(rating): void {
    this.vehicle.paintcondition = rating;
  }

  onRunnignBoard(rating): void {
    this.vehicle.runningboard = rating;
  }

  onLights(rating): void {
    this.vehicle.lights = rating;
  }

  onFontBumperNet(rating): void {
    this.vehicle.frontbumpernet = rating;
  }

  onViper(rating): void {
    this.vehicle.viper = rating;
  }

  onDoorHandle(rating): void {
    this.vehicle.doorhandle = rating;
  }

  onGuardFront(rating): void {
    this.vehicle.frontguard = rating;
  }

  onGuardRear(rating): void {
    this.vehicle.rearguard = rating;
  }

  onRoofCarrier(rating): void {
    this.vehicle.roofcarrier = rating;
  }

  onLicenceLights(rating): void {
    this.vehicle.licenceplatelights = rating;
  }

  onFuelFilter(rating): void {
    this.vehicle.fuelfilter = rating;
  }

  onSteering(rating): void {
    this.vehicle.steering = rating;
  }

  onHinges(rating): void {
    this.vehicle.hinges = rating;
  }

  onDashboard(rating): void {
    this.vehicle.dashboard = rating;
  }

  onInteriorTrims(rating): void {
    this.vehicle.interiortrims = rating;
  }

  onCombinationSwitchAssembly(rating): void {
    this.vehicle.switchassembly = rating;
  }

  onSeatBeltCondition(rating): void {
    this.vehicle.seatbeltcondition = rating;
  }

  onInteriorAccessories(rating): void {
    this.vehicle.accessoriesinterior = rating;
  }

  onHandBreak(rating): void {
    this.vehicle.handbreak = rating;
  }

  onPedals(rating): void {
    this.vehicle.pedals = rating;
  }

  onHorns(rating): void {
    this.vehicle.horns = rating;
  }

  onCarpet(rating): void {
    this.vehicle.carpet = rating;
  }

  onOdometer(rating): void {
    this.vehicle.odometer = rating;
  }

  onHazardLights(rating): void {
    this.vehicle.hazardlight = rating;
  }

  onCentralLock(rating): void {
    this.vehicle.centrallock = rating;
  }

  onCruiseControl(rating): void {
    this.vehicle.cruisecontrol = rating;
  }

  onBootCondition(rating): void {
    this.vehicle.bootcondition = rating;
  }

  onJack(rating): void {
    this.vehicle.jackandtommy = rating;
  }

  onWheelSpammer(rating): void {
    this.vehicle.wheelspanner = rating;
  }

  onRearCamera(rating): void {
    this.vehicle.rearcamera = rating;
  }

  onUpholstery(rating): void {
    this.vehicle.upholstery = rating;
  }

  onCeilingTop(rating): void {
    this.vehicle.ceillingtop = rating;
  }

  onHandlesTop(rating): void {
    this.vehicle.handlestop = rating;
  }

  onRearViewMirror(rating): void {
    this.vehicle.inrearmirror = rating;
  }

  onSeatingAdjustement(rating): void {
    this.vehicle.seatingadjustment = rating;
  }

  onSunProtection(rating): void {
    this.vehicle.sunprotection = rating;
  }

  onToolkit(rating): void {
    this.vehicle.toolkit = rating;
  }

  onAccessoriesSystem(rating): void {
    this.vehicle.accessoriessystem = rating;
  }

  onTransmissionControl(rating): void {
    this.vehicle.transmisioncontrol = rating;
  }

  onGearBox(rating): void {
    this.vehicle.gears = rating;
  }

  onACBlower(rating): void {
    this.vehicle.acblower = rating;
  }

  onCooling(rating): void {
    this.vehicle.cooling = rating;
  }

  onHeater(rating): void {
    this.vehicle.heater = rating;
  }

  onABS(rating): void {
    this.vehicle.abs = rating;
  }

  onMusicSystem(rating): void {
    this.vehicle.musicsystem = rating;
  }

  onAirbags(rating): void {
    this.vehicle.airbag = rating;
  }

  onESP(rating): void {
    this.vehicle.esp = rating;
  }

  onReverseParking(rating): void {
    this.vehicle.reverseparkingassist = rating;
  }

  onStarterMotor(rating): void {
    this.vehicle.startermoter = rating;
  }

  onAlternator(rating): void {
    this.vehicle.alternator = rating;
  }

  onACCompressor(rating): void {
    this.vehicle.accompressor = rating;
  }

  onViperAssembly(rating): void {
    this.vehicle.wiperassembly = rating;
  }

  onBatteryCondition(rating): void {
    this.vehicle.batterycondition = rating;
  }

  onIgnition(rating): void {
    this.vehicle.ignition = rating;
  }

  onHeadAndTailLamp(rating): void {
    this.vehicle.headndtaillamp = rating;
  }

  onFogLamp(rating): void {
    this.vehicle.foglamp = rating;
  }

  onRemoteLock(rating): void {
    this.vehicle.remotelock = rating;
  }

  onIndicatorsandParkingLamop(rating): void {
    this.vehicle.indicators = rating;
  }

  onInteriorLights(rating): void {
    this.vehicle.interiorlight = rating;
  }

  onPowerWindow(rating): void {
    this.vehicle.powerwindow = rating;
  }

  onAutoPArking(rating): void {
    this.vehicle.autoparking = rating;
  }

  onFrontStrut(rating): void {
    this.vehicle.frontstrut = rating;
  }

  onSteeringBoxAssembly(rating): void {
    this.vehicle.steeringbox = rating;
  }

  onPowerSteering(rating): void {
    this.vehicle.powersteering = rating;
  }

  onRearShock(rating): void {
    this.vehicle.rearshock = rating;
  }

  onFrontShock(rating): void {
    this.vehicle.frontshock = rating;
  }

  onFrontDiscPads(rating): void {
    this.vehicle.frontdiscandpads = rating;
  }

  onrearDrumShoes(rating): void {
    this.vehicle.reardrumandshoes = rating;
  }

  onTyreCondition(rating): void {
    this.vehicle.tyrecondition = rating;
  }

  onSpareTyreCondition(rating): void {
    this.vehicle.sparetyre = rating;
  }

  onAlloyWheels(rating): void {
    this.vehicle.alloywheels = rating;
  }

  onDisc(rating): void {
    this.vehicle.disc = rating;
  }

  onBonnetInterior(rating): void {
    this.vehicle.bonnetinterior = rating;
  }

  onlhAppron(rating): void {
    this.vehicle.lhapron = rating;
  }

  onrhAppron(rating): void {
    this.vehicle.rhapron = rating;
  }

  onRadiator(rating): void {
    this.vehicle.radiator = rating;
  }

  onWiring(rating): void {
    this.vehicle.wiring = rating;
  }

  onBrakeFluidLevel(rating): void {
    this.vehicle.brakefluidlevel = rating;
  }

  onEngineOil(rating): void {
    this.vehicle.engineoil = rating;
  }

  onOilFluidLeakage(rating): void {
    this.vehicle.oilandfluidleakage = rating;
  }

  onCoolantLevel(rating): void {
    this.vehicle.coolantlevel = rating;
  }

  onBrakeNdCoolantHose(rating): void {
    this.vehicle.brakecoolantpipe = rating;
  }

  onUndercarriage(rating): void {
    this.vehicle.undercarriage = rating;
  }

  onChassisAndVehicleFrame(rating): void {
    this.vehicle.chassisandvehicleframe = rating;
  }

  onExhaustSystem(rating): void {
    this.vehicle.exhaustsystem = rating;
  }

  onAirCleaner(rating): void {
    this.vehicle.aircleaner = rating;
  }

  onJointCross(rating): void {
    this.vehicle.jointcross = rating;
  }

  onOilChamber(rating): void {
    this.vehicle.oilchember = rating;
  }

  onRCStatus(rating): void {
    this.vehicle.rc_status = rating;
  }

  onFCValiudity(rating): void {
    this.vehicle.fcvalidity = rating;
  }

  onVehicleServiceLog(rating): void {
    this.vehicle.servicelog = rating;
  }

  onFileChange(event) {
    console.log(event);
    if (event.target.files && event.target.files.length > 0) {
      var length = event.target.files.length;
      for (let i = 0; i < length; i++) {
        this.readFile(event.target.files[i]);
      }
      console.log(this.vehicleImages);
    }
  }

  readFile(file) {
    let reader = new FileReader();
    reader.onload = (e) => {
      this.vehicleImages.push(reader.result);
    }
    reader.readAsDataURL(file);
  }

  addBlobImage(id, vehicleImage) {
    var image = {};
    image['entity_name'] = "vehicle";
    image['entity_key_id'] = id;
    image['imagestring'] = vehicleImage;
    this.apiService.addBlobImageMultiple(image).then(result => {
      console.log("result:" + result);
    })
  }

  updateBlobImage(id, vehicleImage) {
    var image = {};
    image['entity_name'] = "vehicle";
    image['entity_key_id'] = id;
    image['imagestring'] = vehicleImage;
    this.apiService.updateBlobImageMultiple(image).then(result => {
      console.log("result:" + result);
    })
  }

  getBlobData(){
    this.apiService.getImageEntityById(16, this.vehicle.entity_key_id).then(result=>{
      this.vehicleImages = JSON.parse(result['blobValue']);
    });
  }

  removeImage(index){
    this.vehicleImages.splice(index, 1);
  }
}
