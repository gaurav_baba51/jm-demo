import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDataService } from './user-data.service';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { VehicleDataService } from '../vehicle-data.service';


@Component({
  selector: 'app-vehicle-transaction',
  templateUrl: './vehicle-transaction.component.html',
  styleUrls: ['./vehicle-transaction.component.scss']
})

export class VehicleTransactionComponent implements OnInit {
  
  public transactiontype:any;
  public currentDate:any;
  id: any;
  showForm:any = false;
  contacts: any;
  user:any;
  branches: any;
  cities: any;
  targetProducts:any;
  vehicle:any;
  constructor(private route: ActivatedRoute,
    private userDataService: UserDataService,
    private VehicleDataService: VehicleDataService,
    private router: Router,private apiService: ApiService,
    private commonDataService: CommonDataService,) { 

     this.id = this.route.snapshot.params['id'];
  }

  ngOnInit() {

           console.log("in init");
          this.apiService.getEntityFromId('vehicle',this.id).then(result => {
            this.vehicle = result;
            console.log(this.vehicle);
          }).catch(error => console.log(error));


 		      this.commonDataService.getCities().then(result => {
		        this.userDataService.setCities(result);
		        this.cities = this.userDataService.getCities();
		        this.commonDataService.getBranches().then(result => {
		          this.userDataService.setBranches(result);
		          this.branches = this.userDataService.getBranches();
		        });
		      });

		      this.commonDataService.getMake().then(result => {
				
					  this.commonDataService.getModel().then(result => {
 
					  }); 	
		      });

	   this.getAllContacts();
       this.currentDate = new Date(); 	
  }


	onChange(deviceValue) {
 	   this.transactiontype = deviceValue;
 	   this.showForm = true;
	 
	}


  getAllContacts() {
    this.commonDataService.getContacts().then(result => {
      this.contacts = result;
      this.userDataService.setAllUsers(result);

    });
  }


  getCity(id) {
    return this.userDataService.getCityNameByCityId(id);
  }


}

