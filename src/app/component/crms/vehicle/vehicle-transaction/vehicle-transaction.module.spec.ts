import { VehicleTransactionModule } from './vehicle-transaction.module';

describe('VehicleTransactionModule', () => {
  let vehicleTransactionModule: VehicleTransactionModule;

  beforeEach(() => {
    vehicleTransactionModule = new VehicleTransactionModule();
  });

  it('should create an instance', () => {
    expect(vehicleTransactionModule).toBeTruthy();
  });
});
