import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {VehicleTransactionComponent} from './vehicle-transaction.component';

const routes: Routes = [
  {
    path: '',
    component: VehicleTransactionComponent,
    data: {
      title: 'Vehicle Transaction'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleTransactionRoutingModule { }
