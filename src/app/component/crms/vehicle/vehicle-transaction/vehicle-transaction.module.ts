import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { VehicleTransactionRoutingModule } from './vehicle-transaction-routing.module';


@NgModule({
  imports: [
    CommonModule,
    VehicleTransactionRoutingModule,
    FormsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
  ],
  declarations: [
  
  ]
})
export class VehicleTransactionModule { }
