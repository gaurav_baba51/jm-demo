import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  users: any;
  branches : any = [];
  cities: any;
  targetProducts: any = [];
  constructor() { }

   getAllUsers() {
    return this.users;
  }

  setAllUsers(users) {
    this.users = users;
  }

  setBranches(branches) {
    this.branches = branches;
  }

   getBranches() {
   	console.log( this.branches);
    return this.branches;
  }


  getCityNameByCityId(cityId) {
    for (var city in this.cities) {
      if (this.cities[city].entity_key_id == cityId) {
        return this.cities[city];
      }
    }
  }

  setCities(cities) {
    this.cities = cities;
  }

  getCities() {
    return this.cities;
  }

  getTargetProducts() {
    return this.targetProducts;
  }

   setTargetProducts(targetProducts) {
    this.targetProducts = targetProducts
  } 

}
