import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';

import { NewVehicleComponent } from './new-vehicle/new-vehicle.component';
import { VehicleSummaryComponent } from './vehicle-summary/vehicle-summary.component';
import { VehicleTransactionComponent } from './vehicle-transaction/vehicle-transaction.component';
import { VehicleListComponent } from './vehicle-list/vehicle-list.component';
import { VehicleComponent } from './vehicle.component';

const routes: Routes = [
  {
    path: '',
    component: VehicleComponent,
    data: {
      title: 'Vehicles'
    }
  },
  {
    path: 'new-vehicle/:id',
    component: NewVehicleComponent ,
    data: {
      title: 'Create New Vehicle'
    }
  },
  {
    path: 'vehicle-summary/:id',
    component: VehicleSummaryComponent ,
    data: {
      title: 'Vehicle Summary'
    }
  },
  {
    path: 'vehicle-transaction/:id',
    component: VehicleTransactionComponent ,
    data: {
      title: 'Vehicle Transaction'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleRoutingModule { }
