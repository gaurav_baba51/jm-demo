import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VehicleDataService {

  constructor() { }

  vehicles: any = [];
  fuelType: any = [];
  targetProducts: any = [];
  contacts: any = [];
  vehicleTypes: any = [];
  vehicleCategorys: any = [];
  transmissions: any = [];

  getTransmissions() {
    return this.transmissions;
  }

  setTransmission(transmissions) {
    this.transmissions = transmissions;
  }

  getTransmissionsById(id) {
    for (var transmission of this.transmissions) {
      if (transmission.entity_key_id == id) {
        return transmission;
      }
    }
  }

  getVehicleTypes() {
    return this.vehicleTypes;
  }

  setVehicleTypes(vehicleTypes) {
    this.vehicleTypes = vehicleTypes;
  }

  getVehicleTypeById(id) {
    for (var vehicleType of this.vehicleTypes) {
      if (vehicleType.entity_key_id == id) {
        return vehicleType;
      }
    }
  }

  getVehicleCategorys() {
    return this.vehicleCategorys;
  }

  setVehicleCategorys(vehicleCategories) {
    this.vehicleCategorys = vehicleCategories
  }

  getVehicleCategoryById(id) {
    for (var vehicleCategory of this.vehicleCategorys) {
      if (vehicleCategory.entity_key_id == id) {
        return vehicleCategory;
      }
    }
  }

  getTargetProducts() {
    return this.targetProducts;
  }

  setTargetProducts(targetProducts) {
    this.targetProducts = targetProducts
  }

  getTargetProductById(id) {
    for (var targetProduct of this.targetProducts) {
      if (targetProduct.entity_key_id == id) {
        return targetProduct;
      }
    }
  }

  getContacts(){
    return this.contacts;
  }

  setContacts(contacts) {
    this.contacts = contacts;
  }

  getContactByContactId(contactId) {
    for (var contact of this.contacts) {
      if (contact.entity_key_id == contactId) {
        return contact;
      }
    }
  }

  getFuelType() {
    return this.fuelType;
  }

  setFuelType(fuelType) {
    this.fuelType = fuelType;
  }

  getFuelTypeByFuelId(fuelId) {
    for (var fuel of this.fuelType) {
      if (fuel.entity_key_id == fuelId) {
        return fuel;
      }
    }
  }

  getVehicles() {
    return this.vehicles;
  }

  setVehicles(vehicles) {
    this.vehicles = vehicles;
  }

  getVehicleByVehicleId(vehicleId) {
    for (var vehicle of this.vehicles) {
      if (vehicle.entity_key_id == vehicleId) {
        return vehicle;
      }
    }
  }

}
