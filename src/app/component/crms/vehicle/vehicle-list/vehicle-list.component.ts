import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';

import { NewVehicleComponent } from '../new-vehicle/new-vehicle.component';
import { CommonDataService } from '../../../../service/common-data.service';
import { ApiService } from '../../../../service/api.service';
import { VehicleDataService } from '../vehicle-data.service';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.scss']
})
export class VehicleListComponent implements OnInit {
  
  vehicles: any;
  bsModalRef: BsModalRef; 
  paginatedEnquiries;

  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {

    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);
  }

  doPagination(start, end) {
    this.paginatedEnquiries = this.vehicles.slice(start, end);
  }

  constructor(private apiService: ApiService,
    private router: Router, private modalService: BsModalService,
    private commonDataService: CommonDataService, private vehicleDataService: VehicleDataService) {
      this.commonDataService.getVehicleCategories().then(result => {
        this.vehicleDataService.setVehicleCategorys(result);
      });
      this.commonDataService.getVehicleTypes().then(result => {
        this.vehicleDataService.setVehicleTypes(result);
      });
      this.commonDataService.getTransmissions().then(result => {
        this.vehicleDataService.setTransmission(result);
      });
    }

  ngOnInit() {
    this.currentEndIndex = this.pageSize;
    this.currentStartIndex = 0;
    this.getVehicles(this.currentStartIndex, this.currentEndIndex);
  }

  getVehicles(start, end) {

    this.apiService.getEntity('vehicle').then(result => {
      this.vehicles = result;


      this.loadData();
      this.length = this.data.length;

     
      this.onChangeTable(this.config);
      this.totalItems = this.vehicles.length;
   
      this.doPagination(start, end);

    }).catch(error => console.log(error));




  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New Vehicle',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewVehicleComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  navigateToVehicleSummary(vehicle) {
    var id = vehicle != undefined ? vehicle.entity_key_id : ':id';
    this.router.navigate(['dashboard/vehicles/vehicle-summary', id]);
  }

  getFuelNameByFuelId(fuelId) {
    var fuelType = this.vehicleDataService.getFuelTypeByFuelId(fuelId);
    return fuelType != undefined ? fuelType.mfueltype : undefined;
  }

  getContactByContactId(contactId) {
    var contact = this.vehicleDataService.getContactByContactId(contactId);
    return contact;
  }


  public rows: Array<any> = [];
  public columns: Array<any> = [
    { title: 'Id', name: 'entity_key', sort: 'asc'/* , filtering: {filterString: '', placeholder: 'Filter by name'} */ },
    { title: 'Vehicle Number', name: 'number' },
    { title: 'Make', name: 'make' },
    { title: 'Model', name: 'model' },
    { title: 'Variant', name: 'variant' },
    { title: 'Date', name: 'createddate' },
    { title: 'Age Days', name: 'age' },
    { title: 'Enquiry Count', name: 'enquiry' },
    { title: 'Status', name: 'trans_status' },
    { title: 'Transaction', name: 'transact' }
    /*{ title: 'Running Kms', name: 'kms' },
    { title: 'Fuel Type', name: 'fuel' },
    { title: 'Vehicle Owner', name: 'owner' },
    { title: 'Owner Contact Number', name: 'ownermobile' }*/
  ];

  loadData() {

    
    this.vehicles.forEach(vehicle => {
     

      var personInfo = this.getContactByContactId(vehicle.personid);
      var fuelInfo = this.getFuelNameByFuelId(vehicle.fueltype);
      var ownerName = "";
      var ownermobile = "";
      if (personInfo != undefined) {
        ownerName = personInfo.fname + ' ' + personInfo.lname;
        ownermobile = personInfo.mobno
      }
      var vehicleTarget = this.getTargetProductById(vehicle.vehiclemodel);

      


        
        var today : any = new Date();
        var dd :  any = today.getDate();
        var mm : any = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd = '0' + dd;
        } 
        if(mm<10){
            mm ='0' + mm;
        } 
        var today1: any = yyyy + '-' + mm + '-' + dd ;
   
       

        var startDate = Date.parse(vehicle.createddate);
            var endDate = Date.parse(today1);
            var timeDiff = endDate - startDate;
            var daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));

      
           
            if( true==vehicle.hasOwnProperty('variant')  && true==vehicle.variant.hasOwnProperty('model') && true==vehicle.variant.model.hasOwnProperty('make'))
              {

                var person = {
                  'entity_key': vehicle.entity_key,
                  'number': vehicle.regno,
                  'make':  vehicle.variant.model.make.name   ,
                  'model':  vehicle.variant.model.name  ,
                  'variant': vehicle.variant.name  ,
                  'createddate': vehicle.createddate,
                  'age': daysDiff + " days",
                  'trans_status':vehicle.trans_status,
                  'owner': ownerName,
                  'ownermobile': ownermobile,
                  'transact':' <span class="btn btn-block btn-primary"  >Transact</span>'
                };
              }

      
        
        this.data.push(person);
    
    });
  }

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  data: Array<any> = [];

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
   
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    console.log(filteredData); 
    filteredData.forEach((item: any) => {
      if(item!=undefined)
        {
          let flag = false;
          console.log(this.columns); 
          this.columns.forEach((column: any) => {
            if (item[column.name] != undefined && item[column.name].toString().match(this.config.filtering.filterString)) {
              flag = true;
            }
          });
          if (flag) {
            tempArray.push(item);
          }          
        }

    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {

    var id = data.row.id != undefined ? data.row.id : '1';
    if(data.column=='model' || data.column=='number'  || data.column=='trans_status' )
       this.router.navigate(['dashboard/vehicles/vehicle-summary', id]);
    else   
       this.router.navigate(['dashboard/vehicles/vehicle-transaction', id]);
  }

  getTargetProductById(id) {
    return this.vehicleDataService.getTargetProductById(id);
  }


 

}
