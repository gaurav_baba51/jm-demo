import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CollapseModule, CarouselModule } from 'ngx-bootstrap';
import { RatingModule } from 'ngx-bootstrap';
import { Ng2TableModule } from 'ngx-datatable/ng2-table';
import { ProgressbarModule } from 'ngx-bootstrap';

import { VehicleTransactionComponent } from './vehicle-transaction/vehicle-transaction.component';
import { NewVehicleComponent } from './new-vehicle/new-vehicle.component';
import { VehicleSummaryComponent } from './vehicle-summary/vehicle-summary.component';
import { VehicleListComponent } from './vehicle-list/vehicle-list.component';
import { VehicleComponent } from './vehicle.component';
import { VehicleRoutingModule } from './vehicle-routing.module';
import { VehicleDataService } from './vehicle-data.service';
import { Ng5SliderModule } from 'ng5-slider';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapSliderModule } from 'ngx-bootstrap-slider';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    VehicleRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    Ng2TableModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    RatingModule.forRoot(),
    CarouselModule.forRoot(),
    ProgressbarModule.forRoot(),
    BsDatepickerModule.forRoot(),
    Ng5SliderModule,
    NgxBootstrapSliderModule
  ],
  declarations: [VehicleComponent ,NewVehicleComponent, VehicleSummaryComponent, VehicleListComponent, VehicleTransactionComponent],
  providers: [VehicleDataService]
})
export class VehicleModule { }
