import { Component, OnInit, EventEmitter } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { CommonDataService } from '../../../../service/common-data.service';
import { NewVehicleComponent } from '../new-vehicle/new-vehicle.component';
import { VehicleDataService } from '../vehicle-data.service';
import { Options } from 'ng5-slider';
import { ApiService } from '../../../../service/api.service';

@Component({
  selector: 'app-vehicle-summary',
  templateUrl: './vehicle-summary.component.html',
  styleUrls: ['./vehicle-summary.component.scss']
})
export class VehicleSummaryComponent implements OnInit {

  max: number = 7;
  rate: number = 5;
  isReadonly: boolean = true;

  id: any;
  vehicle: any;
  originalVehicle: any;
  bsModalRef: BsModalRef;
  isBasicDetailCollapsed = false;
  isExteriorCollapsed = true;
  isInteriorCollapsed = true;
  isSystemsAndFunctionsCollapsed = true;
  isSuspensionsAndBrakesCollapsed = true;
  isWheelsDetailsCollapsed = true;
  isHoodCollapsed = true;
  isUnderBodyCollapsed = true;
  isVehicleDocumentCollapsed = true;

  minValue: number = 1;
  maxValue: number = 40;
  manualRefresh: EventEmitter<void> = new EventEmitter<void>();
  options: Options = {
    floor: 0,
    ceil: 100,
    readOnly: true
  };

  transmissions: any;
  fuelTypes: any;

  vehicleModel: any;
  transmission: any;
  fuel: any;
  vehicletype: any;
  contactName: any;
  contactMobile: any;
  vehicleImages:any = [];

  constructor(private route: ActivatedRoute, 
    private modalService: BsModalService,
    private vehicleDataService: VehicleDataService, 
    private commonDataService: CommonDataService,
    private apiService: ApiService) {
    this.id = this.route.snapshot.params['id'];
    this.vehicle = this.vehicleDataService.getVehicleByVehicleId(this.id);

    if (this.vehicle != undefined) {
      this.originalVehicle = this.vehicle;
    }

    if (this.vehicle == undefined) {
      this.commonDataService.getVehicles().then(result => {
        this.vehicleDataService.setVehicles(result);
        this.vehicle = this.vehicleDataService.getVehicleByVehicleId(this.id);
        this.originalVehicle = this.vehicle;
        this.getTargetProductBytpId(this.vehicle.vehiclemodel);
        this.getBlobData();
      });
    } else {
      this.getTargetProductBytpId(this.vehicle.vehiclemodel);
      this.getBlobData();
    }
  }

  ngOnInit() {
  }

  getBlobData(){
    this.apiService.getImageEntityById(16, this.id).then(result=>{
      this.vehicleImages = JSON.parse(result['blobValue']);
    });
  }

  openModalWithComponent() {
    const initialState = {
      data: this.vehicle,
      title: 'Update Vehicle',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewVehicleComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.modalService.onHide.subscribe((reason: string) => {
      debugger;
      this.getBlobData();
    })
  }

  collapsed(event: any): void {
    //console.log(event);
    this.manualRefresh.emit();
  }

  expanded(event: any): void {
    //console.log(event);
    this.manualRefresh.emit();
  }

  getTargetProductBytpId(id) {
    var targetProduct = this.vehicleDataService.getTargetProductById(id);
    if (targetProduct == undefined) {
      this.commonDataService.getTargetProducts().then(result => {
        this.vehicleDataService.setTargetProducts(result);
        targetProduct = this.vehicleDataService.getTargetProductById(id);
        this.vehicleModel = targetProduct.tmake + ' ' + targetProduct.tmodel;
        this.getTransmissionById(this.vehicle.transmission);
      });
    } else {
      this.vehicleModel = targetProduct.tmake + ' ' + targetProduct.tmodel;
      this.getTransmissionById(this.vehicle.transmission);
    }
  }

  getDate(date: any) {
    var dateVar = new Date(date);
    return dateVar.getDate() + '/' + dateVar.getMonth() + '/' + dateVar.getFullYear();
  }

  getTransmissionById(id) {
    var transmission = this.vehicleDataService.getTransmissionsById(id);
    if (transmission == undefined) {
      this.commonDataService.getTransmissions().then(result => {
        this.vehicleDataService.setTransmission(result);
        transmission = this.vehicleDataService.getTransmissionsById(id);
        if (transmission != undefined && transmission.mtransmission.toUpperCase() == 'other'.toUpperCase()) {
          transmission.mtransmission = transmission.transother;
        } else {
          this.transmission = transmission != undefined ?  transmission.mtransmission : "";
        }
        this.getFuelById(this.vehicle.fueltype);
      });
    } else {
      if (transmission.mtransmission.toUpperCase() == 'other'.toUpperCase()) {
        transmission.mtransmission = transmission.transother;
      } else {
        this.transmission = transmission.mtransmission;
      }
      this.getFuelById(this.vehicle.fueltype);
    }
  }

  getFuelById(id) {
    var transmission = this.vehicleDataService.getFuelTypeByFuelId(id);
    if (transmission == undefined) {
      this.commonDataService.getFuelTypes().then(result => {
        this.vehicleDataService.setFuelType(result);
        transmission = this.vehicleDataService.getFuelTypeByFuelId(id);
        if (transmission.mfueltype.toUpperCase() == 'other'.toUpperCase()) {
          this.fuel = transmission.fueltypeother;
        } else {
          this.fuel = transmission.mfueltype;
        }
        this.getVehicleType(this.vehicle.vehiclecategory);
      });
    } else {
      if (transmission.mfueltype.toUpperCase() == 'other'.toUpperCase()) {
        this.fuel = transmission.fueltypeother;
      } else {
        this.fuel = transmission.mfueltype;
      }
      this.getVehicleType(this.vehicle.vehiclecategory);
    }
  }

  getVehicleType(id) {
    var vehicle = this.vehicleDataService.getVehicleTypeById(id);
    if (vehicle == undefined) {
      this.commonDataService.getVehicleTypes().then(result => {
        this.vehicleDataService.setVehicleTypes(result);
        vehicle = this.vehicleDataService.getVehicleTypeById(id);
        this.vehicletype = vehicle.vehicletype;
        this.getContactById(this.vehicle.personid);
      });
    } else {
      this.vehicletype = vehicle.vehicletype;
      this.getContactById(this.vehicle.personid);
    }
  }

  getContactById(id) {
    var contact = this.vehicleDataService.getContactByContactId(id);
    if (contact == undefined) {
      this.commonDataService.getContacts().then(result => {
        this.vehicleDataService.setContacts(result);
        contact = this.vehicleDataService.getContactByContactId(id);
        this.contactName = contact.fname + ' ' + contact.lname;
        this.contactMobile = contact.mobno;
      });
    } else {
      this.contactName = contact.fname + ' ' + contact.lname;
      this.contactMobile = contact.mobno;
    }
  }

  getRCStatus(statusId) {
    if (statusId == 1) {
      return 'Original';
    } else if (statusId == 2) {
      return 'Photocopy';
    }
  }

  getRatingsString(rating) {
    if (rating == 1) {
      return 'Not Available';
    } else if (rating == 2) {
      return 'Missing';
    } else if (rating == 3) {
      return 'Bad';
    } else if (rating == 4) {
      return 'Not Working';
    } else if (rating == 5) {
      return 'Average';
    } else if (rating == 6) {
      return 'Good';
    } else if (rating == 7) {
      return 'Excellent';
    }
  }

  getStyle(rating) {
    if (rating == 1) {
      return 'danger';
    } else if (rating == 2) {
      return 'warning';
    } else if (rating == 3) {
      return 'warning';
    } else if (rating == 4) {
      return 'primary';
    } else if (rating == 5) {
      return 'primary';
    } else if (rating == 6) {
      return 'success';
    } else if (rating == 7) {
      return 'success';
    }
  }

}
