import { Component, OnInit } from '@angular/core';

import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { VarientDataService } from '../varient-data.service';
import { VarientNewComponent } from '../varient-new/varient-new.component';

@Component({
  selector: 'app-varient-summary',
  templateUrl: './varient-summary.component.html',
  styleUrls: ['./varient-summary.component.scss']
})
export class VarientSummaryComponent implements OnInit {

  entityTitle = "Varient";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private VarientDataService: VarientDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.VarientDataService.getEntityById(this.id);
    
     }


 
  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(VarientNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }
    

  ngOnInit() {
  }

}
