import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VarientSummaryComponent } from './varient-summary.component';

describe('VarientSummaryComponent', () => {
  let component: VarientSummaryComponent;
  let fixture: ComponentFixture<VarientSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VarientSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VarientSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
