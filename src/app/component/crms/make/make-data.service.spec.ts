import { TestBed, inject } from '@angular/core/testing';

import { MakeDataService } from './make-data.service';

describe('MakeDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MakeDataService]
    });
  });

  it('should be created', inject([MakeDataService], (service: MakeDataService) => {
    expect(service).toBeTruthy();
  }));
});
