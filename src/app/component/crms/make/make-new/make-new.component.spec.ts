import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeNewComponent } from './make-new.component';

describe('MakeNewComponent', () => {
  let component: MakeNewComponent;
  let fixture: ComponentFixture<MakeNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakeNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
