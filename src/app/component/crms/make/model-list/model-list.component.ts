import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { ModelDataService } from '../model-data.service';
import { ModelNewComponent } from '../model-new/model-new.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-model-list',
  templateUrl: './model-list.component.html',
  styleUrls: ['./model-list.component.scss']
})
export class ModelListComponent implements OnInit {

  entityTitle = 'Model';

  entities: any;
  entityName: any;
  bsModalRef: BsModalRef;
  paginatedEntity;


  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;
  id:any;
  makes:any;
  currentMake:any;
  constructor(private route: ActivatedRoute, 
    private router: Router,
    private apiService: ApiService,
    private commonDataService : CommonDataService,
    private ModelDataService: ModelDataService,
    private modalService: BsModalService

  ) {
      this.apiService.getAllEntities('make')
      .then(result=>{
        this.makes = result;
      })

   }

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);

  }
  doPagination(start, end){
    this.paginatedEntity = this.entities.slice(start, end);
  }
  getAllEntity(start, end){
    this.id = this.route.snapshot.params['id'];
   
    this.apiService.getEntityByEntityIdList("model","makeid",this.id).then(result => {
      this.currentMake =  this.getMake(this.id);
      console.log( this.currentMake);
      this.entities = result;
      this.ModelDataService.setAllEntity(result);
      this.totalItems = this.entities.length;
      this.doPagination(start, end); 

    });
  }

  navigateToRoute(entity) {
    var id = entity != undefined ? entity.id : '1';
    this.router.navigate(['dashboard/make/new-model', id]);
  }

  navigateToEntitySummary(entity) {
    var id = entity != undefined ? entity.entity_key_id : '1';
    this.router.navigate(['dashboard/make/model-summary', id]);
  }


  navigateToEntityModel(entity) {
    var makeid = this.id;
    var id = entity != undefined ? entity.entity_key_id : '1';
    this.router.navigate(['dashboard/make/varient-list',id, makeid]);
  }


  ngOnInit() {
    this.paginatedEntity = [];
    this.getAllEntity(this.currentStartIndex, this.currentEndIndex); 
  }

  openModalWithComponent() {
    const initialState = {
      makeData:{
        makeId:this.id,
        makeName:this.currentMake
      },
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(ModelNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      console.log("modal close: " + reason);
      this.getAllEntity(this.currentStartIndex, this.currentEndIndex);

    })
  }

  public getMake(makeid) {
    let make;
    make =  this.makes.find(function(currentMake){
       return currentMake.entity_key_id == makeid;
    })
    return make.make;
  }

}
