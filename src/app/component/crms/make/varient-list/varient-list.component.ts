import { Component, OnInit } from '@angular/core';


import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ApiService } from '../../../../service/api.service';
import { Router } from '@angular/router';
import { CommonDataService } from '../../../../service/common-data.service';
import { VarientDataService } from '../varient-data.service';
import { VarientNewComponent } from '../varient-new/varient-new.component';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-varient-list',
  templateUrl: './varient-list.component.html',
  styleUrls: ['./varient-list.component.scss']
})


export class VarientListComponent implements OnInit {
  entityTitle = 'Varient';

  entities: any;
  entityName: any;
  bsModalRef: BsModalRef;
  paginatedEntity;
  varient:any;


  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;

  id:any;

  models:any;
  currentModel:any;
  currentMake:any;
  makes:any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private commonDataService : CommonDataService,
    private VarientDataService: VarientDataService,
    private modalService: BsModalService) {

      this.apiService.getAllEntities('make')
      .then(result=>{
        this.makes = result;
      })

      this.apiService.getAllEntities('model')
      .then(result=>{
        this.models = result;
      })
     }


  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);

  }

  doPagination(start, end){
    this.paginatedEntity = this.entities.slice(start, end);
  }

  getAllEntity(start, end){
    this.id = this.route.snapshot.params['id'];
    this.currentMake = this.route.snapshot.params['makeid'];
    
    this.apiService.getEntityByEntityIdList("varient","modelid",this.id).then(result => {
      this.currentModel =  this.getModel(this.id);
      console.log(result);
      this.entities = result;
      this.VarientDataService.setAllEntity(result);
      this.totalItems = this.entities.length;
      this.doPagination(start, end); 
    });
  }

  navigateToRoute(entity) {
    var id = entity != undefined ? entity.id : '1';
    this.router.navigate(['dashboard/make/new-varient', id]);
  }

  navigateToEntitySummary(entity) {
    var id = entity != undefined ? entity.entity_key_id : '1';
    this.router.navigate(['dashboard/make/varient-summary', id]);
  }

  




  ngOnInit() {
    this.paginatedEntity = [];
    this.getAllEntity(this.currentStartIndex, this.currentEndIndex);  	
  }



  openModalWithComponent() {
    const initialState = {
      modelData:{
        makeid:this.currentMake,
        modelId:this.id,
        modelName:this.currentModel
      },
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(VarientNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      console.log("modal close: " + reason);
      this.getAllEntity(this.currentStartIndex, this.currentEndIndex);

    })
  }


   public getModel(modelid) {
     let model;
     model =  this.models.find(function(currentModel){
        return currentModel.entity_key_id == modelid;
     })
     return model.model;
   }

   public getMake(makeid) {
    let make;
    make =  this.makes.find(function(currentMake){
       return currentMake.entity_key_id == makeid;
    })
    return make.make;
  }

}
