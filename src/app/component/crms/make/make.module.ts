import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';



import { MakeRoutingModule } from './make-routing.module';
import { MakeComponent } from './make.component';
import { MakeListComponent } from './make-list/make-list.component';
import { MakeNewComponent } from './make-new/make-new.component';
import { MakeSummaryComponent } from './make-summary/make-summary.component';
import { MakeDataService } from './make-data.service';

import { ModelSummaryComponent } from './model-summary/model-summary.component';
import { ModelNewComponent } from './model-new/model-new.component';
import { ModelListComponent } from './model-list/model-list.component';
import { VarientListComponent } from './varient-list/varient-list.component';
import { VarientNewComponent } from './varient-new/varient-new.component';
import { VarientSummaryComponent } from './varient-summary/varient-summary.component';


@NgModule({
  imports: [
    CommonModule,
    MakeRoutingModule,

    FormsModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  declarations: [MakeComponent, MakeListComponent, MakeNewComponent, MakeSummaryComponent, ModelSummaryComponent, ModelNewComponent,ModelListComponent
  ,VarientListComponent,VarientNewComponent,VarientSummaryComponent],
  providers: [MakeDataService]
})
export class MakeModule { }
