import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'varient-model-new',
  templateUrl: './varient-new.component.html',
  styleUrls: ['./varient-new.component.scss']
})
export class VarientNewComponent implements OnInit {
  entityTitle = "Varient";
  entity: any;
  modalRef: BsModalRef;
  message: string;
  modelData:any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    public bsModalRef: BsModalRef,
    private apiService: ApiService,
    public modalService: BsModalService,
    private toastr: ToastrService) { 
      console.log("modal:" + modalService);
      console.log("s" + bsModalRef);
      this.entity = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};
      this.modelData = this.modalService.config['modelData'] != undefined ? this.modalService.config['modelData'] : {};
      
    }

    submit(entity) {
      if (entity != undefined && entity.entity_key_id != undefined) {
        this.update(entity);
      } else {
        this.create(entity);
      }
    }

    update(entity) {
      entity.entity_name = 'varient';
      this.apiService.updateEntity(entity).then(result => {
        if (result != undefined) {
          this.modalService.setDismissReason(result);
          this.bsModalRef.hide();
          this.toastr.success(this.entityTitle + ' Updated Successfully');
        } else {
          this.toastr.error('Failed to Update ' + this.entityTitle);
        }
      }).catch(error => {
        this.toastr.error('Failed to update ' + this.entityTitle);
      });
    }
    create(entity) {
      entity.entity_name = 'varient';
      entity.modelid = this.modelData.modelId;
      entity.makeid = this.modelData.makeid;
      this.apiService.addEntity(entity).then(result => {
        if (result != undefined) {
          this.modalService.setDismissReason(result);
          this.bsModalRef.hide();
          this.toastr.success(this.entityTitle + ' Added Successfully');
        } else {
          this.toastr.error('Failed to add ' +this.entityTitle);
        }
      }).catch(error => {
        this.toastr.error('Failed to add ' + this.entityTitle);
      });
    }

  ngOnInit() {
  }

}
