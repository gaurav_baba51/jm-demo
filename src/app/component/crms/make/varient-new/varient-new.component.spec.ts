import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VarientNewComponent } from './varient-new.component';

describe('VarientNewComponent', () => {
  let component: VarientNewComponent;
  let fixture: ComponentFixture<VarientNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VarientNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VarientNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
