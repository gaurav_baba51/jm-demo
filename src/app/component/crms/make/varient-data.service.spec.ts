import { TestBed, inject } from '@angular/core/testing';

import { VarientDataService } from './varient-data.service';

describe('MakeDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VarientDataService]
    });
  });

  it('should be created', inject([VarientDataService], (service: VarientDataService) => {
    expect(service).toBeTruthy();
  }));
});
