import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { MakeComponent } from './make.component';
import { MakeNewComponent } from './make-new/make-new.component';
import { MakeSummaryComponent } from './make-summary/make-summary.component';
import { ModelNewComponent } from './model-new/model-new.component';
import { ModelSummaryComponent } from './model-summary/model-summary.component';
import { ModelListComponent } from './model-list/model-list.component';
import { VarientListComponent } from './varient-list/varient-list.component';
import { VarientNewComponent } from './varient-new/varient-new.component';
import { VarientSummaryComponent } from './varient-summary/varient-summary.component';



const routes: Routes = [
  {
    path: '',
    component: MakeComponent,
    data: {
      title: 'Make'
    }
  },
  {
    path: 'new-make/:id',
    component: MakeNewComponent ,
    data: {
      title: 'Create New Make'
    }
  },
  {
    path: 'make-summary/:id',
    component: MakeSummaryComponent ,
    data: {
      title: 'Make Summary'
    }
  
  },
  {
    path: 'model-list/:id',
    component: ModelListComponent ,
    data: {
      title: 'Model list'
    }
  },
  {
    path: 'model-new/:id',
    component: ModelNewComponent ,
    data: {
      title: 'Model list'
    }
  },
  {
    path: 'model-summary/:id',
    component: ModelSummaryComponent ,
    data: {
      title: 'Model summary'
    }
  },
  
  {
    path: 'varient-list/:id/:makeid',
    component: VarientListComponent ,
    data: {
      title: 'Varient list'
    }
  },
  {
    path: 'varient-new/:id',
    component: VarientNewComponent ,
    data: {
      title: 'Varient list'
    }
  },
  {
    path: 'varient-summary/:id',
    component: VarientSummaryComponent ,
    data: {
      title: 'Varient summary'
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MakeRoutingModule { }
