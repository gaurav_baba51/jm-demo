import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeSummaryComponent } from './make-summary.component';

describe('MakeSummaryComponent', () => {
  let component: MakeSummaryComponent;
  let fixture: ComponentFixture<MakeSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakeSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
