import { Component, OnInit } from '@angular/core';

import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { MakeDataService } from '../make-data.service';
import { MakeNewComponent } from '../make-new/make-new.component';

@Component({
  selector: 'app-make-summary',
  templateUrl: './make-summary.component.html',
  styleUrls: ['./make-summary.component.scss']
})
export class MakeSummaryComponent implements OnInit {

  entityTitle = "Make";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private MakeDataService: MakeDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.MakeDataService.getEntityById(this.id);
     }


 
  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(MakeNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }
    

  ngOnInit() {
  }

}
