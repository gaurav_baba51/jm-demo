import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { ModelDataService } from '../../make/model-data.service';
import { ModelNewComponent } from '../model-new/model-new.component';

@Component({
  selector: 'app-model-summary',
  templateUrl: './model-summary.component.html',
  styleUrls: ['./model-summary.component.scss']
})
export class ModelSummaryComponent implements OnInit {
  entityTitle = "Model";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private MakeDataService: ModelDataService){
      this.id = this.route.snapshot.params['id'];
      this.entity = this.MakeDataService.getEntityById(this.id);
     }

     openModalWithComponent() {
      const initialState = {
        data: this.entity,
        title: 'Create ' + this.entityTitle,
        class: 'modal-lg'
      };
      this.bsModalRef = this.modalService.show(ModelNewComponent, initialState);
      this.bsModalRef.content.closeBtnName = 'Close';
    }

  ngOnInit() {
  }

}
