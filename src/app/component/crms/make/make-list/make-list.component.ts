import { Component, OnInit } from '@angular/core';


import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ApiService } from '../../../../service/api.service';
import { Router } from '@angular/router';
import { CommonDataService } from '../../../../service/common-data.service';
import { MakeDataService } from '../make-data.service';
import { MakeNewComponent } from '../make-new/make-new.component';



@Component({
  selector: 'app-make-list',
  templateUrl: './make-list.component.html',
  styleUrls: ['./make-list.component.scss']
})


export class MakeListComponent implements OnInit {
  entityTitle = 'Make';

  entities: any;
  entityName: any;
  bsModalRef: BsModalRef;
  paginatedEntity;


  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;



  constructor(private router: Router,
    private apiService: ApiService,
    private commonDataService : CommonDataService,
    private MakeDataService: MakeDataService,
    private modalService: BsModalService) { }


  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);

  }

  doPagination(start, end){
    this.paginatedEntity = this.entities.slice(start, end);
  }

  getAllEntity(start, end){
    this.apiService.getAllEntities("make").then(result => {
      this.entities = result;
      console.log(  this.entities);
      this.MakeDataService.setAllEntity(result);
      this.totalItems = this.entities.length;
      this.doPagination(start, end); 
    });
  }

  navigateToRoute(entity) {
    var id = entity != undefined ? entity.id : '1';
    this.router.navigate(['dashboard/make/new-make', id]);
  }

  navigateToEntitySummary(entity) {
    var id = entity != undefined ? entity.entity_key_id : '1';
    this.router.navigate(['dashboard/make/make-summary', id]);
  }
  navigateToEntityModel(entity) {
    var id = entity != undefined ? entity.entity_key_id : '1';
    this.router.navigate(['dashboard/make/model-list',id]);
  }




  ngOnInit() {
    this.paginatedEntity = [];
    this.getAllEntity(this.currentStartIndex, this.currentEndIndex);  	
  }



  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(MakeNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      console.log("modal close: " + reason);
      this.getAllEntity(this.currentStartIndex, this.currentEndIndex);

    })
  }
  

}
