import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { DesignationComponent } from './designation.component';
import { DesignationNewComponent } from './designation-new/designation-new.component';
import { DesignationSummaryComponent } from './designation-summary/designation-summary.component';


const routes: Routes = [
  {
    path: '',
    component: DesignationComponent,
    data: {
      title: 'Designation'
    }
  },
  {
    path: 'new-designation/:id',
    component: DesignationNewComponent ,
    data: {
      title: 'Create New Designation'
    }
  },
  {
    path: 'designation-summary/:id',
    component: DesignationSummaryComponent ,
    data: {
      title: 'Designation'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DesignationRoutingModule { }
