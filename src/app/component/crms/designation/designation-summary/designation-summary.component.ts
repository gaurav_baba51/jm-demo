import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { DesignationDataService } from '../designation-data.service';
import { DesignationNewComponent } from '../designation-new/designation-new.component';

@Component({
  selector: 'app-designation-summary',
  templateUrl: './designation-summary.component.html',
  styleUrls: ['./designation-summary.component.scss']
})
export class DesignationSummaryComponent implements OnInit {

  entityTitle = "Designation";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private designationDataService: DesignationDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.designationDataService.getEntityById(this.id);
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(DesignationNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }


}
