import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignationSummaryComponent } from './designation-summary.component';

describe('DesignationSummaryComponent', () => {
  let component: DesignationSummaryComponent;
  let fixture: ComponentFixture<DesignationSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignationSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignationSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
