import { TestBed, inject } from '@angular/core/testing';

import { DesignationDataService } from './designation-data.service';

describe('DesignationDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DesignationDataService]
    });
  });

  it('should be created', inject([DesignationDataService], (service: DesignationDataService) => {
    expect(service).toBeTruthy();
  }));
});
