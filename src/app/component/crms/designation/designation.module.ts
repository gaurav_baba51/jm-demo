import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { DesignationNewComponent } from './designation-new/designation-new.component';
import { DesignationSummaryComponent } from './designation-summary/designation-summary.component';
import { DesignationListComponent } from './designation-list/designation-list.component';
import { DesignationComponent } from './designation.component';
import { DesignationRoutingModule } from './designation-routing.module';
import { DesignationDataService } from './designation-data.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DesignationRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [
    DesignationComponent,
    DesignationListComponent,
    DesignationSummaryComponent,
    DesignationNewComponent],
  providers: [DesignationDataService]
})
export class DesignationModule { }
