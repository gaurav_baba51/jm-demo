import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiryForNewComponent } from './enquiry-for-new.component';

describe('EnquiryForNewComponent', () => {
  let component: EnquiryForNewComponent;
  let fixture: ComponentFixture<EnquiryForNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiryForNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryForNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
