import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiryForListComponent } from './enquiry-for-list.component';

describe('EnquiryForListComponent', () => {
  let component: EnquiryForListComponent;
  let fixture: ComponentFixture<EnquiryForListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiryForListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryForListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
