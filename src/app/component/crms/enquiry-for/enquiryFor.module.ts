import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { EnquiryForNewComponent } from './enquiry-for-new/enquiry-for-new.component';
import { EnquiryForSummaryComponent } from './enquiry-for-summary/enquiry-for-summary.component';
import { EnquiryForListComponent } from './enquiry-for-list/enquiry-for-list.component';
import { EnquiryForComponent } from './enquiry-for.component';
import { EnquiryForDataService } from './enquiry-for-data.service';
import { EnquiryForRoutingModule } from './enquiryFor-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EnquiryForRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [
    EnquiryForComponent,
    EnquiryForListComponent,
    EnquiryForSummaryComponent,
    EnquiryForNewComponent],
  providers: [EnquiryForDataService]
})
export class EnquiryForModule { }
