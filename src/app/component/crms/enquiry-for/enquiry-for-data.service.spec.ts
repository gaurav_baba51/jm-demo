import { TestBed, inject } from '@angular/core/testing';

import { EnquiryForDataService } from './enquiry-for-data.service';

describe('EnquiryForDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnquiryForDataService]
    });
  });

  it('should be created', inject([EnquiryForDataService], (service: EnquiryForDataService) => {
    expect(service).toBeTruthy();
  }));
});
