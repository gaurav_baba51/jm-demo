import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { EnquiryForDataService } from '../enquiry-for-data.service';
import { EnquiryForNewComponent } from '../enquiry-for-new/enquiry-for-new.component';

@Component({
  selector: 'app-enquiry-for-summary',
  templateUrl: './enquiry-for-summary.component.html',
  styleUrls: ['./enquiry-for-summary.component.scss']
})
export class EnquiryForSummaryComponent implements OnInit {

 
  entityTitle = "Enquiry For";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private enquiryForDataService: EnquiryForDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.enquiryForDataService.getEntityById(this.id);
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(EnquiryForNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }


}
