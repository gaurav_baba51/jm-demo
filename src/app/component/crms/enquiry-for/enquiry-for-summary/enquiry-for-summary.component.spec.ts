import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiryForSummaryComponent } from './enquiry-for-summary.component';

describe('EnquiryForSummaryComponent', () => {
  let component: EnquiryForSummaryComponent;
  let fixture: ComponentFixture<EnquiryForSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiryForSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryForSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
