import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { EnquiryForComponent } from './enquiry-for.component';
import { EnquiryForNewComponent } from './enquiry-for-new/enquiry-for-new.component';
import { EnquiryForSummaryComponent } from './enquiry-for-summary/enquiry-for-summary.component';


const routes: Routes = [
  {
    path: '',
    component: EnquiryForComponent,
    data: {
      title: 'Enquiry For'
    }
  },
  {
    path: 'new-enquiry-for/:id',
    component: EnquiryForNewComponent ,
    data: {
      title: 'Create New Enquiry For'
    }
  },
  {
    path: 'enquiry-for-summary/:id',
    component: EnquiryForSummaryComponent ,
    data: {
      title: 'Enquiry For Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnquiryForRoutingModule { }
