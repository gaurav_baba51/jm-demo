import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { EnquiryDataService } from '../enquiry-data.service';

@Component({
  selector: 'app-new-communication',
  templateUrl: './new-communication.component.html',
  styleUrls: ['./new-communication.component.scss']
})
export class NewCommunicationComponent implements OnInit {

  communication: any;
  enquiries: any;
  communicationFors: any;
  communicationMediums: any;
  contacts: any;
  processStatuses: any;
  enquiry: any;

  constructor(public bsModalRef: BsModalRef, private apiService: ApiService,
    private commonDataService: CommonDataService, public modalService: BsModalService,
    private toastr: ToastrService, private enquiryDataService: EnquiryDataService) {

      this.enquiry = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};

      this.communicationFors = this.enquiryDataService.getCommunicationFors();
      this.communicationMediums = this.enquiryDataService.getCommunicationMediums();
      this.contacts = this.enquiryDataService.getContacts();
      this.processStatuses = this.enquiryDataService.getProcessStatuses();
      this.communication = {};
    }

  ngOnInit() {
  }

  submit(communication) {
    if (communication.entity_key_id != undefined) {
    } else {
      this.create(communication);
    }
  }

  create(communication) {
    communication.entity_name = 'communication';
    communication.inquiryid = this.enquiry.entity_key_id;
    communication.personid = this.enquiry.personid;
    this.apiService.addEntity(communication).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Communication Added Successfully');
      } else {
        this.toastr.error('Failed to add Communication');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Communication');
    });
  }

}
