import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquirySummaryComponent } from './enquiry-summary.component';

describe('EnquirySummaryComponent', () => {
  let component: EnquirySummaryComponent;
  let fixture: ComponentFixture<EnquirySummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquirySummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquirySummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
