import { Component, OnInit } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { ApiService } from '../../../../service/api.service';
import { EnquiryDataService } from '../enquiry-data.service';
import { NewEnquiryComponent } from '../new-enquiry/new-enquiry.component';
import { NewCommunicationComponent } from '../new-communication/new-communication.component';
import { CommonDataService } from '../../../../service/common-data.service';
import { NewTaskComponent } from '../new-task/new-task.component';
import { CommunicationDataService } from '../../communication/communication-data.service';

@Component({
  selector: 'app-enquiry-summary',
  templateUrl: './enquiry-summary.component.html',
  styleUrls: ['./enquiry-summary.component.scss']
})
export class EnquirySummaryComponent implements OnInit {

  id: any;
  enquiry: any;
  originalEnquiry;
  bsModalRef: BsModalRef;
  communications: any;
  tasks: any;
  enquiryFor;
  targetProduct;
  contact;
  branchName;
  enquiryStatus;
  enquirySource;
  processStatus;
  city;

  constructor(private route: ActivatedRoute,
    private enquiryDataService: EnquiryDataService,
    private modalService: BsModalService, 
    private commonDataService: CommonDataService, 
    private router: Router,
    private communicationDataService: CommunicationDataService) {
    this.id = this.route.snapshot.params['id'];
    this.enquiry = {};
    console.log("Enquiry data: " + this.enquiryDataService.getEnquiries().length);
    this.enquiry = this.enquiryDataService.getEnquiryByEnquiryId(this.id);
    // load enquiries if enquiries is not found.
    this.contact = {};
    if (this.enquiry == undefined) {
      this.commonDataService.getEnquiries().then(result => {
        this.enquiryDataService.setEnquiries(result);
        this.enquiry = this.enquiryDataService.getEnquiryByEnquiryId(this.id);
        this.originalEnquiry = JSON.parse(JSON.stringify(this.enquiry));
        this.getEnquiryForNameById(this.enquiry.enquiryfor);
      });
    }else{
      this.getEnquiryForNameById(this.enquiry.enquiryfor);
    }

    this.getCommunicationByEnquiryId();
  }

  ngOnInit() {
  }

  getCommunicationByEnquiryId() {
    this.commonDataService.getCommunicationByEnquiryId(this.id).then(result => {
      this.communications = result;      
      this.enquiryDataService.setCommunications(this.communications);
      this.commonDataService.getCommunicationFors().then(result1 => {
        this.enquiryDataService.setCommunicationFors(result1);
        this.commonDataService.getCommunicationMediums().then(result => {
          this.enquiryDataService.setCommunicationMediums(result);
          this.commonDataService.getEmployees().then(result => {
            this.enquiryDataService.setEmployees(result);
            this.loadData();
            this.onChangeTable(this.config);
          });
        });
      });
    });
  }

  getBranchName(branchId) {
    var branch = this.enquiryDataService.getBranchNameAndLocationByBranchId(branchId);
    if (branch == undefined) {
      this.commonDataService.getBranches().then(result => {
        this.enquiryDataService.setBranches(result);
        branch = this.enquiryDataService.getBranchNameAndLocationByBranchId(branchId);
        this.branchName =  branch.title;
        this.getCityNameByCityId(branch.location);
      });
    } else {
      this.branchName =  branch.title;
      this.getCityNameByCityId(branch.location);
    }
  }

  getCityNameByCityId(cityId) {
    var city = this.enquiryDataService.getCityNameByCityId(cityId);
    if (city == undefined) {
      this.commonDataService.getCities().then(result => {
        this.enquiryDataService.setCities(result);
        city = this.enquiryDataService.getCityNameByCityId(cityId);
        this.branchName = this.branchName + ', ' + city.cityName;
        this.getEnquiryStatusNameById(this.enquiry.statusvalue);
      });
    } else {
      this.branchName = this.branchName + ', ' + city.cityName;
      this.getEnquiryStatusNameById(this.enquiry.statusvalue);
    }
  }

  getEnquiryForNameById(enquiryForId) {
    var enquiryFor = this.enquiryDataService.getEnquiryForNameById(enquiryForId);
    if (enquiryFor == undefined) {
      this.commonDataService.getEnquiryFors().then(result => {
        this.enquiryDataService.setEnquiryFors(result);
        enquiryFor = this.enquiryDataService.getEnquiryForNameById(enquiryForId);
        this.enquiryFor =  enquiryFor.enqfor;
        this.getTargetProductNameById(this.enquiry.targetproduct);
      });
    } else {
      this.enquiryFor =  enquiryFor.enqfor;
      this.getTargetProductNameById(this.enquiry.targetproduct);
    }
  }

  getEnquiryStatusNameById(enquiryStatusId) {
    var enquiryStatus = this.enquiryDataService.getEnquiryStatusNameById(enquiryStatusId);
    if (enquiryStatus == undefined) {
      this.commonDataService.getEnquiryStatuses().then(result => {
        this.enquiryDataService.setEnquiryStatuses(result);
        enquiryStatus = this.enquiryDataService.getEnquiryStatusNameById(enquiryStatusId);
        this.enquiryStatus =  enquiryStatus.statusval;
        this.getEnquirySourceNameById(this.enquiry.enquirysource);
      });
    } else {
      this.enquiryStatus =  enquiryStatus.statusval;
      this.getEnquirySourceNameById(this.enquiry.enquirysource);
    }
  }

  getEnquirySourceNameById(enquirySourceId) {
    var enquirySource = this.enquiryDataService.getEnquirySourceByEnquirySourceId(enquirySourceId);
    if (enquirySource == undefined) {
      this.commonDataService.getEnquirySources().then(result => {
        this.enquiryDataService.setEnquirySources(result);
        enquirySource = this.enquiryDataService.getEnquirySourceByEnquirySourceId(enquirySourceId);
        this.enquirySource =  enquirySource.enqsource;
        this.getProcessStatusById(this.enquiry.processstatus);
      });
    } else {
      this.enquirySource = enquirySource.enqsource;
      this.getProcessStatusById(this.enquiry.processstatus);
    }
  }

  getProcessStatusById(processStatusId) {
    var processStatus = this.enquiryDataService.getProcessStatusById(processStatusId);
    if (processStatus == undefined) {
      this.commonDataService.getProcessStatus().then(result => {
        this.enquiryDataService.setProcessStatuses(result);
        if (this.enquiryDataService.getProcessStatusById(processStatusId) == undefined) {
          this.processStatus = "";
        }
        else{
          this.processStatus = this.enquiryDataService.getProcessStatusById(processStatusId).mprocessstatus;
         
        }
      });
    } else {
      this.processStatus = processStatus.mprocessstatus;
    }
  }

  getTargetProductNameById(targetProductId) {
    var targetProduct = this.enquiryDataService.getTargetProductByTargetProductId(targetProductId);
    if (targetProduct == undefined) {
      this.commonDataService.getTargetProducts().then(result => {
        this.enquiryDataService.setTargetProducts(result);
        targetProduct = this.enquiryDataService.getTargetProductByTargetProductId(targetProductId);
        if (targetProduct == undefined) {
          this.targetProduct = " ";
          this.getContactByContactId(this.enquiry.personid);
        }
        else{
          this.targetProduct = targetProduct.tmake + ' ' + targetProduct.tmodel;
          this.getContactByContactId(this.enquiry.personid);
        }  

      });
    }else{
      this.targetProduct = targetProduct.tmake + ' ' + targetProduct.tmodel;
      this.getContactByContactId(this.enquiry.personid);
    }
  }

  getContactByContactId(contactId) {
    var contact = this.enquiryDataService.getContactByContactId(contactId);
    if (contact == undefined) {
      contact = {};
      this.commonDataService.getContacts().then(result => {
        this.enquiryDataService.setContacts(result);
        contact = this.enquiryDataService.getContactByContactId(contactId);
        this.contact = contact;
        this.getBranchName(this.enquiry.branch);
      });
    }else{
      this.contact = contact;
      this.getBranchName(this.enquiry.branch);
    }
  }

  openModalWithComponent() {
    const initialState = {
      data: this.enquiry,
      title: 'Update Enquiry',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewEnquiryComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  openNewCommunicationModalWithComponent() {
    const initialState = {
      data: this.enquiry,
      title: 'Update Enquiry',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewCommunicationComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  openNewTaskModal() {
    const initialState = {
      data: this.originalEnquiry,
      title: 'Update Enquiry',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewTaskComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      this.getCommunicationByEnquiryId();
      this.loadData();
    })
  }

  getCommunicationMediumByCommunicationMediumId(communicationMediumId) {
    return this.enquiryDataService.getCommunicationMediumByCommunicationMediumId(communicationMediumId);
  }


  getCommunicationForById(communicationForId) {
    return this.enquiryDataService.getCommunicationForByCommunicationForId(communicationForId);
  }

  getContactById(contactId) {
    var contact = this.enquiryDataService.getContactByContactId(contactId);
    if (contact == undefined) {
      contact = {};
      this.commonDataService.getContacts().then(result => {
        this.enquiryDataService.setContacts(result);
        contact = this.enquiryDataService.getContactByContactId(contactId);
        return contact;
      });
    }
    return contact;
  }

  public rows: Array<any> = [];
  public columns: Array<any> = [
    { title: 'Id', name: 'id', sort: 'asc' },
    { title: 'Date', name: 'date', sort: false },
    { title: 'Comunication For', name: 'commfor', sort: false },
    { title: 'Comunication Medium', name: 'commmmedium', sort: false },
    { title: 'Follow Up Date', name: 'followup', sort: false },
    { title: 'Remark', name: 'remark', sort: false }
  ];

  loadData() {
    var temp = [];
    this.communications.forEach((comm, index) => {
      console.log("comm.commmmedium = " + comm.medium);
      console.log(  this.getCommunicationMediumByCommunicationMediumId(comm.medium));


      var enquiry = {
        'id': comm.entity_key_id,
        'date': comm.datevalue,
        'commfor': this.getCommunicationForById(comm.communicationfor).commfor,
        'commmmedium':  this.getCommunicationMediumByCommunicationMediumId(comm.medium).commMedium,
        'followup': comm.followupdate,
        'remark': comm.remarkvalue
      };
      temp.push(enquiry);
      if(index == this.communications.length-1){
        this.data = temp;
        this.data = [...this.data];
      }
    });
  }

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  data: Array<any> = [];
  dataTask: Array<any> = [];

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClickCommunication(data: any): any {
    console.log(data.row.id);
    var id = data.row.id != undefined ? data.row.id : '1';
    this.commonDataService.getCommunications().then(result => {
      this.communicationDataService.setCommunications(result);
      this.router.navigate(['dashboard/communications/communication-summary', id]);
    });
  }

}
