import { TestBed, inject } from '@angular/core/testing';

import { EnquiryDataService } from './enquiry-data.service';

describe('EnquiryDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnquiryDataService]
    });
  });

  it('should be created', inject([EnquiryDataService], (service: EnquiryDataService) => {
    expect(service).toBeTruthy();
  }));
});
