import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnquiryDataService {

  branches : any = [];
  sources: any = [];
  enquiryFors: any = [];
  enquiryStatuses: any = [];
  enquiries: any = [];
  enquirySources: any = [];
  contacts: any = [];
  targetProducts: any = [];
  processStatuses: any = [];
  employees: any = [];
  communications: any = [];
  communicationFors: any = [];
  tasks: any = [];
  communicationMedums: any = [];
  cities: any = [];
  
  constructor() { }

  setCities(cities) {
    this.cities = cities;
  }

  getCities() {
    return this.cities;
  }

  getCityNameByCityId(cityId) {
    for (var city in this.cities) {
      if (this.cities[city].entity_key_id == cityId) {
        return this.cities[city];
      }
    }
  }

  getCommunicationMediums() {
    return this.communicationMedums;
  }

  setCommunicationMediums(communicationMediums) {
    this.communicationMedums = communicationMediums;
  }

  getCommunicationMediumByCommunicationMediumId(communicationMediumId) {
  
    for (var communicationMedium of this.communicationMedums) {
      if (communicationMedium.entity_key_id == communicationMediumId) {
        return communicationMedium;
      }
    }
  }

  getTasks() {
    return this.tasks;
  }

  setTasks(tasks) {
    this.tasks = tasks;
  }

  getTaskByTaskId(taskId) {
    for (var task of this.tasks) {
      if (task.entity_key_id == taskId) {
        return task;
      }
    }
  }

  getCommunicationFors() {
    return this.communicationFors;
  }

  setCommunicationFors(communicationFors) {
    this.communicationFors = communicationFors;
  }

  getCommunicationForByCommunicationForId(communicationForId) {
    for (var communicationFor of this.communicationFors) {
      if (communicationFor.entity_key_id == communicationForId) {
        return communicationFor;
      }
    }
  }

  getCommunications() {
    return this.communications;
  }

  setCommunications(communications) {
    this.communications = communications;
  }

  getCommunicationByCommunicationId(communicationId) {
    for (var communication of this.communications) {
      if (communication.entity_key_id == communicationId) {
        return communication;
      }
    }
  }

  getEmployees() {
    return this.employees;
  }

  setEmployees(employees) {
    this.employees = employees;
  }

  getEmployeeByEmployeeId(employeeId) {
    for (var employee of this.employees) {
      if (employee.entity_key_id == employeeId) {
        return employee;
      }
    }
  }

  getProcessStatuses() {
    return this.processStatuses;
  }

  setProcessStatuses(processStatuses) {
    this.processStatuses = processStatuses;
  }

  getProcessStatusById(processStatusId) {
    for (var processStatus of this.processStatuses) {
      if (processStatus.entity_key_id == processStatusId) {
        return processStatus;
      }
    }
  }

  getTargetProducts() {
    return this.targetProducts;
  }

  setTargetProducts(targetProducts) {
    this.targetProducts = targetProducts;
  }

  getTargetProductByTargetProductId(targetProductId) {
    for (var targetProduct of this.targetProducts) {
      if (targetProduct.entity_key_id == targetProductId) {
        return targetProduct;
      }
    }
  }

  getContacts() {
    return this.contacts;
  }

  setContacts(contacts) {
    this.contacts = contacts;
  }

  getContactByMobileNumber(mobileNumber) {
    for (var contact of this.contacts) {
      if (contact.mobno == mobileNumber) {
        return contact;
      }
    }
  }

  getContactByContactId(contactId) {
    for (var contact of this.contacts) {
      if (contact.entity_key_id == contactId) {
        return contact;
      }
    }
  }


  getEnquirySources() {
    return this.enquirySources;
  }

  setEnquirySources(enquirySources) {
    this.enquirySources = enquirySources;
  }

  getEnquirySourceByEnquirySourceId(enquirySourceId) {
    for (var enquirySource of this.enquirySources) {
      if (enquirySource.entity_key_id == enquirySourceId)  {
        return enquirySource;
      }
    }
  }

  getEnquiries() {
    return this.enquiries;
  }

  setEnquiries(enquiries) {
    this.enquiries = enquiries;
  }

  getEnquiryByEnquiryId(enquiryId) {
    for (var enquiry of this.enquiries) {
      if (enquiry.entity_key_id == enquiryId) {
        return enquiry;
      }
    }
  }

  setBranches(branches) {
    this.branches = branches;
  }

  getBranches() {
    return this.branches;
  }

  getBranchNameAndLocationByBranchId(branchId) {
    for (var i in this.branches) {
      if(this.branches[i].entity_key_id == branchId) {
        return this.branches[i];
      }
    }
  }

  setSources(sources) {
    this.sources = sources;
  }

  getSources() {
    return this.sources;
  }

  setEnquiryFors(forEnquiries) {
    this.enquiryFors = forEnquiries;
  }

  getEnquiryFors() {
    return this.enquiryFors;
  }

  getEnquiryForNameById(enqforid) {
    for (var i in this.enquiryFors) {
      if (this.enquiryFors[i].entity_key_id == enqforid) {
        return this.enquiryFors[i];
      }
    }
  }

  setEnquiryStatuses(enquiryStatuses) {
    this.enquiryStatuses = enquiryStatuses;
  }

  getEnquiryStatuses() {
    return this.enquiryStatuses;
  }

  getEnquiryStatusNameById(stsvalid) {
    for (var i in this.enquiryStatuses) {
      if (this.enquiryStatuses[i].entity_key_id == stsvalid) {
        return this.enquiryStatuses[i];
      }
    }
  }
}
