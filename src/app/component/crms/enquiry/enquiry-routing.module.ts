import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { EnquiryListComponent } from './enquiry-list/enquiry-list.component';
import { NewEnquiryComponent } from './new-enquiry/new-enquiry.component';
import { EnquirySummaryComponent } from './enquiry-summary/enquiry-summary.component';
import { EnquiryComponent } from './enquiry.component';

const routes: Routes = [
  {
    path: '',
    component: EnquiryComponent,
    data: {
      title: 'Enquiries'
    }
  },
  {
    path: 'new-enquiry/:id',
    component: NewEnquiryComponent ,
    data: {
      title: 'Create New Enquiry'
    }
  },
  {
    path: 'enquiry-summary/:id',
    component: EnquirySummaryComponent ,
    data: {
      title: 'Enquiry Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnquiryRoutingModule { }
