import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { EnquiryDataService } from '../enquiry-data.service';

@Component({
  selector: 'app-new-enquiry',
  templateUrl: './new-enquiry.component.html',
  styleUrls: ['./new-enquiry.component.scss']
})
export class NewEnquiryComponent implements OnInit {
  modalRef: BsModalRef;
  enquiry: any;
  contactFound: boolean;
  user: any;
  enquiryFors: any;
  enquiryStatuses: any;
  searchQuery: any;
  enquirySources: any;
  targetProducts: any;
  processStatuses: any;
  employees: any;
  contacts: any;
  branches: any;
  isEditMode: boolean; 
  isContactCollapsed: boolean = true;

  constructor(public bsModalRef: BsModalRef,
    private apiService: ApiService,
    private commonDataService: CommonDataService,
    public modalService: BsModalService,
    private toastr: ToastrService,
    private enquiryDataService: EnquiryDataService) {

    this.enquiry = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};
    this.isEditMode = this.modalService.config['data'] != undefined ? true : false;

    if (this.isEditMode) {
      this.user = this.enquiryDataService.getContactByContactId(this.enquiry.personid);
    } else {
      this.user = {};
    }

  }

  ngOnInit() {
    this.enquiryFors = this.enquiryDataService.getEnquiryFors();
    this.enquiryStatuses = this.enquiryDataService.getEnquiryStatuses();
    this.enquirySources = this.enquiryDataService.getEnquirySources();
    this.targetProducts = this.enquiryDataService.getTargetProducts();
    this.processStatuses = this.enquiryDataService.getProcessStatuses();
    this.employees = this.enquiryDataService.getEmployees();
    this.contacts = this.enquiryDataService.getContacts();
    this.branches = this.enquiryDataService.getBranches();
  }

  searchContact(searchQuery) {
    var contact = this.enquiryDataService.getContactByMobileNumber(searchQuery);
    if (contact == undefined) {
      this.contactFound = false;
    } else {
      this.user = contact;
      this.contactFound = true;
    }
  }

  createUser(user) {
    user.entity_name = 'person';
    this.apiService.addEntity(user).then(result => {
      if (result != undefined) {
        this.toastr.success('Contact Added Successfully');
        setTimeout(() => {
          this.commonDataService.getContacts().then(result => {
          this.enquiryDataService.setContacts(result);
          this.user = this.enquiryDataService.getContactByMobileNumber(this.user.mobno);
          this.contactFound = true;
        });
        }, 4000);
      } else {
        this.toastr.error('Failed to add Contact');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Contact');
    });
  }


  getEmployeesNameByEmployeeId(employeeId) {
    var employee = this.enquiryDataService.getEmployeeByEmployeeId(employeeId);
    var contact = this.enquiryDataService.getContactByContactId(employee.personid);
    return contact.fname + ' ' + contact.lname;
  }

  submit(enquiry) {
    if (enquiry.entity_key_id != undefined) {
      this.update(enquiry);
    } else {
      this.create(enquiry);
    }
  }

  update(enquiry) {
    enquiry.entity_name = 'enquiry';
    this.apiService.updateEntity(enquiry).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Enquiry Updated Successfully');
      } else {
        this.toastr.error('Failed to Update Enquiry');
      }
    }).catch(error => {
      this.toastr.error('Failed to update Enquiry');
    });
  }

  create(enquiry) {
    enquiry.entity_name = 'enquiry';
    enquiry.personid = this.user.entity_key_id;
    this.apiService.addEntity(enquiry).then(result => {
      if (result === true) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Enquiry Added Successfully');
      } else {
        this.toastr.error('Failed to add Enquiry');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Enquiry');
    });
  }

  getBranchName(branchId) {
    var branch = this.enquiryDataService.getBranchNameAndLocationByBranchId(branchId);
    if (branch == undefined) {
      return 'NA';
    } else {
      return branch.title + ', ' + branch.location;
    }
  }

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  compare(c1, c2){
    return c1 && c2 ? c1.entity_key_id === c2.entity_key_id : c1 === c2;
  }
}
