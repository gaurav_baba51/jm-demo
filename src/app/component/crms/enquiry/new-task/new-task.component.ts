import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { CommonDataService } from '../../../../service/common-data.service';
import { EnquiryDataService } from '../enquiry-data.service';
import { ApiService } from '../../../../service/api.service';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss']
})
export class NewTaskComponent implements OnInit {

  task: any;
  employees: any;
  enquiry: any;
  modalRef: BsModalRef;
  statuses: any;

  constructor(public bsModalRef: BsModalRef,
    private commonDataService: CommonDataService,
    public modalService: BsModalService,
    private toastr: ToastrService,
    private enquiryDataService: EnquiryDataService,
    private apiService: ApiService) {
      this.enquiry = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};
      console.log(this.enquiry);
      this.employees = this.enquiryDataService.getEmployees();
      this.statuses = this.enquiryDataService.getEnquiryStatuses();
      this.task = {};
    }

  ngOnInit() {
  }

  submit(task) {
    if (task.entity_key_id != undefined) {
    } else {
      this.create(task);
    }
  }

  create(task) {
    task.entity_name = 'task';
    task.inquiryid = this.enquiry.entity_key_id;
    task.assignto = this.enquiry.assignto;
    task.datevalue = new Date();
    this.apiService.addEntity(task).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Task Added Successfully');
      } else {
        this.toastr.error('Failed to add Task');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Task');
    });
  }

  getEmployeeNameById(employeeId) {
    var employee = this.enquiryDataService.getContactByContactId(employeeId);
    return employee.fname + ' ' + employee.lname;
  }

}
