import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CollapseModule } from 'ngx-bootstrap';
import { Ng2TableModule } from 'ngx-datatable/ng2-table';

import { EnquiryListComponent } from './enquiry-list/enquiry-list.component';
import { NewEnquiryComponent } from './new-enquiry/new-enquiry.component';
import { EnquirySummaryComponent } from './enquiry-summary/enquiry-summary.component';
import { EnquiryComponent } from './enquiry.component';
import { EnquiryRoutingModule } from './enquiry-routing.module';
import { EnquiryDataService } from './enquiry-data.service';
import { TaskListComponent } from './task-list/task-list.component';
import { NewCommunicationComponent } from './new-communication/new-communication.component';
import { NewTaskComponent } from './new-task/new-task.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EnquiryRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    Ng2TableModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot()
  ],
  entryComponents: [ NewCommunicationComponent, NewTaskComponent],
  declarations: [
    EnquiryComponent,
    EnquiryListComponent,
    NewEnquiryComponent,
    EnquirySummaryComponent,
    TaskListComponent,
    NewCommunicationComponent,
    NewTaskComponent],
  providers: [EnquiryDataService]
})
export class EnquiryModule { }
