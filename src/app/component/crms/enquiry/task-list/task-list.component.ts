import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { EnquiryDataService } from '../enquiry-data.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { NewEnquiryComponent } from '../new-enquiry/new-enquiry.component';
import { NewTaskComponent } from '../new-task/new-task.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  tasks: any;
  bsModalRef: BsModalRef;
  id: any;

  constructor(private router: Router, private apiService: ApiService,
    private modalService: BsModalService,
    private enquiryDataService: EnquiryDataService,
    private commonDataService: CommonDataService, private route: ActivatedRoute) {

      this.id = this.route.snapshot.params['id'];
      
      this.commonDataService.getTaskByEnquiryId(this.id).then(result => {
      this.tasks = result;
      console.log(this.id);
      this.enquiryDataService.setTasks(this.tasks);
      this.loadData();
      this.onChangeTable(this.config);
    });
  }

  getContactByEmployeeId(employeeId) {
    var employee = this.enquiryDataService.getEmployeeByEmployeeId(employeeId);
    var contact = this.enquiryDataService.getContactByContactId(employee.personid);
    return contact;
  }

  ngOnInit() {
  }

  public rows: Array<any> = [];
  public columns: Array<any> = [
    { title: 'Id', name: 'id', sort: 'asc' },
    { title: 'Title', name: 'title', sort: false },
    { title: 'Expiry Date', name: 'expdate', sort: false },
    /* { title: 'Assigned To', name: 'assignedto', sort: false }, */
    { title: 'Description', name: 'description', sort: false }
  ];

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  data: Array<any> = [];

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    console.log(data.row.id);
    var id = data.row.id != undefined ? data.row.id : '1';
    this.router.navigate(['dashboard/tasks/task-summary', id]);
  }

  loadData() {
    this.tasks.forEach(task => {
      var enquiry = {
        'id': task.entity_key_id,
        'title': task.task_title,
        'expdate': task.expdate,
        /* assignedto': task.assignto  + '. '+ this.getContactByEmployeeId(task.assignto).fname + ' ' + this.getContactByEmployeeId(task.assignto).lname */
        'description': task.description
      };
      this.data.push(enquiry);
    });
  }

}
