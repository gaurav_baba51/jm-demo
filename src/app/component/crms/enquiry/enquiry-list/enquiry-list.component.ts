import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { EnquiryDataService } from '../enquiry-data.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { NewEnquiryComponent } from '../new-enquiry/new-enquiry.component';

@Component({
  selector: 'app-enquiry-list',
  templateUrl: './enquiry-list.component.html',
  styleUrls: ['./enquiry-list.component.scss']
})
export class EnquiryListComponent implements OnInit {

  enquiries: any;
  bsModalRef: BsModalRef;
  paginatedEnquiries;

  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);

  }

  constructor(private router: Router, private apiService: ApiService,
    private modalService: BsModalService,
    private enquiryDataService: EnquiryDataService,
    private commonDataService: CommonDataService) {

  }

  ngOnInit() {
    this.currentEndIndex = this.pageSize;
    this.currentStartIndex = 0;
    this.getAllEnquiries(this.currentStartIndex, this.currentEndIndex);
  }

  doPagination(start, end) {
    this.paginatedEnquiries = this.enquiries.slice(start, end);
  }

  getAllEnquiries(start, end) {
    this.commonDataService.getEnquiries().then(res => {
      this.commonDataService.getContacts().then(result1 => {
        this.enquiryDataService.setContacts(result1);
        this.commonDataService.getTargetProducts().then(result6 => {
          this.enquiryDataService.setTargetProducts(result6);
          this.commonDataService.getEnquiryStatuses().then(result4 => {
            this.enquiryDataService.setEnquiryStatuses(result4);
            this.enquiries = res;
            this.enquiryDataService.setEnquiries(this.enquiries);
            this.loadData();
            this.onChangeTable(this.config);
          });
        });
      });
    });
    this.commonDataService.getEmployees().then(result8 => {
      this.enquiryDataService.setEmployees(result8);
    });
    this.commonDataService.getProcessStatus().then(result7 => {
      this.enquiryDataService.setProcessStatuses(result7);
    });
    this.commonDataService.getEnquirySources().then(result5 => {
      this.enquiryDataService.setEnquirySources(result5);
    });
    this.commonDataService.getEnquiryFors().then(result3 => {
      this.enquiryDataService.setEnquiryFors(result3);
    });
    this.commonDataService.getBranches().then(result2 => {
      this.enquiryDataService.setBranches(result2);
    });
    this.commonDataService.getCities().then(result9 => {
      this.enquiryDataService.setCities(result9);
    });
  }

  getBranchName(branchId) {
    var branch = this.enquiryDataService.getBranchNameAndLocationByBranchId(branchId);
    if (branch == undefined) {
      return 'NA';
    } else {
      return branch.title + ', ' + branch.location;
    }
  }

  getEnquiryForNameById(enquiryForId) {
    var enquiryFor = this.enquiryDataService.getEnquiryForNameById(enquiryForId);
    if (enquiryFor.enqfor == undefined) {
      return 'N/A';
    } else {
      return enquiryFor.enqfor;
    }
  }

  getEnquiryStatusNameById(enquiryStatusId) {
    var enquiryStatus = this.enquiryDataService.getEnquiryStatusNameById(enquiryStatusId);
    if (enquiryStatus.statusval == undefined) {
      return 'NA';
    } else {
      return enquiryStatus.statusval;
    }
  }

  getEnquirySourceNameById(enquirySourceId) {
    var enquirySource = this.enquiryDataService.getEnquirySourceByEnquirySourceId(enquirySourceId);
    if (enquirySource.enqsource == undefined) {
      return 'N/A';
    } else {
      return enquirySource.enqsource;
    }
  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New Enquiry',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewEnquiryComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      console.log("modal close: " + reason);
      this.getAllEnquiries(this.currentStartIndex, this.currentEndIndex);
      this.loadData();
    })
  }

  navigateToEnquirySummary(enquiry) {
    var id = enquiry != undefined ? enquiry.entity_key_id : '1';
    this.router.navigate(['dashboard/enquiries/enquiry-summary', id]);
  }

  public rows: Array<any> = [];
  public columns: Array<any> = [
    { title: 'Enquiry Id', name: 'id', sort: 'asc' },
    { title: 'Enquiry Date', name: 'datevalue', sort: false },
    { title: 'Contact Name', name: 'contactname', sort: false },
    { title: 'Contact Number', name: 'contactno', sort: false },
    { title: 'Target Product', name: 'targetproduct', sort: false },
    { title: 'Enquiry Status', name: 'status', sort: false }
  ];

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  data: Array<any> = [];

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    console.log(data.row.id);
    var id = data.row.id != undefined ? data.row.id : '1';
    this.router.navigate(['dashboard/enquiries/enquiry-summary', id]);
  }

  loadData() {
    var temp = [];
    this.enquiries.forEach((enq, index) => {
      var enquiry = {
        'id': enq.entity_key_id,
        'datevalue': enq.datevalue,
        'contactname': this.enquiryDataService.getContactByContactId(enq.personid).fname + ' ' + this.enquiryDataService.getContactByContactId(enq.personid).lname,
        'contactno': this.enquiryDataService.getContactByContactId(enq.personid).mobno,
        'targetproduct': this.enquiryDataService.getTargetProductByTargetProductId(enq.targetproduct).tmake + ' ' + this.enquiryDataService.getTargetProductByTargetProductId(enq.targetproduct).tmodel,
        'status': this.enquiryDataService.getEnquiryStatusNameById(enq.statusvalue).statusval
      };
      temp.push(enquiry);
      if(index == this.enquiries.length-1){
        this.data = temp;
        this.data = [...this.data];
      }
    });
  }

}
