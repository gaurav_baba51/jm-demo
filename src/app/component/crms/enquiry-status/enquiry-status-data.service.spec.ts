import { TestBed, inject } from '@angular/core/testing';

import { EnquiryStatusDataService } from './enquiry-status-data.service';

describe('EnquiryStatusDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnquiryStatusDataService]
    });
  });

  it('should be created', inject([EnquiryStatusDataService], (service: EnquiryStatusDataService) => {
    expect(service).toBeTruthy();
  }));
});
