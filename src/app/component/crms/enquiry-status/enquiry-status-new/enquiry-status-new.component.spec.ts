import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiryStatusNewComponent } from './enquiry-status-new.component';

describe('EnquiryStatusNewComponent', () => {
  let component: EnquiryStatusNewComponent;
  let fixture: ComponentFixture<EnquiryStatusNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiryStatusNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryStatusNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
