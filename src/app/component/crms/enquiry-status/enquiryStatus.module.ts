import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { EnquiryStatusSummaryComponent } from './enquiry-status-summary/enquiry-status-summary.component';
import { EnquiryStatusNewComponent } from './enquiry-status-new/enquiry-status-new.component';
import { EnquiryStatusListComponent } from './enquiry-status-list/enquiry-status-list.component';
import { EnquiryStatusComponent } from './enquiry-status.component';
import { EnquiryStatusDataService } from './enquiry-status-data.service';
import { EnquiryStatusRoutingModule } from './enquiryStatus-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EnquiryStatusRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [
    EnquiryStatusComponent,
    EnquiryStatusListComponent,
    EnquiryStatusNewComponent,
    EnquiryStatusSummaryComponent],
  providers: [EnquiryStatusDataService]
})
export class EnquiryStatusModule { }
