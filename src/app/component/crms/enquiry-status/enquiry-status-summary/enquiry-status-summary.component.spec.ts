import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiryStatusSummaryComponent } from './enquiry-status-summary.component';

describe('EnquiryStatusSummaryComponent', () => {
  let component: EnquiryStatusSummaryComponent;
  let fixture: ComponentFixture<EnquiryStatusSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiryStatusSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryStatusSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
