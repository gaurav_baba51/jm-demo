import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { EnquiryStatusDataService } from '../enquiry-status-data.service';
import { EnquiryStatusNewComponent } from '../enquiry-status-new/enquiry-status-new.component';

@Component({
  selector: 'app-enquiry-status-summary',
  templateUrl: './enquiry-status-summary.component.html',
  styleUrls: ['./enquiry-status-summary.component.scss']
})
export class EnquiryStatusSummaryComponent implements OnInit {

  entityTitle = "Enquiry Status";
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private enquiryStatusDataService: EnquiryStatusDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.enquiryStatusDataService.getEntityById(this.id);
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(EnquiryStatusNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

}
