import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { EnquiryStatusComponent } from './enquiry-status.component';
import { EnquiryStatusNewComponent } from './enquiry-status-new/enquiry-status-new.component';
import { EnquiryStatusSummaryComponent } from './enquiry-status-summary/enquiry-status-summary.component';



const routes: Routes = [
  {
    path: '',
    component: EnquiryStatusComponent,
    data: {
      title: 'Enquiry Status'
    }
  },
  {
    path: 'new-enquiry-status/:id',
    component: EnquiryStatusNewComponent ,
    data: {
      title: 'Create New Enquiry Status'
    }
  },
  {
    path: 'enquiry-status-summary/:id',
    component: EnquiryStatusSummaryComponent ,
    data: {
      title: 'Enquiry Status Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnquiryStatusRoutingModule { }
