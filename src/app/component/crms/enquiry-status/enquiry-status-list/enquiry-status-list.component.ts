import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { ApiService } from '../../../../service/api.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { EnquiryStatusDataService } from '../enquiry-status-data.service';
import { EnquiryStatusNewComponent } from '../enquiry-status-new/enquiry-status-new.component';

@Component({
  selector: 'app-enquiry-status-list',
  templateUrl: './enquiry-status-list.component.html',
  styleUrls: ['./enquiry-status-list.component.scss']
})
export class EnquiryStatusListComponent implements OnInit {

  entityTitle = 'Enquiry Status';

  entities: any;
  entityName: any;
  bsModalRef: BsModalRef;
  paginatedEntity;


  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);

  }

  doPagination(start, end){
    this.paginatedEntity = this.entities.slice(start, end);
  }

  getAllEntity(start, end){
    this.apiService.getAllEntities("statusvalue").then(result => {
      this.entities = result;
      this.enquiryStatusDataService.setAllEntity(result);
      this.totalItems = this.entities.length;
      this.doPagination(start, end); 
    });
  }

  navigateToRoute(entity) {
    var id = entity != undefined ? entity.id : '1';
    this.router.navigate(['dashboard/enquiryStatus/new-enquiry-status', id]);
  }

  navigateToEntitySummary(entity) {
    var id = entity != undefined ? entity.entity_key_id : '1';
    this.router.navigate(['dashboard/enquiryStatus/enquiry-status-summary', id]);
  }


  constructor(private router: Router,
    private apiService: ApiService,
    private commonDataService : CommonDataService,
    private enquiryStatusDataService: EnquiryStatusDataService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.paginatedEntity = [];
    this.getAllEntity(this.currentStartIndex, this.currentEndIndex);
  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New ' + this.entityTitle,
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(EnquiryStatusNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      console.log("modal close: " + reason);
      this.getAllEntity(this.currentStartIndex, this.currentEndIndex);

    })
  }
}
