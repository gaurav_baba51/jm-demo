import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiryStatusListComponent } from './enquiry-status-list.component';

describe('EnquiryStatusListComponent', () => {
  let component: EnquiryStatusListComponent;
  let fixture: ComponentFixture<EnquiryStatusListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiryStatusListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryStatusListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
