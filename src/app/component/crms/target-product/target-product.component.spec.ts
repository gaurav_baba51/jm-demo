import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetProductComponent } from './target-product.component';

describe('TargetProductComponent', () => {
  let component: TargetProductComponent;
  let fixture: ComponentFixture<TargetProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
