import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTargetProductComponent } from './new-target-product.component';

describe('NewTargetProductComponent', () => {
  let component: NewTargetProductComponent;
  let fixture: ComponentFixture<NewTargetProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewTargetProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTargetProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
