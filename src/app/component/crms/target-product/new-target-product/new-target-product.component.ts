import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { CommonDataService } from '../../../../service/common-data.service';
import { ApiService } from '../../../../service/api.service';

@Component({
  selector: 'app-new-target-product',
  templateUrl: './new-target-product.component.html',
  styleUrls: ['./new-target-product.component.scss']
})
export class NewTargetProductComponent implements OnInit {

  targetProduct: any;

  modalRef: BsModalRef;
  
  constructor(public bsModalRef: BsModalRef,
    private commonDataService: CommonDataService,
    public modalService: BsModalService,
    private toastr: ToastrService, private apiService: ApiService) {
      this.targetProduct = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};
      //this.targetProduct = {};
    }

  ngOnInit() {
  }

  submit(targetProduct) {
    if (targetProduct.entity_key_id != undefined) {
      this.update(targetProduct);
    } else {
      this.create(targetProduct);
    }
  }

  update(targetProduct) {
    targetProduct.entity_name = 'targetproduct';
    this.apiService.updateEntity(targetProduct).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Target Product Updated Successfully');
      } else {
        this.toastr.error('Failed to Update Target Product');
      }
    }).catch(error => {
      this.toastr.error('Failed to update Target Product');
    });
  }

  create(targetProduct) {
    targetProduct.entity_name = 'targetproduct';
    this.apiService.addEntity(targetProduct).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Target Product Added Successfully');
      } else {
        this.toastr.error('Failed to add Target Product');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Target Product');
    });
  }

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }
}
