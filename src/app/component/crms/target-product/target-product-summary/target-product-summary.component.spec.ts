import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetProductSummaryComponent } from './target-product-summary.component';

describe('TargetProductSummaryComponent', () => {
  let component: TargetProductSummaryComponent;
  let fixture: ComponentFixture<TargetProductSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetProductSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetProductSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
