import { Component, OnInit } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { NewTargetProductComponent } from '../new-target-product/new-target-product.component';
import { TargetProductDataService } from '../target-product-data.service';
import { CommonDataService } from '../../../../service/common-data.service';

@Component({
  selector: 'app-target-product-summary',
  templateUrl: './target-product-summary.component.html',
  styleUrls: ['./target-product-summary.component.scss']
})
export class TargetProductSummaryComponent implements OnInit {

  id: any;
  targetProduct: any;
  bsModalRef: BsModalRef;

  constructor(private route: ActivatedRoute,
    private modalService: BsModalService, private targetProductDataService : TargetProductDataService) {
      this.id = this.route.snapshot.params['id'];
      this.targetProduct = this.targetProductDataService.getTargetProductById(this.id);
    }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.targetProduct,
      title: 'Update Target Product',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewTargetProductComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

}
