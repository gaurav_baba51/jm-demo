import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';

import { NewTargetProductComponent } from '../new-target-product/new-target-product.component';
import { CommonDataService } from '../../../../service/common-data.service';
import { TargetProductDataService } from "../target-product-data.service";

@Component({
  selector: 'app-target-product-list',
  templateUrl: './target-product-list.component.html',
  styleUrls: ['./target-product-list.component.scss']
})
export class TargetProductListComponent implements OnInit {

  bsModalRef: BsModalRef;
  paginatedEnquiries;
  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;

  targetProducts: any;

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);
  }

  doPagination(start, end) {
    this.paginatedEnquiries = this.targetProducts.slice(start, end);
  }

  constructor(private router: Router, private modalService: BsModalService,
    private commonDataService: CommonDataService, private targetProductDataService: TargetProductDataService) { }

  ngOnInit() {
    this.currentEndIndex = this.pageSize;
    this.currentStartIndex = 0;
    this.getTargetProducts(this.currentStartIndex, this.currentEndIndex);
  }

  getTargetProducts(start, end) {
    this.commonDataService.getTargetProducts().then(res => {
      this.targetProducts = res;
      this.targetProductDataService.setTargetProducts(this.targetProducts);
      this.totalItems = this.targetProducts.length;
      console.log("total records:" + this.totalItems);
      this.doPagination(start, end); 
    });
  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New Target Product',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(NewTargetProductComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  navigateToTargetProductSummary(task) {
    var id = task != undefined ? task.entity_key_id : ':id';
    this.router.navigate(['dashboard/target-product/target-product-summary', id]);
  }

}
