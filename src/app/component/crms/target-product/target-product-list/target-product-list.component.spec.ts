import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetProductListComponent } from './target-product-list.component';

describe('TargetProductListComponent', () => {
  let component: TargetProductListComponent;
  let fixture: ComponentFixture<TargetProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetProductListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
