import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TargetProductDataService {
    targetProducts: any;

    getTargetProducts() {
        return this.targetProducts;
    }

    setTargetProducts(targetProducts) {
        this.targetProducts = targetProducts;
    }

    getTargetProductById(targetProductId) {
        for (var targetProduct of this.targetProducts) {
            if (targetProduct.entity_key_id == targetProductId) {
                return targetProduct;
            }
        }
    }
}
