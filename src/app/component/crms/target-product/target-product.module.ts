import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { TargetProductListComponent } from './target-product-list/target-product-list.component';
import { NewTargetProductComponent } from './new-target-product/new-target-product.component';
import { TargetProductSummaryComponent } from './target-product-summary/target-product-summary.component';
import { TargetProductComponent } from './target-product.component';
import { TargetProductRoutingModule } from './target-product-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TargetProductRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [
    TargetProductComponent,
    TargetProductListComponent,
    NewTargetProductComponent,
    TargetProductSummaryComponent],
  providers: []
})
export class TargetProductModule { }
