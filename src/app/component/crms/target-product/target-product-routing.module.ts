import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';

import { TargetProductListComponent } from './target-product-list/target-product-list.component';
import { NewTargetProductComponent } from './new-target-product/new-target-product.component';
import { TargetProductSummaryComponent } from './target-product-summary/target-product-summary.component';
import { TargetProductComponent } from './target-product.component';

const routes: Routes = [
  {
    path: '',
    component: TargetProductComponent,
    data: {
      title: 'Target Product'
    }
  },
  {
    path: 'new-target-product/:id',
    component: NewTargetProductComponent ,
    data: {
      title: 'Create New Target Product'
    }
  },
  {
    path: 'target-product-summary/:id',
    component: TargetProductSummaryComponent ,
    data: {
      title: 'Target Product Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TargetProductRoutingModule { }
