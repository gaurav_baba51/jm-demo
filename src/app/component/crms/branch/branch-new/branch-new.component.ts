import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from '../../../../service/api.service';
import { Router, ActivatedRoute } from '@angular/router';

import { BranchDataService } from '../branch-data.service';

@Component({
  selector: 'app-branch-new',
  templateUrl: './branch-new.component.html',
  styleUrls: ['./branch-new.component.scss']
})
export class BranchNewComponent implements OnInit {
  branch: any;
  modalRef: BsModalRef;
  message: string;

  employees: any;
  contacts: any;
  cities: any;
  
  constructor(private route: ActivatedRoute,
    private router: Router,
    public bsModalRef: BsModalRef,
    private apiService: ApiService,
    public modalService: BsModalService,
    private toastr: ToastrService, private branchDataService: BranchDataService) {
      console.log("modal:" + modalService);
      console.log("s" + bsModalRef);
      this.branch = this.modalService.config['data'] != undefined ? this.modalService.config['data'] : {};

      this.employees = this.branchDataService.getEmployees();
      this.contacts = this.branchDataService.getContacts();
      this.cities = this.branchDataService.getCities();
     }

  ngOnInit() {
  }

  submit(entity) {
    if (entity != undefined && entity.entity_key_id != undefined) {
      this.update(entity);
    } else {
      this.create(entity);
    }
  }

  update(entity) {
    entity.entity_name = 'branch';
    this.apiService.updateEntity(entity).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Branch Updated Successfully');
      } else {
        this.toastr.error('Failed to Update Branch');
      }
    }).catch(error => {
      this.toastr.error('Failed to update Branch');
    });
  }

  create(entity) {
    entity.entity_name = 'branch';
    this.apiService.addEntity(entity).then(result => {
      if (result != undefined) {
        this.modalService.setDismissReason(result);
        this.bsModalRef.hide();
        this.toastr.success('Branch Added Successfully');
      } else {
        this.toastr.error('Failed to add Branch');
      }
    }).catch(error => {
      this.toastr.error('Failed to add Branch');
    });
  }

  getContactByEmployeeId(employeeId) {
    var employee = this.branchDataService.getEmployeeById(employeeId);
    var contact = this.branchDataService.getContactById(employee.personid);
    return contact;
  }

}
