import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BranchDataService {
  
  branches: any;
  employees: any;
  contacts: any;
  cities: any;

  constructor() { }


  setCities(cities) {
    this.cities = cities;
  }

  getCities() {
    return this.cities;
  }

  getCityNameByCityId(cityId) {
    for (var city in this.cities) {
      if (this.cities[city].entity_key_id == cityId) {
        return this.cities[city];
      }
    }
  }

  getAllBranches() {
    return this.branches;
  }

  setAllBranches(branches) {
    this.branches = branches;
  }

  getBranchById(id) {
    for (var i in this.branches) {
      if (this.branches[i].entity_key_id == id) {
        return this.branches[i];
      }
    }
  }

  getContacts() {
    return this.contacts;
  }

  setContacts(contacts) {
    this.contacts = contacts;
  }

  getContactById(id) {
    for (var contact of this.contacts) {
      if (contact.entity_key_id == id) {
        return contact;
      }
    }
  }

  getEmployees() {
    return this.employees;
  }

  setEmployees(employees) {
    this.employees = employees;
  }

  getEmployeeById(id) {
    for (var employee of this.employees) {
      if (employee.entity_key_id == id) {
        return employee;
      }
    }
  }

}
