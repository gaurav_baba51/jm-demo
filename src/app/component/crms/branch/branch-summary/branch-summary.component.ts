import { Component, OnInit } from '@angular/core';
import { CommonDataService } from '../../../../service/common-data.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { ApiService } from '../../../../service/api.service';
import { ActivatedRoute } from '@angular/router';
import { BranchDataService } from '../branch-data.service';
import { BranchNewComponent } from '../branch-new/branch-new.component';

@Component({
  selector: 'app-branch-summary',
  templateUrl: './branch-summary.component.html',
  styleUrls: ['./branch-summary.component.scss']
})
export class BranchSummaryComponent implements OnInit {
  entity: any;
  bsModalRef: BsModalRef;
  id: any;
  employees: any;
  contacts: any;
  cities: any;
  
  constructor(private route: ActivatedRoute, 
    private apiService: ApiService, 
    private modalService: BsModalService, 
    private commonDataService: CommonDataService,
    private branchDataService: BranchDataService) {
      this.id = this.route.snapshot.params['id'];
      this.entity = this.branchDataService.getBranchById(this.id);

      this.employees = this.branchDataService.getEmployees();
      this.contacts = this.branchDataService.getContacts();
     }

  ngOnInit() {
  }

  openModalWithComponent() {
    const initialState = {
      data: this.entity,
      title: 'Create New Branch',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(BranchNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  getContactByEmployeeId(employeeId) {
    var employee = this.branchDataService.getEmployeeById(employeeId);
    var contact = this.branchDataService.getContactById(employee.personid);
    return contact;
  }

  getCityById(id) {
    return this.branchDataService.getCityNameByCityId(id);
  }
}
