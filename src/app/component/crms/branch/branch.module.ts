import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ng-pipes';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { BranchRoutingModule } from './branch-routing.module';
import { BranchComponent } from './branch.component';
import { BranchListComponent } from './branch-list/branch-list.component';
import { BranchNewComponent } from './branch-new/branch-new.component';
import { BranchSummaryComponent } from './branch-summary/branch-summary.component';
import { BranchDataService } from './branch-data.service';
import { Ng2TableModule } from 'ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BranchRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    NgPipesModule,
    TabsModule,
    Ng2TableModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  entryComponents: [
  ],
  declarations: [
    BranchComponent,
    BranchListComponent,
    BranchNewComponent,
    BranchSummaryComponent],
  providers: [BranchDataService]
})
export class BranchModule { }
