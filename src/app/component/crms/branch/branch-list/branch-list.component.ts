import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { UserDataService } from '../../users/user-data.service';
import { CommonDataService } from '../../../../service/common-data.service';
import { ApiService } from '../../../../service/api.service';
import { Router } from '@angular/router';
import { BranchNewComponent } from '../branch-new/branch-new.component';
import { BranchDataService } from '../branch-data.service';

@Component({
  selector: 'app-branch-list',
  templateUrl: './branch-list.component.html',
  styleUrls: ['./branch-list.component.scss']
})
export class BranchListComponent implements OnInit {
  branches: any;
  branchName: any;
  bsModalRef: BsModalRef;
  paginatedBranch;
  contacts: any;
  employees: any;
  cities: any;


  totalItems: number;
  currentPage: number = 0;
  pageSize = 10;
  currentStartIndex;
  currentEndIndex;

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.currentEndIndex = endItem;
    this.currentStartIndex = startItem;
    this.doPagination(startItem, endItem);

  }

  doPagination(start, end) {
    this.paginatedBranch = this.branches.slice(start, end);
  }

  getAllBranches(start, end) {
    this.apiService.getAllEntities("branch").then(result => {
      this.branches = result;
      this.loadData();
      this.length = this.data.length;
      this.onChangeTable(this.config);
      this.branchDataService.setAllBranches(result);
      this.totalItems = this.branches.length;
      this.doPagination(start, end);
    });
  }

  navigateToRoute(branch) {
    var id = branch != undefined ? branch.id : '1';
    this.router.navigate(['dashboard/branches/new-branch', id]);
  }

  navigateToBranchSummary(branch) {
    var id = branch != undefined ? branch.entity_key_id : '1';
    this.router.navigate(['dashboard/branches/branch-summary', id]);
  }

  constructor(private router: Router,
    private apiService: ApiService,
    private commonDataService: CommonDataService,
    private branchDataService: BranchDataService,
    private modalService: BsModalService) {
    this.commonDataService.getEmployees().then(result => {
      this.branchDataService.setEmployees(result);
      this.commonDataService.getContacts().then(result1 => {
        this.branchDataService.setContacts(result1);
        this.commonDataService.getCities().then(result2 => {
          this.branchDataService.setCities(result2);
          this.getAllBranches(this.currentStartIndex, this.currentEndIndex);
        });
      });
    });
    
  }

  ngOnInit() {
    this.paginatedBranch = [];
    this.getAllBranches(this.currentStartIndex, this.currentEndIndex);
  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Create New Branch',
      class: 'modal-lg'
    };
    this.bsModalRef = this.modalService.show(BranchNewComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.modalService.onHide.subscribe((reason: string) => {
      console.log("modal close: " + reason);
      this.getAllBranches(this.currentStartIndex, this.currentEndIndex);

    })
  }

  getContactByEmployeeId(employeeId) {
    var employee = this.branchDataService.getEmployeeById(employeeId);
    var contact = this.branchDataService.getContactById(employee.personid);
    return contact;
  }

  getCityById(id) {
    return this.branchDataService.getCityNameByCityId(id);
  }

  public rows: Array<any> = [];
  public columns: Array<any> = [
    { title: 'Id', name: 'id', sort: 'asc'},
    { title: 'Title', name: 'title' },
    { title: 'Location', name: 'location' },
    { title: 'Contact Number', name: 'contact' },
    { title: 'Admin', name: 'admin' }
  ];

  loadData() {
    this.branches.forEach(vehicle => {
      var location1 = this.getCityById(vehicle.location);
      var location = location1.cityName;
      var admin1 = this.getContactByEmployeeId(vehicle.branchadmin);
      var admin = admin1.fname + ' ' + admin1.lname;
      var person = {
        'id': vehicle.entity_key_id,
        'title': vehicle.title,
        'location': location,
        'contact': vehicle.contno,
        'admin' : admin
      };
      this.data.push(person);
    });
  }

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered']
  };

  data: Array<any> = [];

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name] != undefined && item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    console.log(data.row.id);
    var id = data.row.id != undefined ? data.row.id : '1';
    this.router.navigate(['dashboard/branches/branch-summary', id]);
  }

}
