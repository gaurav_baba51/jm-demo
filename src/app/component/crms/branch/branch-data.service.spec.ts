import { TestBed, inject } from '@angular/core/testing';

import { BranchDataService } from './branch-data.service';

describe('BranchDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BranchDataService]
    });
  });

  it('should be created', inject([BranchDataService], (service: BranchDataService) => {
    expect(service).toBeTruthy();
  }));
});
