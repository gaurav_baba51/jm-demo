import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { BranchComponent } from '../branch/branch.component';
import { BranchNewComponent} from '../branch/branch-new/branch-new.component';
import { BranchSummaryComponent } from '../branch/branch-summary/branch-summary.component';

const routes: Routes = [
  {
    path: '',
    component: BranchComponent,
    data: {
      title: 'Branches'
    }
  },
  {
    path: 'new-branch/:id',
    component: BranchNewComponent ,
    data: {
      title: 'Create New Branch'
    }
  },
  {
    path: 'branch-summary/:id',
    component: BranchSummaryComponent ,
    data: {
      title: 'Branch Summary'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BranchRoutingModule { }
