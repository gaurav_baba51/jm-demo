export const navItems = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'
  },
  {
    name: 'CRMS',
    url: '/dashboard/users',
    icon: 'icon-pencil'
  },
  {
    name: 'Vehicle Management',
    url: '/dashboard/vehicles',
    icon: 'icon-pie-chart'
  },
  {
    name: 'Admin',
    url: '/users',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'City',
        url: '/dashboard/cities',
        icon: 'icon-puzzle'
      },
      {
        name: 'Branch',
        url: '/dashboard/branches',
        icon: 'icon-puzzle'
      },
      {
        name: 'Communication For',
        url: '/dashboard/communicationFor',
        icon: 'icon-puzzle'
      },
      {
        name: 'Communication Medium',
        url: '/dashboard/communicationMedium',
        icon: 'icon-puzzle'
      },
      {
        name: 'Designation',
        url: '/dashboard/designation',
        icon: 'icon-puzzle'
      },
      {
        name: 'Enquiry For',
        url: '/dashboard/enquiryFor',
        icon: 'icon-puzzle'
      },
      {
        name: 'Enquiry Source',
        url: '/dashboard/enquirySource',
        icon: 'icon-puzzle'
      },
      {
        name: 'Enquiry Status',
        url: '/dashboard/enquiryStatus',
        icon: 'icon-puzzle'
      },
      {
        name: 'Target Product',
        url: '/dashboard/make',
        icon: 'icon-puzzle'
      },
      {
        name: 'Occupation',
        url: '/dashboard/accessories',
        icon: 'icon-puzzle'
      },
      {
        name: 'Process Status',
        url: '/dashboard/checkpoint',
        icon: 'icon-puzzle'
      }
    ]
  },
];
