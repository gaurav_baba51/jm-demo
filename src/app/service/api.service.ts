import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { promise } from 'selenium-webdriver';
const conf = require('config/config.json');

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl: any;

  constructor(private http: Http) {
    this.baseUrl = conf.serverBaseUrl;
  }


  getEntity(entityName) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getEntity/' + entityName, options).toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  
  getEntityFromId(entityName,entityId) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getEntity/' + entityName + '/' + entityId , options).toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  getAllEntities(entityName) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getAllEntity/' + entityName, options).toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  getEntityByEntityIdList(primaryEntity, secondayEntity, secondaryEntityId) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getEntityByEntityIdList/' + primaryEntity + '/' + secondayEntity + '/' + secondaryEntityId, options).toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  getEntityById(primaryEntity, entityId) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getEntityById/' + primaryEntity + '/' + entityId, options).toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  getAllContacts() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getAllEntity/person', options).toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  getPersonById(id) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getPersonById/' + id, options).map(data => data.json());
  }

  addContactPromise(user) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.baseUrl + '/addPerson', user, options).
      toPromise().then(this.extractData).catch(this.handleError);
  }

  addEntity(entity) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.baseUrl + '/addEntity', entity, options).
      toPromise().then(this.extractData).catch(this.handleError);
  }

  addEntityReturnRepone(entity) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.baseUrl + '/addEntity', entity, options).
      toPromise().then(this.extractData).catch(this.handleError);
  }

  updateEntity(entity) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.put(this.baseUrl + '/updateEntity', entity, options).
      toPromise().then(this.extractData).catch(this.handleError);
  }

  searchContact(searchQuery) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.baseUrl + '/searchPersonDetails', searchQuery, options).
      toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  updateContactPromise(user) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.put(this.baseUrl + '/updatePerson', user, options).
      toPromise().then(this.extractData).catch(this.handleError);
  }

  getAllCities() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getAllCity', options).toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  getAllBranches() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getAllBranch', options).toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  getAllEnquiries() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getAllEnquiry', options).map(data => data.json());
  }

  getAllEnquiryFor() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getAllEnqFor', options).toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  getAllEnquiryStatus() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getAllStatusVal', options).toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  getImageEntityById(primaryEntity, entityId) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/getEntityBlobById/' + primaryEntity + '/' + entityId, options).toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  addBlobImage(entity){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.baseUrl + '/addBolb', entity, options).
      toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  updateBlobImage(entity){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.put(this.baseUrl + '/updateBlob', entity, options).
      toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  addBlobImageMultiple(entity){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.baseUrl + '/addMultiBolb', entity, options).
      toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  updateBlobImageMultiple(entity){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.put(this.baseUrl + '/updateMultiBolb', entity, options).
      toPromise().then(this.extractGetDate).catch(this.handleError);
  }

  private extractGetDate(res: Response) {
    let body = res.json();
    return body || {};
  }

  private extractData(res: Response) {
    return res.json();
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
