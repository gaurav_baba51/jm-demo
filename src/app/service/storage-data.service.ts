import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class StorageDataService {

  constructor() { }

  public saveData(entityName: string, entityArray: any) {
      var dataObject = {
          storageTime: Date.now(),
          data : entityArray
      };
      localStorage.setItem(entityName, JSON.stringify(dataObject));
  }

  public getData(entityName: string) {
      var dataObj =  JSON.parse(localStorage.getItem(entityName));
      var timeDiff = this.timeDifference(Date.now(), dataObj.storageTime);
  }

  private timeDifference(date1,date2) {
    var difference = date1.getTime() - date2.getTime();
    var minutesDifference = Math.floor(difference/1000/60);
    return difference -= minutesDifference*1000*60
  }
  
}
