import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class CommonDataService {
  contacts: any;
  cities: any;
  branches: any;
  enquiryFors: any;
  enquiryStatuses: any;
  enquirySources: any;

  constructor(private apiService: ApiService) { }

  getContacts(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('person').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getCities(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('city').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getBranches(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('branch').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getBranchByBranchId(branchId): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getEntityById('branch', branchId).then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getEnquiryFors(): Promise<any> {
    return new Promise(resolve => {
      if (this.enquiryFors == undefined) {
        this.apiService.getAllEntities('enquiryfor').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getEnquiryStatuses() {
    return new Promise(resolve => {
      if (this.enquiryStatuses == undefined) {
        this.apiService.getAllEntities('statusvalue').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getCommunicationFors() {
    return new Promise(resolve => {
      if (this.enquiryStatuses == undefined) {
        this.apiService.getAllEntities('commfor').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getEnquiriesByPersonId(personId): Promise<any> {
    return new Promise(resolve => {
      if (this.enquiryFors == undefined) {
        this.apiService.getEntityByEntityIdList('enquiry', 'personid', personId).then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getCommunicationsByPersonId(personId): Promise<any> {
    return new Promise(resolve => {
      if (this.enquiryFors == undefined) {
        this.apiService.getEntityByEntityIdList('communication', 'personid', personId).then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getTaskByEnquiryId(enquiryId): Promise<any> {
    return new Promise(resolve => {
      if (this.enquiryFors == undefined) {
        this.apiService.getEntityByEntityIdList('task', 'inquiryid', enquiryId).then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getCommunicationByEnquiryId(enquiryId): Promise<any> {
    return new Promise(resolve => {
      if (this.enquiryFors == undefined) {
        this.apiService.getEntityByEntityIdList('communication', 'inquiryid', enquiryId).then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getEnquirySources(): Promise<any> {
    return new Promise(resolve => {
      if (this.enquiryFors == undefined) {
        this.apiService.getAllEntities('enquirysource').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getTargetProducts(): Promise<any> {
    return new Promise(resolve => {
      if (this.enquiryFors == undefined) {
        this.apiService.getAllEntities('targetproduct').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getEnquiries(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('enquiry').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getCommunications(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('communication').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getCommunicationMediums(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('commmedium').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getTasks(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('task').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getEmployees(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('employee').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getProcessStatus(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('mprocessstatus').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getVehicles(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('vehicle').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getFuelTypes(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('fueltypems').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getVehicleCategories(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('vehiclecategoryms').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getVehicleTypes(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('vehicletype').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getTransmissions(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('transmissionms').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getOccupation(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('occupation').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getDesignations(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('designation').then(result => {
          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }


  getMake(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('make').then(result => {

          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getModel(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('model').then(result => {

          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

  getVarient(): Promise<any> {
    return new Promise(resolve => {
      if (this.branches == undefined) {
        this.apiService.getAllEntities('varient').then(result => {

          resolve(result);
        }).catch(error => console.log(error));
      }
    });
  }

}
