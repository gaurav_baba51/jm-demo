import { Injectable } from '@angular/core';
const appData = require('assets/metaData/metaData.json');

@Injectable({
  providedIn: 'root'
})
export class DashboardMetaDataService {

  constructor() {
    console.log('in DashboardMetaDataService');
  }

  getAppMetaData() {
    localStorage.setItem('dashboardMetaData', JSON.stringify(appData));
    return appData;
  }

}
