import { TestBed, inject } from '@angular/core/testing';

import { DashboardMetaDataService } from './dashboard-meta-data.service';

describe('DashboardMetaDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DashboardMetaDataService]
    });
  });

  it('should be created', inject([DashboardMetaDataService], (service: DashboardMetaDataService) => {
    expect(service).toBeTruthy();
  }));
});
