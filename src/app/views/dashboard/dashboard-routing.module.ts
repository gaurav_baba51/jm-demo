import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    data: {
      title: 'Dashboard'
    }
  },
  {
    path: 'users',
    loadChildren: '../../component/crms/users/users.module#UsersModule'
  },
  {
    path: 'enquiries',
    loadChildren: '../../component/crms/enquiry/enquiry.module#EnquiryModule'
  },
  {
    path: 'communications',
    loadChildren: '../../component/crms/communication/communication.module#CommunicationModule'
  },
  {
    path: 'cities',
    loadChildren: '../../component/crms/city/city.module#CityModule'
  },
  {
    path: 'branches',
    loadChildren: '../../component/crms/branch/branch.module#BranchModule'
  },
  {
    path: 'tasks',
    loadChildren: '../../component/crms/task/task.module#TaskModule'
  },
  {
    path: 'communicationFor',
    loadChildren: '../../component/crms/communication-for/communicationFor.module#CommunicationForModule'
  },
  {
    path: 'communicationMedium',
    loadChildren: '../../component/crms/comm-medium/communicationMedium.module#CommunicationMediumModule'
  },
  {
    path: 'designation',
    loadChildren: '../../component/crms/designation/designation.module#DesignationModule'
  },
  {
    path: 'enquiryFor',
    loadChildren: '../../component/crms/enquiry-for/enquiryFor.module#EnquiryForModule'
  },
  {
    path: 'enquirySource',
    loadChildren: '../../component/crms/enquiry-source/enquirySource.module#EnquirySourceModule'
  },
  {
    path: 'enquiryStatus',
    loadChildren: '../../component/crms/enquiry-status/enquiryStatus.module#EnquiryStatusModule'
  },
  {
    path: 'target-product',
    loadChildren: '../../component/crms/target-product/target-product.module#TargetProductModule'
  },
  {
    path: 'vehicles',
    loadChildren: '../../component/crms/vehicle/vehicle.module#VehicleModule'
  },
  {
    path: 'accessories',
    loadChildren: '../../component/crms/accessories/accessories.module#AccessoriesModule'
  },
  {
    path: 'checkpoint',
    loadChildren: '../../component/crms/checkpoint/checkpoint.module#CheckpointModule'
  },
  {
    path: 'employee',
    loadChildren: '../../component/crms/employee/employee.module#EmployeeModule'
  },
  {
    path: 'vehicleCategory',
    loadChildren: '../../component/crms/vehicle-category/vehicle-category.module#VehicleCategoryModule'
  },
  {
    path: 'vehicleType',
    loadChildren: '../../component/crms/vehicle-type/vehicle-type.module#VehicleTypeModule'
  },
  {
    path: 'make',
    loadChildren: '../../component/crms/make/make.module#MakeModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
