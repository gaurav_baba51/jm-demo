import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { DashboardMetaDataService } from './dashboard-meta-data.service';
import { CommonDataService } from '../../service/common-data.service'

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  appData: any;
  vehicles: any;
  enquiries: any;

  constructor(public dashboardMetaDataService: DashboardMetaDataService, private router: Router,
    private commonDataService: CommonDataService) {

    this.commonDataService.getVehicles().then(vehicles => {
      this.vehicles = vehicles.length;
      this.commonDataService.getEnquiries().then(enquiries => {
        this.enquiries = enquiries.length;
        this.appData = dashboardMetaDataService.getAppMetaData();
        for (var i = 0; i < this.appData.length; i++) {
          var items = this.appData[i].subMenu;
          for (var j = 0; j < items.length; j++) {
            if (items[j].title == "Vehicle in stock") {
              items[j].badge = this.vehicles;
            }
            if (items[j].title == "Vist Enquiry") {
              items[j].badge = this.enquiries;
            }
          }
        }
      });
    });
  }

  navigateToRoute(route) {
    this.router.navigate(['dashboard/' + route]);
  }

  ngOnInit(): void {

  }

}