import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  user;
  
  constructor(private router: Router, private toastr: ToastrService){
    this.user = {}
  }

  ngOnInit(): void {
  }

  login(){
    if(this.user.username == "test" && this.user.password == "Password1"){
      var encodedToken = btoa(this.user.username + this.user.password);
      localStorage.setItem('token', encodedToken);
      this.router.navigate(['dashboard']);
    }else{
      this.toastr.error("Wrong Credentials");
    }
  }

  keyDownLogin(event){
    if(event.keyCode == 13) {
      this.login();
    }
  }
 }
